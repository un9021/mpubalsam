<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="html" indent="yes" encoding="cp1251"/>
<xsl:template match="AlarmTrace">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=cp1251"/>
<title>Журнал аварий</title>
</head>
<body>
<h1>Журнал аварий</h1>
<xsl:apply-templates select="alarm">
	<xsl:with-param name="size" select="@size"/>
</xsl:apply-templates>
</body>
</html>
</xsl:template>

<xsl:template match="alarm">
<xsl:param name="size"/>
<h3>
<xsl:if test="@hour &lt; 10">0</xsl:if><xsl:value-of select="@hour"/>:<xsl:if test="@min &lt; 10">0</xsl:if><xsl:value-of select="@min"/>:<xsl:if test="@sec &lt; 10">0</xsl:if><xsl:value-of select="@sec"/>&#160;<xsl:if test="@day &lt; 10">0</xsl:if><xsl:value-of select="@day"/>/<xsl:if test="@mon &lt; 10">0</xsl:if><xsl:value-of select="@mon"/>/<xsl:if test="@year &lt; 10">0</xsl:if><xsl:value-of select="@year"/>&#160;
Авария <xsl:value-of select="@id"/>
</h3>
<h3>Описание: <xsl:value-of select="@textname"/></h3>
<b>Шаг: </b><xsl:value-of select="@step_msec"/> мсек
<br/>
<br/>
<table border="1">
	<tr>
		<th style="width:300px;">Датчик/время, мсек</th>
		<xsl:call-template name="time">
			<xsl:with-param name="cnt" select="$size"/>
			<xsl:with-param name="tm" select='0'/>
			<xsl:with-param name="step" select="@step_msec"/>
		</xsl:call-template>
	</tr>
	<xsl:apply-templates select="sensor"/>
</table>
<hr/>
</xsl:template>

<xsl:template match="sensor">
<tr>
<th style="width:300px;">
	<xsl:if test="@textname=''">
		Id <xsl:value-of select="@id"/>
	</xsl:if>
	<xsl:value-of select="@textname"/>
</th>
<xsl:apply-templates select="item"/>
</tr>
</xsl:template>

<xsl:template match="item">
<td><xsl:value-of select="@val"/></td>
</xsl:template>

<xsl:template name="time">
	<xsl:param name="cnt"/>
	<xsl:param name="tm"/>
	<xsl:param name="step"/>
	<xsl:if test="$cnt > 0">
		<th style="width:50px;"><xsl:value-of select="$tm"/></th>
		<xsl:call-template name="time">
			<xsl:with-param name="cnt" select="$cnt - 1"/>
			<xsl:with-param name="tm" select="$tm + $step"/>
			<xsl:with-param name="step" select="$step"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
