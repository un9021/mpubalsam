<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="text" indent="yes" encoding="utf-8"/>

<xsl:template match="//sensors">
"pos(<xsl:value-of select="date:date()"/>)","id","name","textname","iotype","val_type","inverse","winLSU","winDrive","winFQC","winTrans","winCooling","lsuTemp","driveTemp","fqcTemp","transTemp","coolTemp","vmin","vmax","val_max","val_warn","val_alarm","sidWarn","sidAlarm","unsigned","tcp_vtype","mb_vtype","tcp_mbtype","tcp_mbfunc","tcp_mbaddr","h_alarm","direct","title","confFQC","confTemp","confProt","default"
<xsl:for-each select="//sensors/item">
<xsl:value-of select="position()"/>,<xsl:value-of select="@id"/>,"<xsl:value-of select="@name"/>","<xsl:value-of select="@textname"/>","<xsl:value-of select="@iotype"/>","<xsl:value-of select="@val_type"/>","<xsl:value-of select="@inverse"/>","<xsl:value-of select="@winLSU"/>","<xsl:value-of select="@winDrive"/>","<xsl:value-of select="@winFQC"/>","<xsl:value-of select="@winTrans"/>","<xsl:value-of select="@winCooling"/>","<xsl:value-of select="@lsuTemp"/>","<xsl:value-of select="@driveTemp"/>","<xsl:value-of select="@fqcTemp"/>","<xsl:value-of select="@transTemp"/>","<xsl:value-of select="@coolTemp"/>",<xsl:value-of select="@vmin"/>,<xsl:value-of select="@vmax"/>,<xsl:value-of select="@val_max"/>,<xsl:value-of select="@val_warn"/>,<xsl:value-of select="@val_alarm"/>,"<xsl:value-of select="@sidWarn"/>","<xsl:value-of select="@sidAlarm"/>","<xsl:value-of select="@unsigned"/>","<xsl:value-of select="@tcp_vtype"/>","<xsl:value-of select="@mb_vtype"/>","<xsl:value-of select="@tcp_mbtype"/>","<xsl:value-of select="@tcp_mbfunc"/>","<xsl:value-of select="@tcp_mbaddr"/>","<xsl:value-of select="@h_alarm"/>","<xsl:value-of select="@direct"/>","<xsl:value-of select="@title"/>","<xsl:value-of select="@confFQC"/>","<xsl:value-of select="@confTemp"/>","<xsl:value-of select="@confProt"/>",<xsl:value-of select="@default"/><xsl:text>
</xsl:text>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
