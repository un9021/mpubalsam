<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="html" indent="yes" encoding="cp1251"/>
<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=cp1251"/>
<title>Журнал неисправностей</title>
</head>
<body>
<table cellspasing="4px" >
<xsl:for-each select="//warning">
<tr><td width="40px"><xsl:value-of select="@id"/></td><td width="40px"><xsl:value-of select="@time"/></td><td><xsl:value-of select="@text"/></td></tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
