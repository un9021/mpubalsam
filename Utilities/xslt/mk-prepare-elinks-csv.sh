#!/bin/sh

PROG="${0##*/}"

print_usage()
{
    [ "$1" = 0 ] || exec >&2
    cat <<EOF
Usage: $PROG csvfile.csv > result.file

	csvfile format: 
		...
		"Exx" "Cxxx"
		"Exx" "Cxxx"
		"Exx" "Cxxx"
		...
EOF
    [ -n "$1" ] && exit "$1" || exit
}

cfile="$1"

[ -z "$cfile" ] && print_usage 0


cat $cfile | sed -e "s|[С,с,C,c]|\nC|"  | sed -e "s|\"||"
