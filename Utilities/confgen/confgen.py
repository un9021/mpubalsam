#!/usr/bin/env python 
#-*- coding: utf-8 -*-

import sys
import re
import codecs
import datetime

verbose = False
skip_head_line = 1

argc=len(sys.argv)
if argc < 2:
	sys.exit("Unknown file. Use: confgen.py file.csv [configure.xml]")

infilename = sys.argv[1]

outfilename="sensors.xml"
confile=""
if argc > 2:
	confile = sys.argv[2]
	outfilename = "%s.out"%sys.argv[2]

# field id in cvs
csv_fields = [
	[ 0, "pos" ],
	[ 1, "id" ],
  	[ 2, "name" ],
  	[ 3, "textname" ],
	[ 4, "iotype" ],
	[ 5, "val_type" ],
	[ 6, "inverse" ],
	[ 7, "winLSU" ],
	[ 8, "winDrive" ],
	[ 9, "winFQC" ],
	[ 10, "winTrans" ],
	[ 11, "winCooling" ],
	[ 12, "lsuTemp" ],
	[ 13, "driveTemp" ],
	[ 14, "fqcTemp" ],
	[ 15, "transTemp" ],
	[ 16, "coolTemp" ],
	[ 17, "vmin" ],
	[ 18, "vmax" ],
	[ 19, "val_max" ],
	[ 20, "val_warn" ],
	[ 21, "val_alarm" ],
	[ 22, "sidWarn" ],
	[ 23, "sidAlarm" ],
	[ 24, "unsigned" ],
	[ 25, "tcp_vtype" ],
	[ 26, "mb_vtype" ],
	[ 27, "tcp_mbtype" ],
	[ 28, "tcp_mbfunc" ],
	[ 29, "tcp_mbaddr" ],
	[ 30, "h_alarm" ],
	[ 31, "direct" ],
	[ 32, "title" ],
	[ 33, "confFQC" ],
	[ 34, "confTemp" ],
	[ 35, "confProt" ],
	[ 36, "default" ]
]      


def parse_csv_line( s_orig, sep=',', limiter='"' ):

	elist = []
	
	# 0 - wait limiter or sep
	# 1 - wait limiter
	# 2 - wait sep
	wait_event = 0
	
	val = ""
	
	for i in range(0,len(s_orig)):
	
		ch = s_orig[i]
		
		if ch == sep:
			
			if wait_event == 0:
				elist.append(val.strip())
#				print "add(1) %s\n"%val
				val=""
				wait_event = 0
				continue
			
			if wait_event == 2:
				wait_event = 0
				val = ""
				continue
		
		if ch == limiter:
			if wait_event == 1:
				elist.append(val.strip())
#				print "add(2) %s\n"%val
				wait_event = 2
				val = ""
				continue
			
			wait_event = 1
			continue

		val = val + ch
		#print "%s\n"%val
		
	if wait_event != 2:
		elist.append(val.strip())	
		
	return elist

# Делаем словарь, что бы отсортировать по полю "position"
outlist = dict()

# Processing
inf = codecs.open(infilename, "r", "utf-8")
first_line = False
numline = 0

for line in inf.readlines():

	numline += 1
	if numline <= skip_head_line:
		continue;
	
#	print line
	dat = parse_csv_line(line)
	
	if len(dat) < len(csv_fields):
		err = "Incorrect data size.. (data='%d' < need='%d')"%(len(dat),len(csv_fields))
		sys.exit(err)
	
#	print str(dat)
#	exit(0)
	
	if len(dat) == 0:
		continue
	
	tname = dat[2]
	if len(tname) == 0:
		continue

	proplist = dict()
	item_pos = 0 
	
	for e in csv_fields:
		val = dat[e[0]]
		
		# position запоминаем но в xml не пишем 
		# это генерируемое поле
		if e[1] == "pos":
			item_pos = int(val)
			continue
		
		if len(val) == 0:
			continue

#		if e[1] == "id":
#			val = "0x%s"%val
		
		proplist[ e[1] ] = val

	outlist[item_pos] = proplist

inf.close()

def save_sensors_list( out ):
	for lname,lst in outlist.items():
		out.write("\t<item")
#		for nm,val in lst.items():
#			out.write("  %s=\"%s\""%(nm,val) )
#		Выводим в порядке по списку..
		for i in csv_fields:
			k = i[1]
			try:
				out.write(" %s=\"%s\""%(k,lst[k]) )
			except KeyError,ValueError:
				pass
		
		out.write("/>\n")


# Save output
outf = 0

# если указан confugure.xml то тогда делаем замену секции <sensors>
if confile == "":
	outf = codecs.open(outfilename,'w',"utf-8")
	outf.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n")
	outf.write("<sensors generate_date='%s'>\n"%(datetime.datetime.today()) )
	save_sensors_list(outf)			
	outf.write("</sensors>\n")
	outf.close()
else:
	outf = codecs.open(outfilename,'w',"utf-8")
	inf = codecs.open(confile, "r", "utf-8")  
	skip_sensors_sec = False
	for line in inf.readlines():
	
		if line.find("</sensors") != -1:
			skip_sensors_sec = False
		
		if skip_sensors_sec == True:
			continue
		
		if line.find("<sensors") != -1:
			outf.write(line)
			save_sensors_list(outf)			
			skip_sensors_sec = True
			continue
		
		outf.write(line)

	outf.close()
	inf.close()

if verbose == True:
	print "items: %d"% len(outlist.items)
