#!/bin/sh
RETVAL=0
PROG="${0##*/}"
PROJECT=mpubalsam
#NODES="192.168.0.114 192.168.0.113"
NODES="gui gate"
LOGDIR=$HOME/logs
DEBUG=
SSHUSER=root

print_usage()
{
    [ "$1" = 0 ] || exec >&2
	cat <<EOF

Usage: $PROG [ $NODES ] | all

EOF
    [ -n "$1" ] && exit "$1" || exit
}

LIST=

[ "$1" == "all" ] && LIST=$NODES || LIST="$*"

[ -z "$LIST" ] && print_usage 1

KSSH=
[ -z "$DEBUG" ] && KSSH="-T" || KSSH=

PID=

HEAD=`date "+%Y-%m-%d %H:%M"`

mkdir -p $LOGDIR

# install
for n in $LIST; do
	echo "'$n' starting update..."

	LOG="$LOGDIR/$n.update.log"

	echo "                     ">>$LOG
	echo "                     ">>$LOG
	echo "=====================">>$LOG
	echo $HEAD>>$LOG
	echo "=====================">>$LOG
	if ssh -o StrictHostKeychecking=no $KSSH -x $SSHUSER@$n /usr/bin/ctl-update.sh $PROJECT 1>>$LOG 2>>$LOG; then
		echo "'$n' update OK">>$LOG 
		echo "'$n' update OK"
	else
		echo "'$n' update FAILED. See log '$LOG' for details..."
		echo "'$n' update FAILED. ">>$LOG
	fi &

	PID=$!
done

#wating
wait

exit $RETVAL
