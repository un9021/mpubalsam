%def_disable doc

Name: mpubalsam
Version: 1
Release: set76
Summary: MPU

License: GPL
Group: Development/C++
Url: http://setri.ru

Source: %name-%version.tar

Requires: libmpu3 >= 3.1-set20

# Automatically added by buildreq on Fri Apr 27 2012
# optimized out: fontconfig fontconfig-devel gcc-c++ glib2-devel libUCan2-devel libatk-devel libatkmm-devel libcairo-devel libcairomm-devel libcomedi-devel libcommoncpp2-devel libfreetype-devel libgdk-pixbuf libgdk-pixbuf-devel libgio-devel libglade-devel libglademm-devel libglibmm-devel libgtk+2-devel libgtkmm2-devel libmpu3-gate-mil libmpu3-gui libomniORB-devel libpango-devel libpangomm-devel libsigc++2-devel libstdc++-devel libuniset-devel libuniset-extensions libuniset-extensions-devel libwayland-client libwayland-server libxml2-devel pkg-config xorg-xproto-devel
BuildRequires: libmpu3-devel

%description
MPU. This is special internal project for SET Research Instinute

%package gui
Summary: Common package for MPU
Group: Development/Other
Requires: %name
Requires: autologin
Requires: xinit
Requires: dos2unix
Requires: libmpu3-gui
Requires: mpu3-gui

%description gui
GUI for for MPU

%package gate
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name
Requires: libmpu3-gate
Requires: mpu3-gate
Requires: libuniset2-extension-sqlite

%description gate
Converter station for MPU project

%package doc
Summary: Documentation for MPU
Group: Development/Other
Requires: %name = %version-%release

%description doc
Documentation for MPU Project

%prep
%setup

%build
%autoreconf
%if_enabled doc
	%configure --with-doxygen
%else
	%configure
%endif

#%make_build
%make

%install
%makeinstall
mkdir -p -m755 %buildroot/var/local/log/%name
mkdir -p -m755 %buildroot/var/local/run/%name
mkdir -p -m755 %buildroot/var/local/lock/%name/{system,alarmdb}
mkdir -p -m755 %buildroot/var/local/cache/%name/{system,alarmdb}
mkdir -p -m755 %buildroot/var/local/omniORB
mkdir -p -m755 %buildroot/var/local/%name
mkdir -p -m755 %buildroot/var/local/%name/db
mkdir -p -m644 %buildroot%_datadir/%name

mkdir -p -m755 %buildroot%_initdir
#mv %buildroot%_bindir/mpu %buildroot%_initdir/%name
#subst 's|SERVNAME=mpu|SERVNAME=%name|' %buildroot%_bindir/ctl-system.sh

mkdir -p -m755 %buildroot%_datadir/mpu3
echo "%name-%version-%release" > %buildroot%_datadir/mpu3/VERSION.txt

#mv %buildroot%_sysconfdir/%name/mpu.gate.monitrc %buildroot%_sysconfdir/%name/%name.gate.monitrc
#mv %buildroot%_sysconfdir/%name/mpu.gui.monitrc %buildroot%_sysconfdir/%name/%name.gui.monitrc
subst 's|\./\(.*\)\.png|%_datadir/mpu3/images/\1\.png|g' %buildroot%_datadir/%name/*.ui
subst 's|\./\(.*\)\.svg|%_datadir/mpu3/images/\1\.svg|g' %buildroot%_datadir/%name/*.ui


%post
ln -sf %_bindir/ctl-admin-%name.sh %_bindir/ctl-admin-mpu3.sh

# необходимо для скриптов ctl-XXXtohrml, и работы системы "сброса" журналов
ln -sf %_sysconfdir/%name/configure.xml %_sysconfdir/mpu3/configure.xml
mv -bf %_sysconfdir/%name/hosts %_sysconfdir/hosts
#mv -bf %_sysconfdir/mpu3/userconf.d/mpubalsam.conf %_sysconfdir/mpu3/userconf.d/mpu32220.conf.template
cat %_sysconfdir/openssh/authorized_keys2/mpubalsam.pub >> %_sysconfdir/openssh/authorized_keys2/root
uniq %_sysconfdir/openssh/authorized_keys2/root > %_sysconfdir/openssh/authorized_keys2/root2
mv -bf %_sysconfdir/openssh/authorized_keys2/root2 %_sysconfdir/openssh/authorized_keys2/root


%postun

%post -n %name-gui
sed -e "s|<LocalNode name=\".*\"|<LocalNode name=\"GUINode\"|i" %_sysconfdir/%name/configure.xml > %_sysconfdir/%name/configure.xml.o
mv -f %_sysconfdir/%name/configure.xml.o %_sysconfdir/%name/configure.xml
subst "s|SharedMemory1|SharedMemoryGUI|g" %_bindir/ctl-smviewer-%name.sh
subst "s|DiagnosticWindow=\"mpubalsam-gui.ui|DiagnosticWindow=\"/usr/share/mpubalsam/mpubalsam-gui.ui|g" %_sysconfdir/%name/guiconfigure.xml
subst "s|dlgSetVal=\"mpubalsam-gui.ui|dlgSetVal=\"/usr/share/mpubalsam/mpubalsam-gui.ui|g" %_sysconfdir/%name/guiconfigure.xml
subst "s|dlgPass=\"mpubalsam-gui.ui|dlgPass=\"/usr/share/mpubalsam/mpubalsam-gui.ui|g" %_sysconfdir/%name/guiconfigure.xml
ln -sf %_bindir/mpu3-gui-runlist.sh %_bindir/%name-runlist.sh
ln -sf %_bindir/ctl-admin-%name.sh %_bindir/ctl-admin-mpu3.sh
#ln -sf %_bindir/ctl-monit-gui.sh %_bindir/ctl-monit-%name.sh
ln -sf %_datadir/%name/alarmdb %_datadir/mpu3/alarmdb


%postun -n %name-gui
#rm -f %_bindir/%name-runlist.sh %_sysconfdir/%name/%name.monitrc
#rm -f %_bindir/ctl-monit-%name.sh
ln -sf %_datadir/mpu3/VERSION.txt %_datadir/%name/VERSION.txt

%post -n %name-gate
sed -e "s|<LocalNode name=\".*\"|<LocalNode name=\"GateNode\"|i" %_sysconfdir/%name/configure.xml | sed -e "s|/dev/hda|/dev/hdb|i" > %_sysconfdir/%name/configure.xml.o
mv -f %_sysconfdir/%name/configure.xml.o %_sysconfdir/%name/configure.xml
ln -sf %_bindir/mpu3-gate-runlist.sh %_bindir/%name-runlist.sh
ln -sf %_bindir/ctl-monit-gate.sh %_bindir/ctl-monit-%name.sh
ln -sf %_bindir/ctl-admin-%name.sh %_bindir/ctl-admin-mpu3.sh
#mv -f /home/guest/.ssh/mpubalsam_gate.key /home/guest/.ssh/mpubalsam_gate

%postun -n %name-gate
#rm -f %_bindir/%name-runlist.sh %_sysconfdir/%name/%name.monitrc
#rm -f %_bindir/ctl-monit-%name.sh

%files -n %name
#%config %_initdir/%name
%config %_sysconfdir/%name/configure.xml
%_datadir/mpu3/VERSION.txt
%_sysconfdir/%name/hosts
%config %_sysconfdir/mpu3/userconf.d/mpubalsam.conf
%config(noreplace) %_sysconfdir/openssh/authorized_keys2/mpubalsam.pub
#%config %_sysconfdir/openssh/sshd_config
%config(noreplace) %_sysconfdir/apt/sources.list.d/*

%_bindir/ctl-*
%exclude %_bindir/*gui*
%exclude %_bindir/*smemory*
#%_bindir/%{name}*

%dir %_datadir/%name/
%exclude %_datadir/%name/alarmdb
%exclude %_datadir/mpu3/images/*
%exclude %_datadir/%name/*.ui
#%exclude %_datadir/%name/images
%exclude %_sysconfdir/%name/gui*.xml
%exclude %_bindir/*gui*

%attr(0777,root,root) %dir /var/local/log/%name
%attr(0777,root,root) %dir /var/local/run/%name
%attr(0777,root,root) %dir /var/local/lock/%name
%attr(0777,root,root) %dir /var/local/cache/%name
%attr(0777,root,root) %dir /var/local/omniORB/

%files -n %name-gui
%_bindir/*gui*
%dir /home/guest/.ssh/
#%config(noreplace) %_bindir/*gui-runlist*
%config /home/guest/.ssh/mpubalsam_gate.key
%config /home/guest/.ssh/config

%_datadir/%name/alarmdb/*
%_datadir/mpu3/images/*
%_datadir/%name/*.ui
%config %_sysconfdir/%name/guiconfigure.xml


%files -n %name-gate
%_bindir/*smemory2*
%_bindir/%name-udp-test

#%config(noreplace) %_bindir/*gate-runlist*

%if_enabled doc
%files -n %name-doc
%_docdir/%name-%version/
%endif

%changelog
* Tue Apr 03 2018 Ura Nechaev <nechaev@server> 1-set56
- new build (commit: e7fad770f74935e682be1b4526b9125d5b422b44)

* Tue Apr 03 2018 Ura Nechaev <nechaev@server> 1-set54
- new build (commit: e7fad770f74935e682be1b4526b9125d5b422b44)

* Tue Apr 03 2018 Ura Nechaev <nechaev@server> 1-set52
- new build (commit: 1efde79aa07014c4dd820e59fd1a4a79913ef772)

* Mon Apr 02 2018 Ura Nechaev <nechaev@server> 1-set51
- new build (commit: 854d7444ac0992144405a0f851b7d0628cc2f684)

* Mon Apr 02 2018 Ura Nechaev <nechaev@server> 1-set50
- new build (commit: 07fe6b90850e5e5637da9bb796e1a141f3278e14)

* Mon Apr 02 2018 Ura Nechaev <nechaev@server> 1-set49
- new build (commit: 3d667eef5d0f532d939a770eaa6f7c2777550504)

* Sun Apr 01 2018 Ura Nechaev <nechaev@server> 1-set48
- new build (commit: d4aa26527205c256d825a96f53e2f42cf9b0059e)

* Sun Apr 01 2018 Ura Nechaev <nechaev@server> 1-set47
- new build (commit: 93e2fec5c190c4f055c5b89038bf9537e69d8ee6)

* Sat Mar 31 2018 Ura Nechaev <nechaev@server> 1-set46
- new build (commit: 5be62d52faab227fab48a4a204a39a9543c05c3e)

* Sat Mar 31 2018 Ura Nechaev <nechaev@server> 1-set45
- new build (commit: 37b877d21a1ba9b0f03e71ed95f754571b1d46de)

* Sat Mar 31 2018 Ura Nechaev <nechaev@server> 1-set44
- new build (commit: e7c756116595dc99f09f2b180ef3a6a616b0aa83)

* Fri Mar 30 2018 Ura Nechaev <nechaev@server> 1-set43
- new build (commit: 923a793552bf314d8b9a89e6e2a3bffd0c0fdbb0)

* Fri Mar 30 2018 Ura Nechaev <nechaev@server> 1-set41
- new build (commit: 6cdc50eab1bb401c90bf5a313d1df14eb25e447d)

* Thu Mar 29 2018 Ura Nechaev <nechaev@server> 1-set40
- new build (commit: 1e3f4b29f87029fc3676770e3fa184d8840ed703)

* Thu Mar 29 2018 Ura Nechaev <nechaev@server> 1-set39
- new build (commit: 1e3f4b29f87029fc3676770e3fa184d8840ed703)

* Sat Mar 24 2018 Ura Nechaev <nechaev@server> 1-set35
- new build (commit: 65dca6e776c283aba5780072ab6ddced2eb6aff2)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set34
- new build (commit: 12369c4a20cfd1b8af5403a9d3de1f3685b0b338)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set33
- new build (commit: 12369c4a20cfd1b8af5403a9d3de1f3685b0b338)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set32
- new build (commit: 12369c4a20cfd1b8af5403a9d3de1f3685b0b338)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set31
- new build (commit: 12369c4a20cfd1b8af5403a9d3de1f3685b0b338)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set30
- new build (commit: 12369c4a20cfd1b8af5403a9d3de1f3685b0b338)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set29
- new build (commit: a5e968c8b9c8617826ac37780fcbd8c8d3406386)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set28
- new build (commit: a5e968c8b9c8617826ac37780fcbd8c8d3406386)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set27
- new build (commit: ad92f640883bfd5e45f70a0a4d62e0c55e9c1bb9)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set26
- new build (commit: ea5939d48ef14beb69b82711e32dfef669b5a98e)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set25
- new build (commit: ea5939d48ef14beb69b82711e32dfef669b5a98e)

* Thu Dec 07 2017 Ura Nechaev <nechaev@server> 1-set24
- new build (commit: ea5939d48ef14beb69b82711e32dfef669b5a98e)

* Tue Dec 05 2017 Ura Nechaev <nechaev@server> 1-set22
- new build (commit: 748195a21cc97b5343b8ed0295750ac89a83ecab)

* Tue Dec 05 2017 Ura Nechaev <nechaev@server> 1-set21
- new build (commit: cb24322b80001d969f1bc29b5f05094763469308)

* Tue Dec 05 2017 Ura Nechaev <nechaev@server> 1-set20
- new build (commit: 52aed93d9eaf6ba0f4d243dc070cec7718bd2221)

* Sun Dec 03 2017 Ura Nechaev <nechaev@server> 1-set19
- new build (commit: 823e40aed3ea9570f1c40b7b2f16bc297d8fba25)

* Sun Dec 03 2017 Ura Nechaev <nechaev@server> 1-set18
- new build (commit: cb34e87e65ae6a871efd0af5671fb34115c6fe1b)

* Sun Dec 03 2017 Ura Nechaev <nechaev@server> 1-set17
- new build (commit: 5aeece62bab3aa0512992a76d7b4e8698adf9767)

* Sat Dec 02 2017 Ura Nechaev <nechaev@server> 1-set16
- new build (commit: b14cc1b6625f27c9167cf189373aaf7bce42cdd6)

* Sat Dec 02 2017 Ura Nechaev <nechaev@server> 1-set15
- new build (commit: 8ca07925ff5cd9697df088175fabf4ca38d78578)

* Sat Dec 02 2017 Ura Nechaev <nechaev@server> 1-set14
- new build (commit: 9444e02fe23bd81f5c75543069860c7e8badd8be)

* Sat Dec 02 2017 Ura Nechaev <nechaev@server> 1-set13
- new build (commit: 4aa48d9e1f31a1c081303d4d89bbb9573d328d8b)

* Fri Dec 01 2017 Ura Nechaev <nechaev@server> 1-set11
- new build (commit: dba117e7b95464c716a3e79dd41f6b8918f6928d)

* Fri Dec 01 2017 Ura Nechaev <nechaev@server> 1-set10
- new build (commit: 7dfe6de962d472fa96129c3bc98673fce3734863)

* Fri Dec 01 2017 Ura Nechaev <nechaev@server> 1-set9
- new build (commit: 7afd7c5013c28781ec50e59a5264a045c489525b)

* Thu Nov 30 2017 Ura Nechaev <nechaev@server> 1-set8
- new build (commit: 20ca165a9c6de63e5f14803a9f2f6544706feef1)

* Thu Nov 30 2017 Ura Nechaev <nechaev@server> 1-set7
- new build (commit: 8dd2ecec333d1a7fb9c0d42366b485bbcd853808)

* Thu Nov 30 2017 Ura Nechaev <nechaev@server> 1-set6
- new build (commit: 748fecd5a3834680d82c06551f1ae6497a6cf73b)

* Thu Nov 30 2017 Ura Nechaev <nechaev@server> 1-set5
- new build (commit: a4147f8ff6140338fe4af7db9f22ab82687aeadb)

* Thu Nov 30 2017 Ura Nechaev <nechaev@server> 1-set4
- new build (commit: e16206728fc0fe6066641796b16aebde57d8265a)

* Wed Nov 29 2017 Ura Nechaev <nechaev@server> 1-set2
- new build (commit: 3929a7296e002afbf97149a32454653689507ef7)

* Wed Nov 29 2017 Ura Nechaev <nechaev@server> 1-set1
- new build (commit: bcaabba9c163a5a55f1ae08aa63dc981ad016628)

* Wed Nov 29 2017 Ura Nechaev <nechaev@server> 1-set0
- new build (commit: 69a92cf68b63182a4f177142cedc193514871733)

* Wed May 06 2015 Vinogradov Aleksei <uzum664@gmail.com> 0.9-set37
- new build (commit: 1eb4ef908c09ad303a340c9828d014de4c1890c0)

* Wed May 06 2015 Vinogradov Aleksei <uzum664@gmail.com> 0.9-set36
- corrections in rpm 'long to short' translation

* Wed May 06 2015 Vinogradov Aleksei <uzum664@gmail.com> 0.9-set35
- udp-test updated

* Wed Apr 22 2015 Vinogradov Aleksei <uzum664@gmail.com> 0.9-set34
- rebuild(protocol corrections)

* Wed Apr 22 2015 Vinogradov Aleksei <uzum664@gmail.com> 0.9-set33
- rebuild with protocol corrections

* Fri Mar 20 2015 Pavel Vainerman <pv@etersoft.ru> 0.9-set32
- rebuild for new protocol and new libmpu

* Thu Aug 21 2014 Pavel Vainerman <pv@etersoft.ru> 0.9-set31
- new build (commit: db9b829c8ed576c8597424d5441dc731dd5b0793)

* Thu Aug 21 2014 Pavel Vainerman <pv@etersoft.ru> 0.9-set30
- new build (commit: bee476f7de89e30689bc68e1c8ab1ca3508ff5a5)

* Wed Aug 20 2014 Pavel Vainerman <pv@altlinux.ru> 0.9-set29f.1
- fixed bug #6215

* Thu Jul 31 2014 Pavel Vainerman <pv@etersoft.ru> 0.9-set29f
- float processing (commit: 088daf2405baf1b7f768efec16956deeac2b6673)

* Mon May 12 2014 Pavel Vainerman <pv@etersoft.ru> 0.9-set29
- new build (commit: bbd8d1397a391dd944cabac88bbf7689da82c147)

* Mon May 12 2014 Pavel Vainerman <pv@etersoft.ru> 0.9-set28
- new build (commit: 46d36babebb324eb4653607859758f9912a26a35)

* Tue May 06 2014 Pavel Vainerman <pv@etersoft.ru> 0.9-set27
- add intensity control (commit: 7f9cd956273ccbad6cc4871083498b907163baff)

* Mon Nov 19 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set25
- new build (commit: 9017f4470d04eba1ab6445933721b8a58ac44264)
- rebuild for new libmpu

* Thu Nov 15 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set24
- new build (commit: ae452097361cc16856be7382fa3477ac20f057e4)
- rebuild for new libmpu

* Tue Nov 06 2012 Pavel Vainerman <pv@altlinux.ru> 0.9-set23
- rebuild for new libmpu

* Wed Oct 24 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set22
- new build (commit: c07e393b92242e91ac0efcbc6ca64f5e7aab6f68)
- rebuild for new libmpu

* Mon Oct 22 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set21
- new build (commit: 7e1924c0a2d704c962c1e9857b2f673c0d6faa62)
- add w_BITREGxxx registers

* Mon Oct 22 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set20
- new build (commit: 7e1924c0a2d704c962c1e9857b2f673c0d6faa62)

* Thu Oct 18 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set19
- new build (commit: 8ae0cd1af05f1bfb6801062c44a6c0d97c8b90ae)
- fixed bug for 5159

* Tue Oct 09 2012 Pavel Vainerman <pv@altlinux.ru> 0.9-set18
- fixed bug in ControlWindow (last focus)

* Mon Oct 08 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set17
- new build (commit: b2cfe341b8859da6dd0b293565e08ac1af0c4c4b)
- new svu registers

* Thu Oct 04 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set16
- new build (commit: d1887f3dc3d65a7ba048e2b6c386c1576ffd3659)
- add new registers

* Fri Aug 31 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set15
- new build (commit: 34d6a31e58bd65ab1a4c3cb7560a742edc51c1e3)
- disable w_confTemp_xxx 

* Fri Aug 31 2012 Pavel Vainerman <pv@altlinux.ru> 0.9-set14
- new numeration registers (revert)
- fixed minor bug in SetValueDialog (off focus or keys)

* Thu Aug 30 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set13
- new build (commit: b5cdd613bbf386c4fa51ba100f383faf96b9dbdd)
- revert renumeration registers
- odd confirm dialog for Start and Stop

* Tue Aug 28 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set12
- new build (commit: fdf4742fd9377457a572a6fe32230240920128f3)

* Wed Aug 22 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set11
- new build (commit: f96102dad023736f5827355f6bee4c16522cad00)

* Wed Aug 22 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set10
- new build (commit: e691ff0cb4079c1e47fba64a367e8b0e6c940175)

* Tue Aug 21 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set9
- new build (commit: ae4c05449114887e7e1d92c26b81a5c66190758a)

* Tue Aug 21 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set8
- new build (commit: 527807db7db623e436b2e78800a83965461d75f3)

* Mon Aug 20 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set7
- new build (commit: 9553147da96a0b3da5918b3bbeeb1d1daf2338a3)

* Thu Aug 16 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set6
- new build (commit: 80854dd18830a15a6236d2ce23fc4268348fc7a5)

* Thu Aug 16 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set5
- new build (commit: 36bbd7e5e11b9661ad51597293bc2c83745d6787)

* Thu Aug 16 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set4
- new build (commit: 48ac90c42000366c95db1052c237187518d9a23f)

* Wed Aug 15 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set3
- new build (commit: 676d1678249b559d78e4103c4254acf3a2d4cb26)

* Wed Aug 15 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set2
- new build (commit: afb97e3c3a2168e6e0b6d5091e6c8567e4b53bf9)

* Thu Aug 09 2012 Pavel Vainerman <pv@etersoft.ru> 0.9-set1
- new build (commit: 3e5ba1ce1ee46a42fa7645a0328edc55180b2a1e)
- inital version for libmpu

* Sun May 13 2012 Pavel Vainerman <pv@etersoft.ru> 0.8-set6
- new build (commit: fe8547631436778a1a4b2ac490c26f67bba7b56e)

* Sun May 13 2012 Pavel Vainerman <pv@etersoft.ru> 0.8-set5
- new build (commit: fb22e9067fa9ebd5f709f91bad6b3bd8bec3d057)

* Thu Apr 26 2012 Pavel Vainerman <pv@etersoft.ru> 0.8-set4
- new build (commit: d3d7b41324ac395070103616690340e6cd467a70)

* Wed Apr 25 2012 Pavel Vainerman <pv@etersoft.ru> 0.8-set3
- new build (commit: 9553ebc57659ebce0e6b363a7eb4f44fa4b18ad5)

* Wed Apr 25 2012 Pavel Vainerman <pv@etersoft.ru> 0.8-set2
- new build (commit: 9553ebc57659ebce0e6b363a7eb4f44fa4b18ad5)

* Wed Apr 25 2012 Pavel Vainerman <pv@etersoft.ru> 0.8-set1
- new build (commit: 9553ebc57659ebce0e6b363a7eb4f44fa4b18ad5)

* Fri Apr 06 2012 Pavel Vainerman <pv@etersoft.ru> 0.7-set5
- new build (commit: 4ee35bf9bc5e25a246e7d67cbea5f37eda556ba4)

* Mon Apr 02 2012 Pavel Vainerman <pv@etersoft.ru> 0.7-set4
- add can filter mask (commit: 24af00db35f8c26e942183ea171b6e35bf7c1d93)

* Mon Apr 02 2012 Pavel Vainerman <pv@etersoft.ru> 0.7-set3
- new build (commit: 21e6c4b9d1f246478e5d48ebe20bce8b78229df2)

* Fri Mar 30 2012 Pavel Vainerman <pv@etersoft.ru> 0.7-set2
- new build (commit: e75ddc6a55c6fe1f9a05682d79b64a228b997025)

* Wed Mar 28 2012 Pavel Vainerman <pv@etersoft.ru> 0.7-set1
- new build (commit: 799a412ad4ba4e03423080371292d9966ec23452)

* Wed Mar 14 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set7
- new build

* Wed Mar 14 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set6
- new build

* Tue Mar 13 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set5
- new build

* Mon Mar 12 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set4
- new build

* Sun Mar 11 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set3
- new build

* Tue Mar 06 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set2
- new build

* Mon Mar 05 2012 Pavel Vainerman <pv@etersoft.ru> 0.6-set1
- new build

* Thu Mar 01 2012 Pavel Vainerman <pv@etersoft.ru> 0.5-set6
- fixed bug in CAN(RS) Exchange

* Wed Oct 12 2011 Pavel Vainerman <pv@etersoft.ru> 0.5-set5
- new build

* Sat Oct 08 2011 Pavel Vainerman <pv@altlinux.ru> 0.5-set4
- new build

* Fri Oct 07 2011 Pavel Vainerman <pv@etersoft.ru> 0.5-set3
- new build

* Fri Oct 07 2011 Pavel Vainerman <pv@etersoft.ru> 0.5-set2
- new build

* Fri Oct 07 2011 Pavel Vainerman <pv@etersoft.ru> 0.5-set1
- new build

* Fri Oct 07 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-set3
- new build

* Sun Aug 14 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-set2
- new build

* Thu Jun 23 2011 Pavel Vainerman <pv@etersoft.ru> 0.4-set1
- new build

* Wed May 11 2011 Pavel Vainerman <pv@etersoft.ru> 0.3-set2
- new build

* Fri May 06 2011 Pavel Vainerman <pv@etersoft.ru> 0.3-set1
- new build

* Fri May 06 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set24
- new build (new configure.xml)

* Fri May 06 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set23
- new build

* Thu May 05 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set22
- fixed bug in smemory2

* Thu May 05 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set21
- rebuild for new uniset

* Wed May 04 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set20
- rebuild for new uniset

* Wed May 04 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set19
- rebuild for new uniset

* Wed May 04 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set18
- new build

* Fri Apr 29 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set17
- new build

* Fri Apr 29 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set16
- new test build

* Thu Apr 28 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set15
- new build

* Wed Apr 27 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set14
- new build

* Sun Mar 27 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set13
- new build

* Sun Mar 27 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set12
- test build

* Sun Mar 27 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set11
- test build

* Sun Mar 27 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set10
- test build

* Sat Mar 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set9
- test build

* Sat Mar 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set8
- test build

* Sat Mar 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set7
- test build

* Sat Mar 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set6
- test build

* Sat Mar 26 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set5
- test build

* Mon Jan 31 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set4
- test build

* Fri Jan 14 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set3
- test build

* Fri Jan 14 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set2
- test build

* Fri Jan 14 2011 Pavel Vainerman <pv@etersoft.ru> 0.2-set1
- test build

* Fri Jan 14 2011 Pavel Vainerman <pv@etersoft.ru> 0.1-set4
- test build

* Fri Jan 14 2011 Pavel Vainerman <pv@etersoft.ru> 0.1-set3
- test build

* Fri Dec 17 2010 Pavel Vainerman <pv@altlinux.ru> 0.1-set2
- test build

* Thu Dec 16 2010 Pavel Vainerman <pv@altlinux.ru> 0.1-set1
- test build

