#!/bin/sh
RETVAL=0
PROG="${0##*/}"
NODES="gui gate"
LOGDIR=$HOME/logs
DEBUG=
SSHUSER=root
CMD="$1"

print_usage()
{
    [ "$1" = 0 ] || exec >&2
	cat <<EOF

Usage: $PROG [ start|stop|restart|reboot|poweroff ] [ $NODES | all ]

EOF
    [ -n "$1" ] && exit "$1" || exit
}

LIST=


if [ "$2" == "all" ]; then
	LIST=$NODES 
else
	for p in `seq 2 14`; do
		eval t=\$$p
		[ -z "$t" ] && break
		LIST="$LIST $t" 
	done

fi

[ -z "$LIST" ] && print_usage 1
[ -z "$CMD" ] && print_usage 1

KSSH=
[ -z "$DEBUG" ] && KSSH="-T" || KSSH=

PID=

HEAD=`date "+%Y-%m-%d %H:%M"`

mkdir -p $LOGDIR

# install
for n in $LIST; do
	
	echo "'$n' run command '$CMD'..."

	LOG="$LOGDIR/$n.command.log"

	echo "                     ">>$LOG
	echo "                     ">>$LOG
	echo "=====================">>$LOG
	echo $HEAD>>$LOG
	echo "=====================">>$LOG
	if ssh -o StrictHostKeychecking=no $KSSH -x $SSHUSER@$n /usr/bin/ctl-system.sh $CMD 1>>$LOG 2>>$LOG; then
		echo "'$n' $CMD OK">>$LOG 
		echo "'$n' $CMD OK"
	else
		echo "'$n' $CMD FAILED. See log '$LOG' for details..."
		echo "'$n' $CMD FAILED. ">>$LOG
	fi &

	PID=$!
done

wait

exit $RETVAL
