#include <libglademm.h>
#include <sstream>
#include "KeyCode.h"
#include "VObject.h"
#include "MyMainFrame.h"
#include "MPUConfiguration.h"
// -------------------------------------------------------------------------
using namespace uniset;
//using namespace UniSetGraphics;
using namespace MPU;
// -------------------------------------------------------------------------
//extern UniXML* guixml;
extern Glib::RefPtr<Gnome::Glade::Xml> refXml;
//extern Gtk::Window* pMainWindow;
// -------------------------------------------------------------------------
MainFrame::MainFrame( const string cname, std::shared_ptr<uniset::SMInterface>& shm ):
	ChildFrame(cname),
	vbMain(NULL),
	btnDriveConf(NULL),
	btnFQCConf(NULL),
	btnLSUConf(NULL),
	btnTransConf(NULL),
	btnCoolingConf(NULL),
	drive(NULL),
	fqc(NULL),
	lsu(NULL),
	trans(NULL),
	cooling(NULL),
	winDrive(NULL),
	winFQC(NULL),
	winLSU(NULL),
	winTrans(NULL),
	winCooling(NULL),
	lastBtn(NULL)
{
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();
	xmlNode* cnode = guixml->findNode(guixml->getFirstNode(), "MainFrame");

	if( cnode == NULL )
		throw SystemError("Not find xml-confnode for " + cname);

	Gtk::VBox* vbMainExt = 0;
	{
		GETWIDGET(vbMainExt, "vbMainWindow");
		GETWIDGET(vbMain, "vbMainFrame");
		vbMain->reparent(*vbMainExt);
	}

	vbMainDefault = 0;
	GETWIDGET(vbMainDefault, "MainFrame_vbDefault");

	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	drive 	= new CommonVO("Drive_VO", shm, refXml);
	fqc 	= new CommonVO("FQC_VO", shm, refXml);
	lsu 	= new CommonVO("LSU_VO", shm, refXml);
	trans 	= new CommonVO("Trans_VO", shm, refXml);
	cooling = new CommonVO("Cooling_VO", shm, refXml);

	GETWIDGET(btnDriveConf, "btnDriveConf");
	btnDriveConf->signal_clicked().connect(sigc::mem_fun(*this, &MainFrame::btnDriveConf_clicked));
	SET_FOCUS_SIG(MainFrame, btnDriveConf);
	////GUIConf::setFocusStyle(btnDriveConf);
	btnAttach( btnDriveConf, KEY_2);
	btnAttach( btnDriveConf, KEY_m2);

	GETWIDGET(btnFQCConf, "btnFQCConf");
	btnFQCConf->signal_clicked().connect(sigc::mem_fun(*this, &MainFrame::btnFQCConf_clicked));
	SET_FOCUS_SIG(MainFrame, btnFQCConf);
	////GUIConf::setFocusStyle(btnFQCConf);
	btnAttach( btnFQCConf, KEY_3);
	btnAttach( btnFQCConf, KEY_m3);

	GETWIDGET(btnLSUConf, "btnLSUConf");
	btnLSUConf->signal_clicked().connect(sigc::mem_fun(*this, &MainFrame::btnLSUConf_clicked));
	SET_FOCUS_SIG(MainFrame, btnLSUConf);
	//GUIConf::setFocusStyle(btnLSUConf);
	btnAttach( btnLSUConf, KEY_4);
	btnAttach( btnLSUConf, KEY_m4);

	GETWIDGET(btnTransConf, "btnTransConf");
	btnTransConf->signal_clicked().connect(sigc::mem_fun(*this, &MainFrame::btnTransConf_clicked));
	SET_FOCUS_SIG(MainFrame, btnTransConf);
	////GUIConf::setFocusStyle(btnTransConf);
	btnAttach( btnTransConf, KEY_5);
	btnAttach( btnTransConf, KEY_m5);

	GETWIDGET(btnCoolingConf, "btnCoolingConf");
	btnCoolingConf->signal_clicked().connect(sigc::mem_fun(*this, &MainFrame::btnCoolingConf_clicked));
	SET_FOCUS_SIG(MainFrame, btnCoolingConf);
	////GUIConf::setFocusStyle(btnCoolingConf);
	//btnAttach( btnTransConf,KEY_5);
	//btnAttach( btnTransConf,KEY_m5);

	GETWIDGET(btnControl, "btnControl");
	btnAttach(btnControl, KEY_1);
	btnAttach(btnControl, KEY_m1);

	//	vbExt=0;
	//	GETWIDGET(vbExt,"MainFrame_vbExt");

	winDrive = new DeviceWindow("DriveViewParamList", vbMainExt, "DriveParam_VO", shm);
	winDrive->hide();

	winFQC = new DeviceWindow("FQCViewParamList", vbMainExt, "FQCParam_VO", shm);
	winFQC->hide();

	//winFQC = new MPU::BookSensors("FQCViewParamList", vbMainExt, shm);
	//winFQC->init();
	//winFQC->hide();

	winLSU = new DeviceWindow("LSUViewParamList", vbMainExt, "LSUParam_VO", shm);
	winLSU->hide();

	winTrans = new DeviceWindow("TransViewParamList", vbMainExt, "TransParam_VO", shm);
	winTrans->hide();

	winCooling = new DeviceWindow("CoolingViewParamList", vbMainExt, "CoolingParam_VO", shm);
	winCooling->hide();

	win_activate(); // Это главное окно поэтому его активируем сразу...

	//pMainWindow->signal_key_press_event().connect(sigc::mem_fun(*this,&MainFrame::on_key_press_event));
}

// -------------------------------------------------------------------------
MainFrame::~MainFrame()
{
	delete drive;
	delete fqc;
	delete lsu;
	delete trans;
	delete winDrive;
	delete winFQC;
	delete winLSU;
	delete winTrans;
	delete winCooling;
}
// -------------------------------------------------------------------------
Gtk::Widget* MainFrame::get_widget()
{
	return vbMain;
}
// -------------------------------------------------------------------------
void MainFrame::show()
{
	init_first_time();
	vbMain->show();

	if( btnControl )
		btnControl->show();

	vbMainDefault->show();

	if( lastBtn == NULL && btnControl )
		btnControl->grab_focus();
	else
		lastBtn->grab_focus();
}
// -------------------------------------------------------------------------

// чтобы окна на главном окне инициализировались сразу
// делаем принудительный вызов функций init_first_time();
// т.е. замедляется первоначальный старт, но зато потом в процессе работы
// окна будут открываться быстрее
void MainFrame::init_first_time()
{
	winDrive->init_first();
	winFQC->init_first();
	winLSU->init_first();
	winTrans->init_first();
	winCooling->init_first();
	drive->init_first_time();
	fqc->init_first_time();
	lsu->init_first_time();
	trans->init_first_time();
	cooling->init_first_time();
}
// -------------------------------------------------------------------------

void MainFrame::hide()
{
	vbMain->hide();

	if( btnControl )
		btnControl->hide();

	vbMainDefault->hide();
	winDrive->hide();
	winFQC->hide();
	winLSU->hide();
	winTrans->hide();
	winCooling->hide();
}
// -------------------------------------------------------------------------
void MainFrame::show_child( MPU::DeviceWindow* win )
{
	win->win_activate();
}
// -------------------------------------------------------------------------
bool MainFrame::on_key_press_event( GdkEventKey* event )
{
	if( event->keyval == KEY_ESC )
	{
		if( btnControl && btnControl->is_visible() )
		{
			btnControl->grab_focus();
			lastBtn = btnControl;
		}
	}

	return false;
}
// -------------------------------------------------------------------------
void MainFrame::btnDriveConf_clicked()
{
	dlog[Debug::INFO] << myguiname << ": btnDriveConf_clicked..." << endl;
	lastBtn = btnDriveConf;
	show_child(winDrive);
}
// -------------------------------------------------------------------------

void MainFrame::btnFQCConf_clicked()
{
	dlog[Debug::INFO] << myguiname << ": btnFQCConf_clicked..." << endl;
	lastBtn = btnFQCConf;
	show_child(winFQC);
	//vbMain->hide();
	//winFQC->show();
}
// -------------------------------------------------------------------------
void MainFrame::btnLSUConf_clicked()
{
	dlog[Debug::INFO] << myguiname << ": btnLSUConf_clicked..." << endl;
	lastBtn = btnLSUConf;
	show_child(winLSU);
}
// -------------------------------------------------------------------------
void MainFrame::btnTransConf_clicked()
{
	dlog[Debug::INFO] << myguiname << ": btnTransConf_clicked..." << endl;
	lastBtn = btnTransConf;
	show_child(winTrans);
}
// -------------------------------------------------------------------------
void MainFrame::btnCoolingConf_clicked()
{
	dlog[Debug::INFO] << myguiname << ": btnCoolingConf_clicked..." << endl;
	lastBtn = btnCoolingConf;
	show_child(winCooling);
}
// -------------------------------------------------------------------------
// дизайн (реализация подсвечивания кнопок)
// макросы см. GUIConfiguration.h
SET_FOCUS_FUNC_IMPL( MainFrame, btnDriveConf )
SET_FOCUS_FUNC_IMPL( MainFrame, btnFQCConf )
SET_FOCUS_FUNC_IMPL( MainFrame, btnLSUConf )
SET_FOCUS_FUNC_IMPL( MainFrame, btnTransConf )
SET_FOCUS_FUNC_IMPL( MainFrame, btnCoolingConf )
// -------------------------------------------------------------------------
void MainFrame::poll()
{
	//	if ( !is_show() )
	//		return;
	winFQC->poll();
	drive->poll();
	fqc->poll();
	lsu->poll();
	trans->poll();
	cooling->poll();
}
// -------------------------------------------------------------------------
void MainFrame::initIterators()
{
	drive->initIterators();
	fqc->initIterators();
	lsu->initIterators();
	trans->initIterators();
	cooling->initIterators();
	winDrive->initIterators();
	winFQC->initIterators();
	winLSU->initIterators();
	winTrans->initIterators();
	winCooling->initIterators();
}
// -------------------------------------------------------------------------
void MainFrame::getList(ParamList::ValList* lst)
{
	winDrive->getList(lst);
	winFQC->getList(lst);
	winLSU->getList(lst);
	winTrans->getList(lst);
	winCooling->getList(lst);
}
// ----------------------------------------------------------------------
