/*! $Id: MainFrame.h,v 1.15 2008/07/22 07:44:03 pv Exp $ */
// -------------------------------------------------------------------------
#ifndef MainFrame_H_
#define MainFrame_H_
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <string>
#include <UniSetTypes.h>
#include <UniXML.h>
#include <GUIConfiguration.h>
#include "ExtEntry.h"
#include <BookSensors.h>
#include <ChildFrame.h>
#include <PasswordDialog.h>
#include <CommonVO.h>
#include <DeviceWindow.h>
#include <ParamList.h>
// -------------------------------------------------------------------------
/*! Экран основных параметров */
using namespace MPU;
using namespace std;
class MainFrame:
	public ChildFrame
{
	public:
		MainFrame( const string cname, std::shared_ptr<uniset::SMInterface>& shm );
		virtual ~MainFrame();

		virtual void poll();
		virtual void initIterators();
		virtual void show();
		virtual void hide();
		virtual Gtk::Widget* get_widget();

		inline bool is_mapped()
		{
			return vbMain->is_mapped();
		}

		void getList(MPU::ParamList::ValList* lst);

	protected:

		virtual void init_first_time();
		void btnDriveConf_clicked();
		void btnFQCConf_clicked();
		void btnLSUConf_clicked();
		void btnTransConf_clicked();
		void btnCoolingConf_clicked();
		void show_child( MPU::DeviceWindow* win );
		bool on_key_press_event( GdkEventKey* event );

		// дизайн (реализация подсвечивания кнопок)
		// макросы см. GUIConfiguration.h
		SET_FOCUS_FUNC( btnDriveConf );
		SET_FOCUS_FUNC( btnFQCConf );
		SET_FOCUS_FUNC( btnLSUConf );
		SET_FOCUS_FUNC( btnTransConf );
		SET_FOCUS_FUNC( btnCoolingConf );

		Gtk::VBox* vbMain;
		Gtk::VBox* vbMainDefault;
		Gtk::Button* btnDriveConf;
		Gtk::Button* btnFQCConf;
		Gtk::Button* btnLSUConf;
		Gtk::Button* btnTransConf;
		Gtk::Button* btnCoolingConf;

		MPU::CommonVO* drive; 	/*!< параметры двигателя */
		MPU::CommonVO* fqc;		/*!< параметры преобразователя */
		MPU::CommonVO* lsu;		/*!< параметры ЛСУ */
		MPU::CommonVO* trans;	/*!< параметры трансформатора */
		MPU::CommonVO* cooling;	/*!< параметры системы охлаждения */

		MPU::DeviceWindow* winDrive; 	/*!< окно текущих параметров двигателя */
		MPU::DeviceWindow* winFQC; 		/*!< окно текущих параметров преобразователя */
		MPU::DeviceWindow* winLSU; 		/*!< окно текущих параметров ЛСУ */
		MPU::DeviceWindow* winTrans; 	/*!< окно текущих параметров Трансформатора */
		MPU::DeviceWindow* winCooling; 	/*!< окно текущих параметров "охлаждение" */
		Gtk::Button* btnControl;

		Gtk::Button* lastBtn;		/*!< указатель на кнопку, которая была последняя нажата */

	private:
		Gtk::Window* pMainWindow;

};
// -------------------------------------------------------------------------
#endif // MainFrame_H_
// -------------------------------------------------------------------------
