#ifndef _MainWindow_H_
#define _MainWindow_H_
// -------------------------------------------------------------------------
#include <Trigger.h>
#include <ProxyManager.h>
#include <IONotifyController.h>
#include <UInterface.h>
#include <BaseMainWindow.h>
#include <UWidgets.h>
#include <UImage.h>
#include <gtkmm.h>
#include <extensions/SMInterface.h>
#include <WarningsWindow.h>
#include <WarningsJournal.h>
#include "LedMeterVO.h"
#include "CommonVO.h"
#include "StateVO.h"
#include "PasswordDialog.h"
#include "SetValueDialog.h"
#include "WinManager.h"
#include "NetStateVO.h"
#include "GUIConfiguration.h"
#include "ProcessProgress.h"
#include "ExtMessageDialog.h"
#include "FlashLed.h"
#include "DebugWindow.h"
#include "ParamList.h"
#include "MyControlWindow.h"
#include "MyMainFrame.h"
#include "DebugWindow.h"
#include <sstream>
#include <time.h>
#include <error.h>
#include <errno.h>
#include <fstream>
#include <libglademm.h>
#include <Exceptions.h>
#include "MPUConfiguration.h"
#include <UniSetActivator.h>
#include "Journal.h"
#include "KeyCode.h"

namespace MPU
{
	class WarningsJournal;
}

#define BUTTONS_BLOCK_TIMEOUT 10
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
/*!
	Главное окно графического интерфейса.
	В задачи входит:
		- обработка всех приходящих сообщений
		- управление переключением окон
		- и т.п.
	\warning Обработка сообщений ведётся по Gtk-таймеру. Т.к. если создавать собственный поток,
	то он не синхронизирован с gtk-потоком и это вызывает проблеммы с отрисовкой.
*/
class MainWindow:
	public MPU::BaseMainWindow
{
	public:
		virtual ~MainWindow();

		//static void init_gui( int argc, char *argv[] );
		static Glib::RefPtr<MainWindow> Instance();
		static Glib::RefPtr<MainWindow> init_gui( uniset::ObjectId id, int argc, char* argv[], std::shared_ptr<uniset::IONotifyController>& nc );

		virtual Gtk::Window* getWin();
		void initIterators();
		//void activate();

		const std::string getSaveDir();
		bool createDir( const std::string dir );
		const std::string getBackupDir();

		typedef sigc::signal<void, long> FlashStateChange_Signal;
		inline FlashStateChange_Signal signal_flash_state()
		{
			return m_flash_signal;
		}

		void beep();

		/*! Установить блокировку кнопок работы с флеш */
		void setFlashButtonsBlock(bool state);

		/*! Возвращает true, если заблокированы кнопки работы с флеш */
		bool getFlashButtonsBlock();

		/*! Функция сохраняет значения сенсоров на диск и возвращает путь сохранения */
		string saveSensors();

	protected:
		uniset::UniSetActivator* act;
		MainWindow( uniset::ObjectId id, std::shared_ptr<uniset::IONotifyController>& nc );

		LedMeterVO* createLed( const string idname, const string wname, const string sname );
		/*!
			\page MPU_buttons Назначение кнопок проекта МПУ
				\section Main_buttons Кнопки главного окна
				- \ref btnControl
			\par
			Открывает окно управления (для входа необходимо ввести пароль)
			\image html control_button.png

				- \ref btnJrn
			\par
			Открывает окно со списком всех неисправностей.
			\image html journal_button.png

				- \ref btnSnap
			\par
			Кнопка с изображением камеры (досупна при подключении съёмного носителя).
			Скриншоты сохраняются в папку mpu/#дата#/screenshots на флеш-диске
			\image html snap_button.png

				- \ref btnSave
			\par
			Функция вызывается при нажатии кнопки, обозначенной "флешкой".
			По нажатию сохраняется журнал аварий, дамп параметров и все датчики.
			Данные сохраняются в папку mpu/#дата# на флеш-диске (3 папки:
			alarms, logs и sensors). Копирование запускается в фоне и может требовать
			длительное время на выполнение.
			\image html flash_button.png

			    - \ref btnWarn
			\par
			Открывает окно со списком текущих неисправностей.
			\image html warning_button.png
		*/

		/*!
			\section btnControl Открыть окно управления
		*/
		void btnControl_clicked();
		void btnMnemoExit_clicked();
		bool btnMnemoShow_clicked(GdkEvent* event);
		bool btnBigMnemoExit_clicked(GdkEvent* event);
		void btnExit_clicked();
		void on_showAlarmJournal(  const std::string& code, const std::string& datetime, const std::string& numcon, const std::string& bitreg );

		/*!
			\section btnJrn Открыть окно журнала аварий
		*/
		void btnJrn_clicked();

		bool setTime();
		bool setDate();
		//bool poll();

		void poll();
		void myPoll();

		virtual void sensorInfo( uniset::SensorMessage* sm );
		void updateDS( uniset::ObjectId sid, bool& prev, uniset::IOController::IOStateList::iterator& dit );

	private:

		static Glib::RefPtr<MainWindow> inst;
		bool on_destroy(GdkEventAny* evnt);
		bool on_key_press_event( GdkEventKey* event );
		void on_flash_state( long state );

		Gtk::Window* pMainWindow;

		void checkRespond();

		// дизайн (реализация подсвечивания кнопок)
		// макросы см. GUIConfiguration.h
		SET_FOCUS_FUNC( btnControl );
		SET_FOCUS_FUNC( btnJrn );
		SET_FOCUS_FUNC( btnExit );
		SET_FOCUS_FUNC( btnSave );
		SET_FOCUS_FUNC( btnNow );
		SET_FOCUS_FUNC( btnArch );

		uniset::ObjectId idNetCommRestart_FS;	/*!< датчик рестарта процесса обмена с "конвертором" */

		LedMeterVO* ledCurGED_RPM;		/*!< текущие обороты ГЭД */
		LedMeterVO* ledTargetGED_RPM;	/*!< заданные(целевые) обороты ГЭД */
		LedMeterVO* ledCurrent;			/*!< ток */
		LedMeterVO* ledPower;			/*!< мощность */
		LedMeterVO* ledVoltage;			/*!< напряжение */
		//		LedMeterVO* ledPowerLim;		/*!< ограничение мощности */
		LedMeterVO* ledCurrent2;
		LedMeterVO* ledVoltage2;


		typedef list<LedMeterVO*> LEDList;
		LEDList ledlist;

		FlashLed* flash;

		Gtk::Label* lblTime;
		Gtk::Label* lblDate;

		Gtk::Button* btnControl;
		Gtk::Button* btnJrn;
		Gtk::Button* btnExit;
		Gtk::Button* btnMnemoExit;

		Gtk::Button* btnArch;
		Gtk::Button* btnNow;

		void btnArch_clicked();
		void btnNow_clicked();

		Gtk::Fixed* fixLeft;
		Gtk::Fixed* fixRight;
		Gtk::Fixed* fixState;
		Gtk::Fixed* fixMode;
		Gtk::Fixed* fixInfo;
		Gtk::VBox* mainscreen;
		Gtk::VBox* bigmnemo;
		Gtk::VBox* vbMW;
		Gtk::VBox* vbAW;
		Gtk::VBox* jrnbox;
		Gtk::VBox* enwin;
		Gtk::Notebook* jrnbook;
		bool alarm;
		Gtk::EventBox* evboxmnemo;
		Gtk::EventBox* evboxbigmnemo;
		bool netRespond, lsuRespond;
		uniset::ObjectId idNetRespond_s;
		uniset::ObjectId idLSURespond_s;
		uniset::IOController::IOStateList::iterator ditNetRespond_s, ditLSURespond_s, ditNetRestart_fs, ditPowLim;

		sigc::connection connPoll;
		sigc::connection connTime;
		sigc::connection connDate;
		sigc::connection connActivate;
		sigc::connection connWaitForMount;

		int pollTime;
		int pollCountMsg;

		// check password
		Glib::RefPtr<PasswordDialog> dlgPass;
		//bool checkPassword( MPU::PassID p_id, int maxCount, const string title="" );
		int maxCheckPassCount;

		// дополнительные окна
		Glib::RefPtr<MPU::SetValueDialog>  dlgSetVal;

		Glib::RefPtr<WinManager> wman;
		Glib::RefPtr<WarningsWindow> wwin;
		Glib::RefPtr<MPU::WarningsJournal> jwin;
		Glib::RefPtr<AlarmWindow> awin;

		std::string myname;
		uniset::UInterface uin;
		uniset::SensorMessage sm;
		bool prevPowerLimit;
		bool prevNetRestart;

		MainFrame* mainFrame;
		ControlWindow* cwin; 		// окно управления (с экрана)

		bool noLSUCheckRespond;		// признак отключения "реакции на пропажу связи"
		bool noConvCheckRespond;		// признак отключения "реакции на пропажу связи"

		ProcessProgress* sp;
		bool initOK;
		int tm_msec;
		int psplash;
		int tm_checknetwork_msec;
		uniset::Trigger trCheckLink;
		uniset::Trigger trChangeLink;

		ExtMessageDialog* md;
		std::string nolink_txt;
		//uniset::SMInterface shm;

		// --------SensorsFile---------
		std::string backupSensors;
		std::string backupDir;

		xmlNode* onflashNode;
		void flashOnEvent();
		FlashStateChange_Signal m_flash_signal;
		// ------------------------------

		// Debug window
		DebugWindow* dwin;

		// ---- screen saver -----------
		bool screen_saver_state;
		sigc::connection connScreenSaver;
		int ss_msec;
		bool keyIsPressed;
		bool on_timer_screen_saver();
		void set_screen_saver( bool state );
		void on_new_warning( const Gtk::TreeModel::iterator& );
		void on_showAlarmWarning(  const std::string& code, const std::string& datetime, const std::string& numcon, const std::string& bitreg );
		bool on_my_button_press_event( GdkEventButton* event );
		std::string ss_script; // screen save script

		std::string flash_script;
		int waiting_for_mount;
		bool waitingForMount();
};
// -------------------------------------------------------------------------
#endif
// -------------------------------------------------------------------------
