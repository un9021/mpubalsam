// -------------------------------------------------------------------------
#ifndef ControlWindow_H_
#define ControlWindow_H_
// -------------------------------------------------------------------------
#include <list>
#include <map>
#include <gtkmm.h>
#include <UniSetTypes.h>
#include <UWidgets.h>
#include <UniXML.h>
#include <TriggerOUT.h>
#include <WarningsJournal.h>
#include "ExtEntry.h"
#include "ChildFrame.h"
#include "ControlWindowSpec.h"
#include "Journal.h"
#include "VObject.h"
#include "GUIConfiguration.h"
#include "MainWindow.h"
#include "SetValueDialog.h"
#include <sstream>
#include <libglademm.h>
#include <extensions/Extensions.h>
#include "KeyCode.h"
#include "UIndicator.h"
#include "VObject.h"
#include "MPUConfiguration.h"
#include "MPUTypes.h"
#include "KeyCode.h"


class MainWindow;
namespace MPU
{
	class ControlWindowSpec;
}
// -------------------------------------------------------------------------
/*
	Note: пока сюда переносятся выполненные вещи из TODO, которые потом пойдут в описание...

- ПОКА не выбран МПУ кнопки ПУСК,СТОП, ввод оборотов должны быть блокированы.
- "Задание принято" начинает мигать, когда пришло подтверждение о передаче
сообщения в контроллер...

*/

// -------------------------------------------------------------------------
/*!
	Экран управления.
*/
namespace MPU
{
	class ControlWindow:
		public ChildFrame,
		public VObject
	{
		public:
			ControlWindow( const string idname, std::shared_ptr<uniset::SMInterface>& shm, MainWindow* mwin );
			virtual ~ControlWindow();

			virtual void poll();
			virtual void initIterators();

			virtual void show();
			virtual void hide();
			virtual Gtk::Widget* get_widget();

			inline long getRPMPrecision()
			{
				return (kRPM / 10);
			}

		protected:
			virtual void sensorInfo( uniset::SensorMessage* sm );
			virtual void init_first_time();

			void update_set_rpm( long val_rpm );
			void rpm_entry_finish_input( Gtk::ResponseType res );
			void int_entry_finish_input( Gtk::ResponseType res );
			void update_setINT( long val_rpm );
			/*
					bool rpm_entry_focus_out(GdkEventFocus*);
					void rpm_entry_start_editing();
			*/
			void rpm_btn_on_click();
			void rpm_entry_set(bool state);
			void int_entry_set(bool state);
			void set_control(bool state);
			bool unlockExit();
			bool startTimeout();
			bool stopTimeout();
			bool stopGEDTimeout();
			bool setTimeout();
			bool setINTTimeout();
			void setOut( uniset::ObjectId sid, bool state );
			void setRPM( long val );
			void setINT( long val );

			void btnMode11_clicked();
			void btnMode2_clicked();
			void btnMode121_clicked();
			void btnMode131_clicked();
			void btnMode122_clicked();
			void btnMode132_clicked();
			void btnMode3_clicked();

			void btnOpenPump_clicked();
			void btnClosePump_clicked();

			long str2RPM( const string& s );

			void btnStart_clicked();
			void btnStop_clicked();
			void btnKeyboard_clicked();
			void btnKeyboardGV_clicked();
			void btnKeyboardINT_clicked();
			bool btnConfirm(string ask);
			void btnInfo();
			void on_msg_finish_blink();
			void btnSpecWin_clicked();

			void setDefaultMode();

			bool confirmCommand();

			UIndicator* tp;	/*!< транспарант 'ошибок ввода' */

			enum SetErrors
			{
				erNone,
				erTimeout,
				erBadValue,
				erInternal
			};

			struct tpInfo
			{
				std::string text;
				std::string font;
				Glib::ustring bgcolor;
				Glib::ustring fgcolor;
			};

			tpInfo tpDefault;	// ожидание ввода
			tpInfo tpOK;		// команда принята
			tpInfo tpRepeat;	// повторите ввод
			tpInfo tpTimeout;	// нет связи
			tpInfo tpBadValue;	// некорректное значение
			tpInfo tpInternal;	// внутренняя ошибка
			tpInfo tpFailed;	// несанкионированный доступ
			tpInfo tpOff;		// управление отключено
			tpInfo tpStartFailed;	// команда 'Старт' не прошла
			tpInfo tpStopFailed;	// команда 'Стоп' не прошла
			tpInfo tpStopGEDFailed;	// команда 'Откл. ГЭД' не прошла
			tpInfo tpStart;		// команда 'Старт'
			tpInfo tpStop;		// команда 'Стоп'
			tpInfo tpStopGED;	// команда 'Откл. ГЭД'
			tpInfo tpOffRPM;	// управление оборотами отключено

			struct TPStateInfo
			{
				std::string text;
				std::string font;
				std::string bgcolor;
				std::string fgcolor;
			};

			typedef std::map<long, TPStateInfo> TPStateMap;

			UIndicator* tpPost;	/*!< транспарант 'выбранный пост управления' */
			TPStateMap tpPostMap;	/*!< карата соответсвия состояния и надписи для индикатора екущего поста */

			void init( xmlNode* cnode, tpInfo* ti, const string name );
			void tp_set_state(tpInfo& ti, bool state = false);

			Gtk::VBox* vbMain;
			Gtk::VBox* jrnbox;
			Gtk::Entry* entRPM;
			Gtk::Entry* entGV;
			Gtk::Entry* entINT;
			Gtk::Button* btnStart;
			Gtk::Button* btnStop;
			Gtk::Button* btnKeyboard;
			Gtk::Button* btnKeyboardGV;
			Gtk::Button* btnKeyboardINT;
			/*MODE*/
			Gtk::Button* btnMode11;
			Gtk::Button* btnMode2;
			Gtk::Button* btnMode121;
			Gtk::Button* btnMode131;
			Gtk::Button* btnMode122;
			Gtk::Button* btnMode132;
			Gtk::Button* btnMode3;
			/*MODE*/

			Gtk::Label* lbllfider;
			Gtk::Label* lblrfider;

			Gtk::Button* btnOpenPump;
			Gtk::Button* btnClosePump;

			Gtk::Table* vbKeyBox;
			Gtk::Table* vbModeBox;
			Gtk::Button* btnSpecial;
			Gtk::Button* btnMode;
			Gtk::Button* btnSpeed;
			Gtk::Button* btnSControl;
			Gtk::Button* btnTest9;
			Gtk::Button* btnTest18;
			Gtk::Button* btnIns;
			Gtk::Button* btnLFider;
			Gtk::Button* btnRFider;
			Gtk::Frame* modeFrame;
			Gtk::HBox* hboxcontrol;
			Gtk::VBox* vbspeed;
			Gtk::VBox* vbmode;
			Gtk::VBox* vbscontrol;
			void btnMode_clicked();
			void btnSpeed_clicked();
			void btnSControl_clicked();
			void btnIns_clicked();
			void ModeDefault();
			void btnLFider_clicked();
			void btnRFider_clicked();
			Gtk::EventBox* evben;
			bool evben_clicked(GdkEvent* event);


			MPU::ControlWindowSpec* winSpec;
			Glib::RefPtr<MPU::PasswordDialog> dlgPass;
			Glib::RefPtr<WarningsJournal> winJournal;
			//Journal* winJournal;

			ADD_BUTTON( btnStopGED );

			// дизайн (реализация подсвечивания кнопок)
			// макросы см. GUIConfiguration.h
			SET_FOCUS_FUNC( btnRPM );
			SET_FOCUS_FUNC( btnStart );
			SET_FOCUS_FUNC( btnStop );
			SET_FOCUS_FUNC( btnKeyboard );
			SET_FOCUS_FUNC( btnKeyboardGV );
			SET_FOCUS_FUNC( btnKeyboardINT );
			SET_FOCUS_FUNC( btnSpecial );
			SET_FOCUS_FUNC( btnMode11 );
			SET_FOCUS_FUNC( btnMode2 );
			SET_FOCUS_FUNC( btnMode121 );
			SET_FOCUS_FUNC( btnMode131 );
			SET_FOCUS_FUNC( btnMode122 );
			SET_FOCUS_FUNC( btnMode132 );
			SET_FOCUS_FUNC( btnMode3 );
			SET_FOCUS_FUNC( btnMode );
			SET_FOCUS_FUNC( btnSpeed );
			SET_FOCUS_FUNC( btnSControl );
			SET_FOCUS_FUNC( btnOpenPump );
			SET_FOCUS_FUNC( btnClosePump );
			SET_FOCUS_FUNC( btnTest9 );
			SET_FOCUS_FUNC( btnTest18 );
			SET_FOCUS_FUNC( btnIns );
			SET_FOCUS_FUNC( btnRFider );
			SET_FOCUS_FUNC( btnLFider );

			uniset::ObjectId post_as; /*!< датчик определяющий текущий выбранный пост */
			uniset::ObjectId keyboard_s; /*!< датчик определяющий положение задатчика клавиатуры */

			uniset::ObjectId cur_rpm_as; 	/*!< текущие обороты */
			uniset::ObjectId set_rpm_as; 	/*!< заданные обороты */
			uniset::ObjectId cur_set_rpm_as; 	/*!< текущие заданные обороты */
			uniset::ObjectId mode_as; 		/*!< текущий режим работы */
			uniset::ObjectId pmode_as; 	/*!< текущий схема работы */
			uniset::ObjectId keyControl_s;	/*!< управление с клавиатуры */
			uniset::ObjectId start_s;		/*!< команда на запуск ПЧ */
			uniset::ObjectId stop_s;		/*!< команда на остановку ПЧ */
			uniset::ObjectId stop_ged_s;	/*!< команда на разбор схемы */

			uniset::ObjectId setINT_as; 	/*!< заданная интенсивность */
			uniset::ObjectId w_setINT_as; 	/*!< заданная интенсивность (запись) */

			uniset::ObjectId closepump_s;	/*!< команда закрыть насос */
			uniset::ObjectId openpump_s;	/*!< команда открыть насос */
			uniset::ObjectId numalarm;		/*!< число аварий */
			uniset::ObjectId numwarn;		/*!< число неисправностей */
			uniset::ObjectId typebtnJrn;	/*!< тип кнопки Журнал(0 - Журнал, 1 -Авария, 2 - Наисправность) */
			uniset::ObjectId intens;		/*!< значение интенсивности */

			uniset::IOController::IOStateList::iterator aitPost;
			uniset::IOController::IOStateList::iterator aitMode;
			uniset::IOController::IOStateList::iterator aitPMode;
			uniset::IOController::IOStateList::iterator aitCurRPM;
			uniset::IOController::IOStateList::iterator aitSetRPM;
			uniset::IOController::IOStateList::iterator aitCurSetRPM;
			uniset::IOController::IOStateList::iterator ditKeyControl;
			uniset::IOController::IOStateList::iterator r_aitSetINT;
			uniset::IOController::IOStateList::iterator w_aitSetINT;
			uniset::IOController::IOStateList::iterator r_numalarm;
			uniset::IOController::IOStateList::iterator r_numwarn;
			uniset::IOController::IOStateList::iterator r_intens;
			uniset::IOController::IOStateList::iterator r_lfider;
			uniset::IOController::IOStateList::iterator r_rfider;
			uniset::IOController::IOStateList::iterator r_rlfider;
			uniset::IOController::IOStateList::iterator r_rrfider;
			uniset::IOController::IOStateList::iterator r_workfider;


			bool prev_KeyControl;
			long prev_CurRPM;
			long prev_SetRPM;
			long prev_CurSetRPM;
			long prev_Post;
			long prev_Mode;
			long prev_PMode;
			long prev_SetINT;
			long prev_numalarm;
			long prev_numwarn;
			long prev_intens;
			long prev_lfider;
			long prev_rfider;
			long prev_rlfider;
			long prev_rrfider;
			long prev_workfider;


			uniset::ObjectId mode11_S;
			uniset::ObjectId mode2_S;
			uniset::ObjectId mode121_S;
			uniset::ObjectId mode131_S;
			uniset::ObjectId mode122_S;
			uniset::ObjectId mode132_S;
			uniset::ObjectId mode3_S;
			uniset::ObjectId lastmode;
			uniset::ObjectId lfider;	// в 1 блокируем работу от левого фидера
			uniset::ObjectId rfider;	// в 1 блокируем работу от правого фидера
			uniset::ObjectId rlfider;
			uniset::ObjectId rrfider;
			uniset::ObjectId workfider; // для транспаранта с разрешением на работу от фидера

			uniset::ObjectId ins;

			long rpm_set_minval;	/*!< минимальные разрешённые обороты */
			long rpm_set_maxval;	/*!< максимальные разрешённые обороты */
			long rpm_minval;	/* минимальные разрешения оборотов для диалога ввода*/
			long rpm_maxval;	/* максимальные разрешения оборотов для диалога ввода*/
			long INT_minval;	/*!< минимальные разрешённая интенсивность */
			long INT_maxval;	/*!< максимальные разрешённая интенсивность */

			// запоминать текущее значение необходимо для
			// реализации отмены при вводе,потере фокуса, неправильном значении
			// чтобы не вызывать getValue
			long set_rpm;		/*!< значение заданных оборотов с МПУ */
			long cur_set_rpm;	/*!< текущее заданное значение оборотов (приходящее от ЛСУ ПЧ) */
			float kGV; /*!< передаточное число */
			long kRPM; /*!< коэффициент, на который домнажается число заданных оборотов (т.к. оно задаётся с плавающей точкой) */

			long w_setINT; /*!< заданная интенсивность (запись) */
			long r_setINT;	/*!< заданная интенсивность (чтение) */

			bool onControl;
			bool keyControl;

			Glib::RefPtr<MPU::SetValueDialog> dlgSetVal;

			sigc::connection connUnblockExit;

			sigc::connection connStartTimeout;
			sigc::connection connStopTimeout;
			sigc::connection connStopGEDTimeout;
			sigc::connection connSetTimeout;
			sigc::connection connSetINTTimeout;
			int startTimeout_ms;
			int stopTimeout_ms;
			int stopGEDTimeout_ms;
			int setTimeout_ms;
			long valRun1;
			long valRun2;
			long valReady2;
			bool waitStart;
			bool waitStop;
			bool waitStopGED;
			bool waitSetRPM;
			bool waitSetINT;
			uniset::TriggerOUT<ControlWindow, uniset::ObjectId, bool>* out;
			bool isActivated;

		private:

	};
}
// -------------------------------------------------------------------------
#endif // ControlWindow_H_
// -------------------------------------------------------------------------
