#!/bin/sh
jmake
export PATH="${PATH}:./:../../libs/:../../libs/.libs/"

#ulimit -Sc 10000000
START=uniset2-start.sh

GTK2_RC_FILES=./gtkrc LC_ALL=ru_RU.UTF-8 ${START} -f ./mpubalsam-gui --smemory-id SharedMemoryGUI \
	--mpu-guifile mpubalsam-gui.ui \
	--mpu-confile guiconfigure.xml \
	--pass-check-off \
	--no-beep \
	--mbtcp-name MBTCPMultiMaster1 \
	--mbtcp-reg-from-id 1 \
	--mbtcp-gateway-port 2048 \
	--mbtcp-set-prop-prefix tcp_ \
	--pulsar-id Pulsar_S \
	--pulsar-msec 5000 \
	--e-filter evnt_gui \
	$*
killall get-gate-version.sh
