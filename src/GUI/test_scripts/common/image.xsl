<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>


<xsl:template match="MainWidgets/item">
<xsl:if test="@type='image'">
  <xsl:value-of select="@widget"/>
  <xsl:text> </xsl:text>
  <xsl:value-of select="@sid"/>
  <xsl:for-each select="item">
    <xsl:call-template name="item"/>
  </xsl:for-each>
</xsl:if>
</xsl:template>

<xsl:template name="item">
  <xsl:text> </xsl:text>
  <xsl:value-of select="@val"/>
  <xsl:text> </xsl:text>
  <xsl:value-of select="@img"/>
</xsl:template>

</xsl:stylesheet> 