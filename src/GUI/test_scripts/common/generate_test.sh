#!/bin/sh
xmlfile=../guiconfigure.xml
imagefile=common/image.xsl
vofile=common/VO.xsl
otherfile=common/other.xsl
labelfile=common/label.xsl
tempscript=/common/template.sh
tempfile=`mktemp`

ln -f -s ../../../conf/configure.xml

create()
{
    namenew=`echo $name | sed s:[[:space:]]::g`
    newscript="$namenew"_"$znach".sh
    touch $newscript
    cat "common/template.sh" > $newscript
    echo "\$sV  $sensor=$val" >> $newscript
    echo "" >> $newscript
}
parse()
{
while read line; do
    b=($line)

    curpos=0
    name=${b[curpos]}
        curpos=$((curpos+1))
    sensor=${b[curpos]}
        curpos=$((curpos+1))

    for i in `seq 0 20`; do
        if [[ ${b[$curpos]} ]]; then
            val=${b[curpos]}
                curpos=$((curpos+1))
            znach=${b[curpos]}
                curpos=$((curpos+1))
            create
        fi
    done

done < $tempfile
}

parsenew()
{
while read line; do
    IFS="/"
    set -- $line
    b=($line)
    sensor=0
    name=0
    count=0
    for i in ${b[@]}; do
        if [[ $count -eq 0 ]]; then
            name=$i
            #echo name=$name
        else if [[ $count -eq 1 ]]; then
            sensor=$i
            #echo sensor=$sensor
        else if [[ $(($count % 2)) -eq 0 ]]; then
                val=$i
                #echo val=$val
            else
                znach=$i
                #echo znach=$znach
                create
            fi

        fi
        fi

            #echo "--------------------------"
            #echo count=$count
            #echo name=$name
            #echo sensor=$sensor
            #echo znach=$znach
            #echo val=$val
            #echo "--------------------------"

            count=$((count+1))

    done

done < $tempfile
}

img()
{
#парсим guiconfigure и ищем виджеты img
xsltproc $imagefile $xmlfile | grep -v '^[[:space:]]\{0,\}$' | sed "s:.png::g;s:.svg::g;s:\/::g;s:<?xml version=\"1.0\"?>::g" > $tempfile
parse
}
vo()
{
#парсим guiconfigure и ищем виджеты типа _VO
xsltproc $vofile $xmlfile | grep -v '^[[:space:]]\{0,\}$' | sed "s:.png::g;s:.svg::g;s:\/::g;s:<?xml version=\"1.0\"?>::g" > $tempfile
parse
}
transparant()
{
#парсим guiconfigure и ищем виджеты типа transparant с разными val
xsltproc $otherfile $xmlfile | grep -v '^[[:space:]]\{0,\}$' | sed "s:.png::g;s:.svg::g;s:<?xml version=\"1.0\"?>::g" > $tempfile
parsenew
}
label()
{
#парсим guiconfigure и ищем виджеты типа label
xsltproc $labelfile $xmlfile | grep -v '^[[:space:]]\{0,\}$' | sed "s:.png::g;s:.svg::g;s:<?xml version=\"1.0\"?>::g" > $tempfile
xsltproc $labelfile $xmlfile | grep -v '^[[:space:]]\{0,\}$' | sed "s:.png::g;s:.svg::g;s:<?xml version=\"1.0\"?>::g"
parsenew
}

#img
#vo
#transparant
label


rm -f $tempfile
chmod +x *.sh
