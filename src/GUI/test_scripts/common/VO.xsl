<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>


<xsl:template match="MainFrame/*">
<xsl:if test="@name!=''">
<xsl:value-of select="@name"/>
<xsl:text> </xsl:text>
<xsl:value-of select="@state_as"/>
<xsl:for-each select="var">
    <xsl:call-template name="var"/>
  </xsl:for-each>
</xsl:if>
</xsl:template>

<xsl:template name="var">
  <xsl:text> </xsl:text>
  <xsl:value-of select="@value"/>
  <xsl:text> </xsl:text>
  <xsl:value-of select="@text"/>
</xsl:template>

</xsl:stylesheet> 