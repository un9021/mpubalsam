#include "MyMainWindow.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace MPU;
// -------------------------------------------------------------------------
Glib::RefPtr<MainWindow> MainWindow::inst;
// -------------------------------------------------------------------------
Glib::RefPtr<MainWindow> MainWindow::Instance()
{
	return inst;
}
// -------------------------------------------------------------------------
Glib::RefPtr<MainWindow> MainWindow::init_gui( uniset::ObjectId id, int argc, char* argv[], std::shared_ptr<IONotifyController>& nc )
{
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();

	if( !inst )
		inst = Glib::RefPtr<MainWindow>( new MainWindow(id, nc) );

	return inst;
}
// -------------------------------------------------------------------------
extern void setNumLock( bool state, bool no_emit = false );
// -------------------------------------------------------------------------
#include "ProfileTime.h"
// -------------------------------------------------------------------------
static void events_pending()
{
	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
}
// -------------------------------------------------------------------------
Gtk::Window* MainWindow::getWin()
{
	return pMainWindow;
}
// -------------------------------------------------------------------------
MainWindow::MainWindow( uniset::ObjectId id, std::shared_ptr<IONotifyController>& nc ):
	BaseMainWindow(id, nc, "MainWindow"),
	act(act),
	pMainWindow(NULL),
	ledCurGED_RPM(NULL),
	ledTargetGED_RPM(NULL),
	ledCurrent(NULL),
	ledPower(NULL),
	ledVoltage(NULL),
	ledCurrent2(NULL),
	ledVoltage2(NULL),
	flash(0),
	btnControl(NULL),
	btnJrn(NULL),
	netRespond(true),
	lsuRespond(false),
	pollTime(100),
	dlgPass(NULL),
	maxCheckPassCount(3),
	prevNetRestart(false),
	mainFrame(NULL),
	cwin(NULL),
	noLSUCheckRespond(NULL),
	noConvCheckRespond(NULL),
	sp(NULL),
	initOK(false),
	tm_msec(),
	psplash(0),
	trCheckLink(true),
	md(0),
	mainscreen(NULL),
	bigmnemo(NULL),
	dwin(0),
	screen_saver_state(false),
	ss_msec(0),
	wman(NULL),
	wwin(NULL),
	jwin(NULL),
	alarm(false),
	keyIsPressed(false)
{
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	auto conf = uniset_conf();
	static struct timeval tbeg;
	getTime(tbeg);

	myname = "MainWindow";
	uin.setCacheMaxSize(2000);

	GUIXml_ref guixml = GUIConf::GUIXmlInstance();

	idNetCommRestart_FS = conf->getSensorID("NetComm_Restart_FS");

	pMainWindow = 0;
	GETWIDGET(pMainWindow, "MainWindow");
	pMainWindow->signal_key_press_event().connect(sigc::mem_fun(*this, &MainWindow::on_key_press_event), true);
	pMainWindow->signal_button_press_event().connect(sigc::mem_fun(*this, &MainWindow::on_my_button_press_event), true);

	pMainWindow->hide();
	pMainWindow->property_user_data() = pMainWindow;

	sp = new ProcessProgress();
	sp->write(5);

	xmlNode* cnode = guixml->findNode(guixml->getFirstNode(), "MainWindow");

	if( cnode == NULL )
		throw SystemError("Not found xml-confnode for MainWindow");

	onflashNode = guixml->findNode(cnode, "OnFlashList");

	if( dlog.debugging(Debug::INFO) )
		dlog[Debug::INFO] << myname << "(init): onFlashList " << (onflashNode ? "yes" : "no") << endl;

	UniXML_iterator it(cnode);

	pollTime = guixml->getPIntProp(cnode, "pollTime", 150);
	pollCountMsg = guixml->getPIntProp(cnode, "pollCountMsg", 50);

	nolink_txt = guixml->getProp(cnode, "nolink_txt");

	if( nolink_txt.empty() )
		nolink_txt = "Нет связи с локальным контроллером...";

	noLSUCheckRespond 	= guixml->getIntProp(cnode, "no_lsu_check_respond");
	noConvCheckRespond	= guixml->getIntProp(cnode, "no_conv_check_respond");

	tm_checknetwork_msec = guixml->getPIntProp(cnode, "tm_checknetwork_msec", 60000);

	backupDir = it.getProp("backupDir");

	if( backupDir.empty() )
		backupDir = conf->getLogDir();

	backupSensors = it.getProp("backupSensors");

	if( backupSensors.empty() )
		backupSensors = "SensorsList";

	// --------------------------------------------------------------
	cerr << "STATE IND..." << endl;
	BEGIN_TIME
	//net 	= new NetStateVO("NetState_VO", &shm);
	//SHOW_TIME(myname, "NetState_VO");
	// --------------------------------------------------------------
	sp->write(10);
	// --------------------------------------------------------------
	ledCurGED_RPM 		= createLed("GEDCurRPMLed", "hbCurGED", "GED_CurRPM_AS");
	ledTargetGED_RPM	= createLed("GEDTargetRPMLed", "hbTargetGED", "GED_TargetRPM_AS");

	SHOW_TIME(myname, "create leds...");

	ledPower	= createLed("PowerLed", "hbPower", "Drive_Power_AS");
	ledVoltage	= createLed("VoltageLed", "hbVoltage1", "FQC1_DC_U1_AS");
	ledCurrent	= createLed("CurrentLed", "hbCurrent1", "FQC1_DC_I1_AS");
	ledVoltage2	= createLed("VoltageLed2", "hbVoltage2", "FQC1_DC_U2_AS");
	ledCurrent2	= createLed("CurrentLed2", "hbCurrent2", "FQC1_DC_I2_AS");
	SHOW_TIME(myname, "create leds(2)...");
	sp->write(20);
	// --------------------------------------------------------------
	//	fixLeft = 0;
	//	GETWIDGET(fixLeft,"fixLeft");
	fixRight = 0;
	GETWIDGET(fixRight, "fixRight");
	//	fixState = 0;
	//	GETWIDGET(fixState,"fixState");
	fixMode = 0;
	GETWIDGET(fixMode, "fixMode");
	//	fixInfo = 0;
	//	GETWIDGET(fixInfo,"fixInfo");

	idNetRespond_s = conf->getSensorID("Conv_Respond_FS");
	idLSURespond_s = conf->getSensorID("LSU_Respond_FS");

	// -------------------------------------------------------------
	lblTime = 0;
	GETWIDGET(lblTime, "lblTime");
	Pango::FontDescription fn("Helvetica Bold 14");
	lblTime->modify_font(fn);
	lblDate = 0;
	GETWIDGET(lblDate, "lblDate");
	Pango::FontDescription fn1("Helvetica Bold 10");
	lblDate->modify_font(fn1);
	// --------------------------------------------------------------
	{
		Gtk::Label* lbl = 0;
		GETWIDGET(lbl, "lblMode");
		Pango::FontDescription fd("Helvetica Bold 14");
		lbl->modify_font(fd);
	}
	{
		Gtk::Label* lbl = 0;
		GETWIDGET(lbl, "lblControl");
		Pango::FontDescription fd("Helvetica Bold 14");
		lbl->modify_font(fd);
	}
	// --------------------------------------------------------------
	{
		Gtk::Label* lbl = 0;
		GETWIDGET(lbl, "lblPowerMode");
		Pango::FontDescription fd("Helvetica Bold 14");
		lbl->modify_font(fd);
	}
	{
		Gtk::Label* lbl = 0;
		GETWIDGET(lbl, "lblGED");
		Pango::FontDescription fd("Helvetica Bold 12");
		lbl->modify_font(fd);
	}
	SHOW_TIME(myname, "get widgets...");
	// --------------------------------------------------------------
	sp->write(25);
	// --------------------------------------------------------------
	dlgPass 	= PasswordDialog::Instance();
	dlgSetVal	= SetValueDialog::Instance();
	SHOW_TIME(myname, "instance dialogs...");
	// --------------------------------------------------------------
	GETWIDGET(btnControl, "btnControl");
	btnControl->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnControl_clicked));
	SET_FOCUS_SIG(MainWindow, btnControl);
	//GUIConf::setFocusStyle(btnControl);

	GETWIDGET(btnJrn, "btnJrn");
	btnJrn->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnJrn_clicked));
	SET_FOCUS_SIG(MainWindow, btnJrn);

	GETWIDGET(evboxmnemo, "evboxmnemo");
	evboxmnemo->signal_event().connect(sigc::mem_fun(*this, &MainWindow::btnMnemoShow_clicked));
	GETWIDGET(evboxbigmnemo, "evboxbigmnemo");
	evboxbigmnemo->signal_event().connect(sigc::mem_fun(*this, &MainWindow::btnBigMnemoExit_clicked));

	GETWIDGET(mainscreen, "mainscreen");
	GETWIDGET(bigmnemo, "bigmnemo");
	bigmnemo->hide();

	GETWIDGET(btnMnemoExit, "btnMnemoExit");
	btnMnemoExit->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnMnemoExit_clicked));

	//GUIConf::setFocusStyle(btnJrn);
	// --------------------------------------------------------------
	Gtk::EventBox* ebox = 0;
	refXml->get_widget("evbox_NET", ebox);

	if( ebox )
		dwin = new DebugWindow("DebugWindow", shm);

	// --------------------------------------------------------------
	sp->write(30);
	// --------------------------------------------------------------
	GETWIDGET(btnExit, "btnExit");
	btnExit->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnExit_clicked));
	SET_FOCUS_SIG(MainWindow, btnExit);
	//GUIConf::setFocusStyle(btnExit);
	// --------------------------------------------------------------
	SHOW_TIME(myname, "get buttons...");

	// --------------------------------------------------------------
	if( ebox )
		mainFrame 	= new MainFrame("MainFrame", shm);

	SHOW_TIME(myname, "MainFrame...");
	// --------------------------------------------------------------
	sp->write(40);
	// --------------------------------------------------------------
	cwin = new ControlWindow("ControlWindow", shm, this);
	SHOW_TIME(myname, "ControlWindow...");

	//	ledTargetGED_RPM->set_precision( cwin->getRPMPrecision() );
	//ledTargetGED_RPM->set_format( cwin->getRPMPrecision(), 0 );
	//	ledCurGED_RPM->set_precision( cwin->getRPMPrecision() );
	//ledCurGED_RPM->set_format( cwin->getRPMPrecision(), 0 );

	sp->write(50);
	// --------------------------------------------------------------
	cerr << "create alarm windows" << endl;
	awin = AlarmWindow::Instance();
	awin->show();
	cerr << "create win manager" << endl;
	wman = WinManager::Instance();
	wman->setMainWindow(mainFrame);
	cerr << "create warning windows" << endl;
	wwin = WarningsWindow::Instance(shm);
	wwin->show();
	wwin->signal_list_add_item().connect(sigc::mem_fun(*this, &MainWindow::on_new_warning));
	wwin->signal_show_alarm().connect(sigc::mem_fun(*this, &MainWindow::on_showAlarmWarning));
	// --------------------------------------------------------------
	jwin = WarningsJournal::Instance(shm);
	jwin->signal_show_alarm().connect(sigc::mem_fun(*this, &MainWindow::on_showAlarmJournal));
	jwin->hide();
	GETWIDGET(vbMW, "journal_vbMain");
	GETWIDGET(vbAW, "journal_vbAlarm");
	GETWIDGET(jrnbox, "jrnbox");
	GETWIDGET(jrnbook, "jrnbook");
	GETWIDGET(enwin, "vbControlSpec")

	GETWIDGET(btnArch, "btnarch");
	btnArch->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnArch_clicked));
	SET_FOCUS_SIG(MainWindow, btnArch);
	GETWIDGET(btnNow, "btnnow");
	btnNow->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnNow_clicked));
	SET_FOCUS_SIG(MainWindow, btnNow);


	jrnbox->hide();
	vbMW->show();
	vbAW->show();
	// --------------------------------------------------------------
	flash = new FlashLed("FlashLed", shm, this, refXml);
	flash->signal_flash_state().connect(sigc::mem_fun(*this, &MainWindow::on_flash_state));
	// --------------------------------------------------------------
	sp->write(60);
	// --------------------------------------------------------------
	connTime.disconnect();
	connTime = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::setTime), 1000);

	setDate(); // выставим дату текущую
	connDate.disconnect();
	connDate = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::setDate), 60000);
	// --------------------------------------------------------------
	ss_msec = guixml->getIntProp(cnode, "screensaver_msec");

	if( ss_msec > 0 )
	{
		ss_script = it.getProp("screensaver_script");
		dlog[Debug::INFO] << myname << "(init): init scrensaver script=" << ss_script
						  << " screensaver time " << ss_msec << " msec" << endl;
		connScreenSaver = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::on_timer_screen_saver), ss_msec);
	}

	pMainWindow->signal_delete_event().connect(sigc::mem_fun(*static_cast<class MainWindow*>(this), &MainWindow::on_destroy));
	pMainWindow->set_resizable(false);
	btnControl->grab_focus();
	// отключаем обработку NumLock в X-ах!!!
	// (см. описание в файле SetNumLock.cc)
	setNumLock(false, true);
	// --------------------------------------------------------------
	sp->write(70);
	// --------------------------------------------------------------
	checkRespond();
	pMainWindow->hide();
	// --------------------------------------------------------------
	sp->write(80);
	// --------------------------------------------------------------
	//	connPoll.disconnect();
	//	if( pollTime )
	//		connPoll = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::poll),pollTime);

	if( uniset::findArgParam("--maximize", conf->getArgc(), conf->getArgv()) != -1 )
	{
		pMainWindow->move(0, 0);
		pMainWindow->maximize();
		pMainWindow->fullscreen();
	}


	static struct timeval tend;
	cout << "(MainWindow): *********************** init time ";
	endTime( cout, tbeg, tend );
	cout << endl;

	psplash = 190;
	sp->write(psplash);
	sp->write(-1);

	pMainWindow->show();

	//Установка времени двойного клика в 1/2 секунды
	Glib::RefPtr<Gdk::Display> disp = Gdk::Display::get_default();
	disp->set_double_click_time(500);

	waiting_for_mount = 0;
	cnode = guixml->findNode(guixml->getFirstNode(), "FlashLed");

	if( cnode == NULL )
		throw SystemError( "Not find conf-node for FlashLed" );

	it = UniXML_iterator(cnode);

	flash_script = it.getProp("flash_script");
}

// -------------------------------------------------------------------------
MainWindow::~MainWindow()
{
	delete sp;
	//delete net;
	delete mainFrame;
	delete flash;
	delete cwin;
	delete dwin;
	connPoll.disconnect();
	connTime.disconnect();
}

// -------------------------------------------------------------------------
bool MainWindow::on_destroy( GdkEventAny* evnt )
{
	// возвращаем обработку NumLock в X-ах!!!
	// (см. описание в файле SetNumLock.cc)
	setNumLock(false, true);

	//	act->oakill(SIGINT);
	//	msleep(200);
	//	act->oakill(SIGKILL);
	return true;
}

// -------------------------------------------------------------------------
LedMeterVO* MainWindow::createLed( const string idname, const string wname,
								   const string sname )
{
#warning сделать настройку параметров из conf-файла
	/*
		LedMeterVO* led = 0;
		Gtk::HBox* hb	= 0;
		GETWIDGET(hb,wname);
		led = manage(new class LedMeterVO(idname,&shm,sname,0,"Helvetica 12"));
		led->show();
	//	led->set_size_request(50,24);
		led->set_name(idname);
		hb->add(*led);

		ledlist.push_back(led);
		return led
		*/
}
// -------------------------------------------------------------------------
/*
bool MainWindow::checkPassword( MPU::PassID p_id, int maxCount, const string title )
{
	if( dlgPass == NULL )
	{
		dlog[Debug::CRIT] << myname << "(checkPassword): dlgPass=NULL!" << endl;
		return false;
	}

	dlgPass->set_title(title);
	bool res = dlgPass->check(p_id,maxCount);
	pMainWindow->show();

	return res;
}
*/
// -------------------------------------------------------------------------
void MainWindow::btnControl_clicked()
{
	if( wman->areButtonsBlocked() )
		return;

	dlog[Debug::INFO] << myname << ": btnControl_clicked..." << endl;
	/*
		// 20.08.2014: По просьбе заказчика(НИО-12) вход по паролю убран (bug #6215)

		if( !checkPassword(pControl,maxCheckPassCount,"Доступ к функциям управления") )
		{
			btnControl->set_state(Gtk::STATE_PRELIGHT);
			dlog[Debug::INFO] << myname << "(btnControl_clicked):check password failed..." << endl;
			return;
		}
		dlog[Debug::INFO] << myname << "(btnControl_clicked): check password OK" << endl;
	*/
	cwin->win_activate();
	jrnbox->hide();
	btnExit->grab_focus();
}
// -------------------------------------------------------------------------
void MainWindow::btnJrn_clicked()
{
	if( wman->areButtonsBlocked() )
		return;

	btnJrn->set_state(Gtk::STATE_PRELIGHT);
	dlog[Debug::INFO] << myname << ": btnJnr_clicked..." << endl;
	jwin->win_activate();
	jwin->show();
	jrnbox->show();
	vbAW->hide();
	cwin->hide();
	mainFrame->hide();
	//btnControl->show();
	enwin->hide();
	btnExit->show();
	btnNow->grab_focus();
	jrnbook->set_current_page(1);
}
// -------------------------------------------------------------------------
string MainWindow::saveSensors()
{
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();
	xmlNode* cnode = guixml->findNode(guixml->getFirstNode(), "FlashLed");
	auto conf = uniset_conf();

	if( cnode == NULL )
		throw SystemError( "Not find conf-node for FlashLed" );

	UniXML_iterator it(cnode);

	string flash_script = it.getProp("flash_script");

	if( flash_script.empty() )
	{
		Gtk::MessageDialog msg(_("Не задана команда для сохранения на флэш-диск"), false, Gtk::MESSAGE_WARNING);
		msg.set_modal( true );
		msg.set_position( Gtk::WIN_POS_CENTER );
		msg.run();
		return "";
	}

	ParamList::ValList lst;
	mainFrame->getList(&lst);
	ostringstream cmd;

	if( !flash_script[0] == '.' && !flash_script[0] == '/' )
		cmd << conf->getBinDir();

	string dir(backupDir + "/sensors");

	if( !createDir(backupDir) || !createDir(dir) )
		return "";

	std::string htmlSensors(dir + "/" + backupSensors + ".html");
	cout << "saving sensors html-file to " << htmlSensors << endl;

	fstream fout( htmlSensors.c_str(), ios::out | ios::trunc );
	fout << "<html xmlns:date=\"http://exslt.org/dates-and-times\">" << endl;
	fout << "<head>" << endl;
	fout << "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" << endl;
	fout << "<title>Датчики</title>" << endl;
	fout << "</head>" << endl;
	fout << "<body><h1>Датчики</h1>" << endl;
	fout << "<table border=\"1\">" << endl;
	ParamList::ValList::iterator lst_it = lst.begin();

	for( unsigned int i = 0; i < lst.size(); i++ )
	{
		fout << "\t<tr>" << endl;
		fout << "\t\t<th style=\"width:400px;\">" << lst_it->name << "</th>" << endl;
		fout << "\t\t<th style=\"width:100px;\">" << lst_it->val << "</th>" << endl;
		fout << "\t</tr>" << endl;
		lst_it++;
	}

	fout << "</table>" << endl << "</body>" << endl << "</html>" << endl;

	cmd.str("");

	if( !flash_script[0] == '.' && !flash_script[0] == '/' )
		cmd << conf->getBinDir();

	return dir;
}
// -------------------------------------------------------------------------
bool MainWindow::setTime()
{
	static char buf[30];
	static time_t GMTime;
	static struct tm* tms;

	memset(buf, 0, sizeof(buf));
	GMTime = time(NULL);
	tms = localtime(&GMTime);

	strftime(buf, sizeof(buf), "%T", tms);
	lblTime->set_text(Glib::ustring(buf));
	return true;
}
// -------------------------------------------------------------------------
bool MainWindow::setDate()
{
	static char buf[30];
	static time_t GMTime;
	static struct tm* tms;

	memset(buf, 0, sizeof(buf));
	GMTime = time(NULL);
	tms = localtime(&GMTime);

	strftime(buf, sizeof(buf), "%d/%m/%Y", tms);
	lblDate->set_text(Glib::ustring(buf));
	return true;
}
// -------------------------------------------------------------------------
void MainWindow::poll()
{
	try
	{
		wman->poll();
	}
	catch( ... ) {}

	try
	{
		wwin->poll();
	}
	catch( ... ) {}

	try
	{
		myPoll();
	}
	catch( ... ) {}
}
// -------------------------------------------------------------------------
void MainWindow::set_screen_saver( bool state )
{
	dlog[Debug::INFO] << myname << "(set_screen_saver): set state=" << state << endl;

	connScreenSaver.disconnect();

	// reinit timer..
	if( !state )
	{
		keyIsPressed = true;
		connScreenSaver = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::on_timer_screen_saver), ss_msec);
	}

	if( screen_saver_state == state )
		return;

	screen_saver_state = state;

	// run screen_saver..
	if( !ss_script.empty() )
	{
		ostringstream cmd;
		cmd << ss_script << " " << ( state ? "start" : "stop" );

		dlog[Debug::INFO] << myname << "(set_screen_saver): run " << cmd.str() << endl;

		try
		{
			Glib::spawn_command_line_sync( cmd.str() );
		}
		catch( Glib::Exception& ex )
		{
			dlog[Debug::CRIT] << myname << "(set_screen_saver): " <<  ex.what() << endl;
		}
	}
}
// -------------------------------------------------------------------------
bool MainWindow::on_timer_screen_saver()
{
	if( !keyIsPressed )
	{
		set_screen_saver(true);
		return false;
	}

	keyIsPressed = false;
	return true;
}
// -------------------------------------------------------------------------
/*
void MainWindow::activate()
{
	initIterators();
	connPoll.disconnect();
	/*if( pollTime )
		connPoll = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::poll),pollTime);
}
*/
// -------------------------------------------------------------------------
void MainWindow::initIterators()
{
	cerr << myname << "(initIterators): *******" << endl;

	shm->initIterator( ditNetRespond_s );
	shm->initIterator( ditLSURespond_s );
	shm->initIterator( ditNetRestart_fs );
	shm->initIterator( ditPowLim );


	for( LEDList::iterator it = ledlist.begin(); it != ledlist.end(); ++it )
	{
		try
		{
			cerr << myname << "(initIterators): LEDList********" << endl;
			(*it)->initIterators();
		}
		catch(...) {}
	}

	//cerr << myname << "(initIterators): NET..." << endl;
	//net->initIterators();
	cerr << myname << "(initIterators): CWIN..." << endl;
	cwin->initIterators();
	cerr << myname << "(initIterators): JWIN..." << endl;
	jwin->initIterators();
	cerr << myname << "(initIterators): WWIN..." << endl;
	wwin->initIterators();
	cerr << myname << "(initIterators): MAINFRAME..." << endl;
	mainFrame->initIterators();


	cerr << myname << "(initIterators): Flash..." << endl;
	flash->initIterators();
}
// -------------------------------------------------------------------------
void MainWindow::sensorInfo( uniset::SensorMessage* sm )
{
	if( sm->id == idNetCommRestart_FS )
	{
		if( sm->value )
		{
			dlog[Debug::WARN] << myname << "(sensorInfo): РЕСТАРТ процесса обмена..." << endl;
			//SystemMessage sm(SystemMessage::StartUp);
			//act->broadcast( sm.transport_msg() );
		}
	}
	else if( sm->id == idNetRespond_s )
	{
		netRespond = sm->value;
		checkRespond();
	}
	else if( sm->id == idLSURespond_s )
	{
		lsuRespond = sm->value;
		checkRespond();
	}

}
// -------------------------------------------------------------------------
void MainWindow::checkRespond()
{
	if( noLSUCheckRespond && noConvCheckRespond )
		return;


	bool state = false;

	//	если отключена проверка, то делаем всегда true
	if( noLSUCheckRespond )
		lsuRespond = true;

	//	если отключена проверка, то делаем всегда true
	if( noConvCheckRespond )
		netRespond = true;

	// собственно проверка
	if( netRespond && lsuRespond )
		state = true;

	if( trChangeLink.change( state ) )
	{
		cout << "*************** change network state " << state << endl;
		fixRight->set_sensitive(state);
		//		fixLeft->set_sensitive(state);
		//		fixState->set_sensitive(state);
		fixMode->set_sensitive(state);
		//		fixInfo->set_sensitive(state);
		//btnControl->set_sensitive(state);
		btnJrn->set_sensitive(state);
	}

	// Всегда true
	btnExit->set_sensitive(true);

	if( !dlgPass->is_mapped() && !dlgSetVal->is_mapped() )
	{
		if( btnExit->is_visible() )
			btnExit->grab_focus();
		else if( btnControl->is_visible() )
			btnControl->grab_focus();
	}

	if( trCheckLink.low(netRespond) )
	{
		if( !md )
		{
			//			cout << "******************************* create ExtMessageDialog ********" << endl;
			md = manage( new ExtMessageDialog("Ожидание связи", Gtk::MESSAGE_INFO) );
			md->set_name("ExtMessageDialog");
			md->present();
			md->grab_focus();
		}

		md->mrun(idNetRespond_s, shm, 500, nolink_txt);
		//		md->present();
	}
}
// -------------------------------------------------------------------------
// дизайн (реализация подсвечивания кнопок)
// макросы см. GUIConfiguration.h
SET_FOCUS_FUNC_IMPL( MainWindow, btnControl )
SET_FOCUS_FUNC_IMPL( MainWindow, btnJrn )
SET_FOCUS_FUNC_IMPL( MainWindow, btnExit )
SET_FOCUS_FUNC_IMPL( MainWindow, btnNow )
SET_FOCUS_FUNC_IMPL( MainWindow, btnArch )
// -------------------------------------------------------------------------
void MainWindow::updateDS( uniset::ObjectId sid, bool& prev, uniset::IOController::IOStateList::iterator& dit )
{
	auto conf = uniset_conf();
	sm.node = conf->getLocalNode();
	sm.sensor_type = UniversalIO::DI;
	sm.id = sid;

	bool st = prev;

	if( getState( shm, sm.id, st, dit) )
	{
		if( sm.value == prev )
			return;

		try
		{
			sensorInfo(&sm);
			prev = sm.value;
		}
		catch(...) {}
	}
}
// -------------------------------------------------------------------------
void MainWindow::myPoll()
{
	updateDS( idNetCommRestart_FS, prevNetRestart, ditNetRestart_fs );
	updateDS( idNetRespond_s, netRespond, ditNetRespond_s);
	updateDS( idLSURespond_s, lsuRespond, ditLSURespond_s );

	cwin->poll();
	flash->poll();
}
// -------------------------------------------------------------------------
void MainWindow::on_flash_state( long state )
{
	set_screen_saver(false);
	m_flash_signal.emit(state);
}
// -------------------------------------------------------------------------
const std::string MainWindow::getSaveDir()
{
	std::ostringstream s;
	s << backupDir << "/" << uniset::dateToString(time(0), "-");
	return s.str();
}
//------------------------------------------------------------------------------
const std::string MainWindow::getBackupDir()
{
	return backupDir;
}
//------------------------------------------------------------------------------
bool MainWindow::createDir( const std::string dirname )
{
	if( dirname.empty() || dirname == "/" )
		return false;

	ostringstream cmd;
	cmd << "mkdir -p -m 777 " << dirname;

	try
	{
		Glib::spawn_command_line_sync( cmd.str() );
	}
	catch( Glib::Exception& ex )
	{
		dlog[Debug::CRIT] << myname << "(mkdir): " <<  ex.what() << endl;
	}

	//dlog[Debug::INFO] << system( cmd.str().c_str()) << endl;

	struct stat st;

	if (stat(dirname.c_str(), &st))
	{
		cerr << "mkdir " << dirname << " FAILED! (" << strerror(errno) << endl;
		Gtk::MessageDialog mdialog(_("(mkdir): Не удалось создать папку"), true, Gtk::MESSAGE_WARNING);
		mdialog.run();
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
void MainWindow::on_new_warning( const Gtk::TreeModel::iterator& )
{
	dlog[Debug::INFO] << myname << ": new warning event.." << endl;
	set_screen_saver(false);
}
//------------------------------------------------------------------------------
bool MainWindow::on_my_button_press_event( GdkEventButton* event )
{
	//	cerr << "my button.." << endl;
	// key pressed ---> on_screen... reinit timer...
	set_screen_saver(false);
	return false;
}
//------------------------------------------------------------------------------
bool MainWindow::on_key_press_event( GdkEventKey* event )
{
	if( event->keyval == KEY_ESC )
	{
		btnExit->grab_focus();
	}

	// key pressed ---> on_screen... reinit timer...
	set_screen_saver(false);

	if( event->keyval == KEY_m0 || event->keyval == KEY_0 )
		return false;

	// чтобы не блокировать обработчики всех дочерних элементов
	// мы должны вернуть false
	return false;
}
// -------------------------------------------------------------------------
void MainWindow::beep()
{
	pMainWindow->get_screen()->get_display()->beep();
}
// -------------------------------------------------------------------------
void MainWindow::setFlashButtonsBlock( bool state )
{
	wman->blockFlashButtons(state);

	if( state )
		connWaitForMount = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::waitingForMount), 1000);
}
// -------------------------------------------------------------------------
bool MainWindow::getFlashButtonsBlock()
{
	return  wman->areFlashButtonsBlocked();
}
// -------------------------------------------------------------------------
bool MainWindow::waitingForMount()
{
	std::string cmd_check(flash_script + " --check");
	int ret = 0;

	try
	{
		Glib::spawn_command_line_sync( cmd_check, 0, 0, &ret );
	}
	catch( Glib::Exception& ex )
	{
		dlog[Debug::CRIT] << myname << "(mkdir): " <<  ex.what() << endl;
		ret = 0;
	}

	//	int ret = system(cmd_check.c_str());
	int status = WEXITSTATUS(ret);

	if( status >= FlashLed::flEnd )
		status = 0;

	/*! Ожидаем монтирования флеш в течение BUTTONS_BLOCK_TIMEOUT раз вызова update_menu */
	if( getFlashButtonsBlock() && status < FlashLed::flMounted )
	{
		waiting_for_mount++;
		cout << "waiting for flash mount..." <<  waiting_for_mount << endl;
	}
	else
	{
		waiting_for_mount = 0;
		connWaitForMount.disconnect();
	}

	if( waiting_for_mount == BUTTONS_BLOCK_TIMEOUT )
	{
		setFlashButtonsBlock(false);
		waiting_for_mount = 0;
		connWaitForMount.disconnect();
		cout << "buttons block disabled" << endl;
	}

	return true;
}
// -------------------------------------------------------------------------
void MainWindow::on_showAlarmWarning(const string& code, const string& datetime, const string& numcon, const std::string& bitreg )
{
	cout << "doule clik journal" << endl;
	//vbMW->hide();
	vbAW->show();
	jrnbox->show();
	jrnbook->set_current_page(2);
	awin->showPage(code, datetime, numcon );
	awin->show();
	alarm = true;
}
// -------------------------------------------------------------------------
bool MainWindow::btnMnemoShow_clicked(GdkEvent* event)
{
	GdkEventButton event_btn = event->button;

	if((event_btn.type == GDK_2BUTTON_PRESS))
	{
		mainscreen->hide();
		bigmnemo->show();
	}

	return false;

}
bool MainWindow::btnBigMnemoExit_clicked(GdkEvent* event)
{
	GdkEventButton event_btn = event->button;

	if((event_btn.type == GDK_2BUTTON_PRESS))
	{
		mainscreen->show();
		bigmnemo->hide();
	}

	return false;

}
// -------------------------------------------------------------------------
void MainWindow::btnMnemoExit_clicked()
{
	mainscreen->show();
	bigmnemo->hide();
}
void MainWindow::btnExit_clicked()
{
	if (alarm)
	{
		mainFrame->hide();
		vbAW->show();
		vbMW->show();
		jwin->show();
		awin->hide();
		alarm = false;
		btnExit->show();
		jrnbook->set_current_page(0);
	}
	else
	{
		cout << "alarm false" << endl;
		jrnbox->hide();
		cwin->hide();
		enwin->hide();
		mainFrame->win_activate();
		mainFrame->show();
	}

}

void MainWindow::on_showAlarmJournal(const string& code, const string& datetime, const string& numcon, const std::string& bitreg )
{
	cout << "doule click journal" << endl;
	//vbMW->hide();
	vbAW->show();
	jrnbox->show();
	jrnbook->set_current_page(2);
	awin->showPage(code, datetime, numcon );
	awin->show();
	alarm = true;
}

void MainWindow::btnNow_clicked()
{
	cout << "Now" << endl;
	awin->hide();
	btnExit->show();
	alarm = false;
	jrnbook->set_current_page(1);
}

void MainWindow::btnArch_clicked()
{
	cout << "Arch" << endl;
	awin->hide();
	btnExit->show();
	alarm = false;
	jrnbook->set_current_page(0);
}

