#include "MyControlWindow.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace MPU;
// -------------------------------------------------------------------------
//extern UniXML* guixml;
extern Glib::RefPtr<Gnome::Glade::Xml> refXml;
extern Gtk::Window* pMainWindow;

static const int blinkCount = 6; /*!< количество миганий  */
extern void setNumLock( bool state, bool no_emit = false );
// -------------------------------------------------------------------------
#include "ProfileTime.h"
// -------------------------------------------------------------------------

ControlWindow::ControlWindow( const string idname, std::shared_ptr<uniset::SMInterface>& shm, MainWindow* mwin ):
	ChildFrame(idname),
	VObject(idname, shm),
	tp(NULL),
	tpPost(NULL),
	vbMain(NULL),
	entRPM(NULL),
	entGV(NULL),
	entINT(NULL),
	winSpec(NULL),
	winJournal(NULL),
	prev_KeyControl(false),
	prev_CurRPM(-1),
	prev_SetRPM(-1),
	prev_CurSetRPM(-1),
	prev_Post(-1),
	prev_Mode(-1),
	prev_PMode(-1),
	prev_SetINT(-1),
	prev_numalarm(-1),
	prev_numwarn(-1),
	prev_intens(-1),
	prev_workfider(-1),
	set_rpm(0),
	onControl(false),
	keyControl(false),
	valRun1(0),
	valRun2(0),
	valReady2(0),
	waitStart(false),
	waitStop(false),
	waitStopGED(false),
	waitSetRPM(false),
	waitSetINT(false),
	out(NULL),
	dlgPass(NULL),
	isActivated(false)
{
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();
	xmlNode* cnode = guixml->findNode(guixml->getFirstNode(), idname);
	auto conf = uniset_conf();

	if( cnode == NULL )
		throw SystemError("Not found conf-node for " + idname);

	BEGIN_TIME
	{
		Gtk::VBox* vbExt = 0;
		GETWIDGET(vbExt, "vbExtWindow");
		GETWIDGET(vbMain, "vbControl");
		vbMain->reparent(*vbExt);
	}
	SHOW_TIME(idname, "get main widgets...");

	init(cnode, &tpDefault, "tp_default");
	init(cnode, &tpOK, "tp_ok");
	init(cnode, &tpRepeat, "tp_repeat");
	init(cnode, &tpTimeout, "tp_timeout");
	init(cnode, &tpBadValue, "tp_badvalue");
	init(cnode, &tpInternal, "tp_internal");
	init(cnode, &tpFailed, "tp_failed");
	init(cnode, &tpOff, "tp_off");
	init(cnode, &tpStartFailed, "tp_start_failed");
	init(cnode, &tpStopFailed, "tp_stop_failed");
	init(cnode, &tpStopGEDFailed, "tp_stop_ged_failed");
	init(cnode, &tpStart, "tp_start");
	init(cnode, &tpStop, "tp_stop");
	init(cnode, &tpStopGED, "tp_stop_ged");
	init(cnode, &tpOffRPM, "tp_off_rpm");
	SHOW_TIME(idname, "init transparants...");

	int width 	= guixml->getIntProp(cnode, "tp_width");
	int height 	= guixml->getIntProp(cnode, "tp_height");

	tp = manage( new UIndicator(tpOff.text) );
	tp->modify_on_state_color(tpOff.fgcolor, tpOff.bgcolor);
	tp->modify_off_state_color(tpOff.fgcolor, tpOff.bgcolor);
	tp->set_size_request(width, height);
	tp->signal_finish_blink().connect( sigc::mem_fun(*this, &ControlWindow::on_msg_finish_blink) );
	tp->show();

	SHOW_TIME(idname, "init tpOff...");

	Gtk::HBox* hb = 0;
	GETWIDGET(hb, "Control_hbTarget");
	hb->add(*tp);

	SHOW_TIME(idname, "init Control hbTarget...");
	btnKeyboard = 0;
	GETWIDGET(btnKeyboard, "Control_btnKeyboard");
	btnKeyboard->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnKeyboard_clicked));
	SET_FOCUS_SIG(ControlWindow, btnKeyboard);
	//GUIConf::setFocusStyle(btnKeyboard);
	btnKeyboard->set_sensitive(true);
	btnKeyboardGV = 0;
	GETWIDGET(btnKeyboardGV, "Control_btnKeyboardGV");
	btnKeyboardGV->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnKeyboardGV_clicked));
	SET_FOCUS_SIG(ControlWindow, btnKeyboardGV);
	//GUIConf::setFocusStyle(btnKeyboardGV);
	btnKeyboardGV->set_sensitive(true);

	GETWIDGET(btnKeyboardINT, "Control_btnKeyboardINT");
	btnKeyboardINT->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnKeyboardINT_clicked));
	SET_FOCUS_SIG(ControlWindow, btnKeyboardINT);
	//GUIConf::setFocusStyle(btnKeyboardINT);
	btnKeyboardINT->set_sensitive(true);

	vbKeyBox = 0;
	GETWIDGET(vbKeyBox, "Control_keyBox");

	vbModeBox = 0;
	GETWIDGET(vbModeBox, "Control_modeBox");

	kGV = atof(guixml->getProp(cnode, "kGV").c_str());

	if( kGV <= 0 )
		kGV = 1.0;

	cerr  << "************ kGV=" << kGV <<  endl;

	// --------------------------------------------------------------
	int rpm_maxlen = guixml->getPIntProp(cnode, "rpm_max_lenght", 3);
	int int_maxlen = guixml->getPIntProp(cnode, "int_max_lenght", 2);
	kRPM = guixml->getPIntProp(cnode, "rpm_k", 10);

	entRPM = 0;
	GETWIDGET(entRPM, "Control_rpmEntry");;
	entGV = 0;
	GETWIDGET(entGV, "Control_rpmEntryGV");
	entINT = 0;
	GETWIDGET(entINT, "Control_intEntry");

	string rpm_font(guixml->getProp(cnode, "rpm_font"));

	if( rpm_font.empty() )
		rpm_font = "Helvetica Bold 16";

	entRPM->modify_font(Pango::FontDescription(rpm_font));
	entRPM->set_sensitive(false);
	entRPM->property_max_length() = rpm_maxlen;
	entGV->modify_font(Pango::FontDescription(rpm_font));
	entGV->set_sensitive(false);
	entGV->property_max_length() = rpm_maxlen;
	dlgSetVal = SetValueDialog::Instance();
	SHOW_TIME(idname, "init entRPM...");

	entINT->modify_font(Pango::FontDescription(rpm_font));
	entINT->set_sensitive(false);
	entINT->property_max_length() = int_maxlen;

	winSpec = new ControlWindowSpec("ControlWindowSpec", shm);
	SHOW_TIME(idname, "init ControlWindowSpec...");

	winJournal = WarningsJournal::Instance(shm);
	SHOW_TIME(idname, "init WarningsJournal...");

	dlgPass = PasswordDialog::Instance();

	btnSpecial = 0;
	GETWIDGET(btnSpecial, "btnSpecial");
	btnSpecial->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnSpecWin_clicked));
	SET_FOCUS_SIG(ControlWindow, btnSpecial);
	//GUIConf::setFocusStyle(btnSpecial);
	btnAttach(btnSpecial, KEY_1);
	btnAttach(btnSpecial, KEY_m1);

	GETWIDGET(evben, "evben");
	evben->signal_event().connect(sigc::mem_fun(*this, &ControlWindow::evben_clicked));

	// --------------------------------------------------------------

	btnStart = 0;
	GETWIDGET(btnStart, "btnStart");
	btnStart->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnStart_clicked));
	SET_FOCUS_SIG(ControlWindow, btnStart);
	//GUIConf::setFocusStyle(btnStart);
	btnAttach(btnStart, KEY_7);
	btnAttach(btnStart, KEY_m7);
	// --------------------------------------------------------------
	btnStop = 0;
	GETWIDGET(btnStop, "btnStop");
	btnStop->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnStop_clicked));
	SET_FOCUS_SIG(ControlWindow, btnStop);
	//GUIConf::setFocusStyle(btnStop);
	btnAttach(btnStop, KEY_9);
	btnAttach(btnStop, KEY_m9);
	// --------------------------------------------------------------
	CREATE_BUTTON(btnStopGED, ControlWindow, "btnStopGED");
	// --------------------------------------------------------------
	SHOW_TIME(idname, "init buttons...");

	/*MODE*/
	GETWIDGET(btnMode11, "btnMode11");
	btnMode11->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode11_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode11);
	GETWIDGET(btnMode2, "btnMode2");
	btnMode2->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode2_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode2);
	GETWIDGET(btnMode121, "btnMode121");
	btnMode121->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode121_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode121);
	GETWIDGET(btnMode131, "btnMode131");
	btnMode131->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode131_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode131);
	GETWIDGET(btnMode122, "btnMode122");
	btnMode122->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode122_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode122);
	GETWIDGET(btnMode132, "btnMode132");
	btnMode132->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode132_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode132);
	GETWIDGET(btnMode3, "btnMode3");
	btnMode3->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode3_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode3);
	/*MODE*/

	GETWIDGET(btnOpenPump, "btnOpenPump");
	SET_FOCUS_SIG(ControlWindow, btnOpenPump);
	//btnOpenPump->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnOpenPump_clicked));
	GETWIDGET(btnClosePump, "btnClosePump");
	SET_FOCUS_SIG(ControlWindow, btnClosePump);
	//btnClosePump->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnClosePump_clicked));
	GETWIDGET(btnTest9, "btntest12");
	SET_FOCUS_SIG(ControlWindow, btnTest9);
	GETWIDGET(btnTest18, "btntest18");
	SET_FOCUS_SIG(ControlWindow, btnTest18);
	GETWIDGET(btnIns, "btnIntence");
	btnIns->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnIns_clicked));
	SET_FOCUS_SIG(ControlWindow, btnIns);
	GETWIDGET(btnLFider, "btnLeftFider");
	btnLFider->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnLFider_clicked));
	SET_FOCUS_SIG(ControlWindow, btnLFider);
	GETWIDGET(btnRFider, "btnRightFider");
	btnRFider->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnRFider_clicked));
	SET_FOCUS_SIG(ControlWindow, btnRFider);



	GETWIDGET(btnSpeed, "btnSpeed");
	btnSpeed->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnSpeed_clicked));
	SET_FOCUS_SIG(ControlWindow, btnSpeed);
	GETWIDGET(btnMode, "btnMode");
	btnMode->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnMode_clicked));
	SET_FOCUS_SIG(ControlWindow, btnMode);
	GETWIDGET(btnSControl, "btnSpecialControl");
	btnSControl->signal_clicked().connect(sigc::mem_fun(*this, &ControlWindow::btnSControl_clicked));
	SET_FOCUS_SIG(ControlWindow, btnSControl);

	GETWIDGET(jrnbox, "jrnbox");


	GETWIDGET(vbspeed, "vboxspeed");
	GETWIDGET(vbmode, "vboxmode");
	GETWIDGET(vbscontrol, "vbSControl");
	vbmode->show();
	vbspeed->hide();
	vbscontrol->hide();


	hide();
	vbMain->hide();

	string sname = guixml->getProp(cnode, "post_sensor");
	post_as = conf->getSensorID(sname);

	GETWIDGET(modeFrame, "modeFrame");
	GETWIDGET(hboxcontrol, "hbox29");
	string keyname = guixml->getProp(cnode, "keycontrol_s");
	keyboard_s = conf->getSensorID(keyname);

	//че это а хрень ниже?
	if( cur_set_rpm_as == DefaultObjectId )
	{
		ostringstream err;
		err << "(init): Unknown ID for (post_sensor) " << sname;
		dlog[Debug::CRIT] << myname << err.str() << endl;
		throw SystemError(err.str());
	}

	sname = guixml->getProp(cnode, "rpm_sensor");
	cur_rpm_as = conf->getSensorID(sname);

	//че это а хрень ниже?
	if( cur_set_rpm_as == DefaultObjectId )
	{
		ostringstream err;
		err << "(init): Unknown ID for (rpm_sensor) " << sname;
		dlog[Debug::CRIT] << myname << err.str() << endl;
		throw SystemError(err.str());
	}

	sname = guixml->getProp(cnode, "rpm_set_sensor");
	set_rpm_as = conf->getSensorID(sname);

	//че это а хрень ниже?
	if( cur_set_rpm_as == DefaultObjectId )
	{
		ostringstream err;
		err << "(init): Unknown ID for (rpm_set_sensor) " << sname;
		dlog[Debug::CRIT] << myname << err.str() << endl;
		throw SystemError(err.str());
	}

	sname = guixml->getProp(cnode, "rpm_cur_set_sensor");
	cur_set_rpm_as = conf->getSensorID(sname);

	if( cur_set_rpm_as == DefaultObjectId )
	{
		ostringstream err;
		err << "(init): Unknown ID for (rpm_cur_set_sensor) " << sname;
		dlog[Debug::CRIT] << myname << err.str() << endl;
		throw SystemError(err.str());
	}

	rpm_set_minval = guixml->getIntProp(cnode, "rpm_set_minval");
	rpm_set_maxval = guixml->getIntProp(cnode, "rpm_set_maxval");

	if( rpm_set_maxval <= 0 )
		dlog[Debug::WARN] << myname << "(): максимальные обороты =0!!!" << endl;

	Gtk::Label* lbl = 0;
	GETWIDGET(lbl, "Control_lblRPM");
	{
		ostringstream txt;
		txt << "( " << rpm_set_minval << " <= значение <= " << rpm_set_maxval << " )";
		lbl->set_text(_(txt.str()));

		string lbl_font = guixml->getProp(cnode, "rpm_limit_font");

		if( lbl_font.empty() )
			lbl_font = "Helvetica 10";

		lbl->modify_font(Pango::FontDescription(lbl_font));
	}

	GETWIDGET(lbllfider, "labelLeftFider");
	GETWIDGET(lblrfider, "labelRightFider");


	// --------------------------------------------------
	sname = guixml->getProp(cnode, "setINT_as");
	setINT_as = conf->getSensorID(sname);

	if( setINT_as == DefaultObjectId )
	{
		ostringstream err;
		err << "(init): Unknown ID for (setINT_as) " << sname;
		dlog[Debug::CRIT] << myname << err.str() << endl;
		throw SystemError(err.str());
	}

	sname = guixml->getProp(cnode, "w_setINT_as");
	w_setINT_as = conf->getSensorID(sname);

	if( w_setINT_as == DefaultObjectId )
	{
		ostringstream err;
		err << "(init): Unknown ID for (w_setINT_as) " << sname;
		dlog[Debug::CRIT] << myname << err.str() << endl;
		throw SystemError(err.str());
	}

	INT_minval = guixml->getIntProp(cnode, "INT_minval");
	INT_maxval = guixml->getIntProp(cnode, "INT_maxval");

	GETWIDGET(lbl, "Control_lblINT");
	{
		ostringstream txt;
		txt << "( " << INT_minval << " <= значение <= " << INT_maxval << " )";
		lbl->set_text(_(txt.str()));

		string lbl_font = guixml->getProp(cnode, "INT_limit_font");

		if( lbl_font.empty() )
			lbl_font = "Helvetica 10";

		lbl->modify_font(Pango::FontDescription(lbl_font));
	}

	// --------------------------------------------------

	// Настройка индикатора выбранного поста
	{
		//переделал на transparant
		//Gtk::HBox* hb = 0;
		//GETWIDGET(hb, "hbPost_VO");

		xmlNode* tpNode = guixml->findNode(cnode, "tp_post");

		if( !tpNode )
			throw SystemError("Не найден узел 'tp_post' для ControlWindow");

		UniXML_iterator tit(tpNode);

		// по умолчанию
		string bgcolor 	= tit.getProp("bgcolor");
		string fgcolor 	= tit.getProp("fgcolor");
		string tpfont 	= tit.getProp("font");

		if( !tit.goChildren() )
			throw SystemError("Not found state list for 'tp_post'");

		// Наполняем список
#warning встроить проверку на дублирование....

		for(; tit; tit.goNext())
		{
			int v = tit.getIntProp("value");

			TPStateInfo si;
			si.text 	= tit.getProp("text");
			uniset::extensions::escape_string(si.text);

			si.font 	= tit.getProp("font");
			si.fgcolor 	= tit.getProp("fgcolor");
			si.bgcolor 	= tit.getProp("bgcolor");

			if( si.font.empty() )
				si.font = tpfont;

			if( si.bgcolor.empty() )
				si.bgcolor = bgcolor;

			if( si.fgcolor.empty() )
				si.fgcolor = fgcolor;

			tpPostMap[v] = si;
		}

		int w = guixml->getPIntProp(tpNode, "width", 145);
		int h = guixml->getPIntProp(tpNode, "height", 24);

		tpPost = manage( new UIndicator("---", tpfont) );
		tpPost->set_size_request(w, h);
		tpPost->show();

		//hb->add(*tpPost);
	}
	SHOW_TIME(idname, "init posts...");

	sname 	= guixml->getProp(cnode, "mode_as");
	mode_as = conf->getSensorID(sname);
	sname 	= guixml->getProp(cnode, "pmode_as");
	pmode_as = conf->getSensorID(sname);
	startTimeout_ms = guixml->getPIntProp(cnode, "start_timeout_ms", 5000);
	stopTimeout_ms = guixml->getPIntProp(cnode, "stop_timeout_ms", 5000);
	stopGEDTimeout_ms = guixml->getPIntProp(cnode, "stop_ged_timeout_ms", 5000);

	setTimeout_ms = guixml->getPIntProp(cnode, "set_timeout_ms", 5000);

	valRun1 = guixml->getIntProp(cnode, "running1_val");

	if( valRun1 <= 0 )
		dlog[Debug::WARN] << myname << ": valRun1<=0!!!! " << endl;

	valRun2 = guixml->getIntProp(cnode, "running2_val");

	if( valRun2 <= 0 )
		dlog[Debug::WARN] << myname << ": valRun2<=0!!!! " << endl;

	valReady2 = guixml->getPIntProp(cnode, "ready2_val", 2);

	sname 	= guixml->getProp(cnode, "keycontrol_s");
	keyControl_s  = conf->getSensorID(sname);

	sname 		= guixml->getProp(cnode, "start_s");
	start_s  	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "stop_s");
	stop_s  	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "stop_ged_s");
	stop_ged_s	= conf->getSensorID(sname);


	sname 		= guixml->getProp(cnode, "numalarm");
	numalarm	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "numwarn");
	numwarn		= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "btnJrn");
	typebtnJrn	= conf->getSensorID(sname);

	sname 		= guixml->getProp(cnode, "intens");
	intens	= conf->getSensorID(sname);


	sname 		= guixml->getProp(cnode, "mode11_s");
	mode11_S	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "mode2_s");
	mode2_S		= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "mode121_s");
	mode121_S	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "mode131_s");
	mode131_S	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "mode122_s");
	mode122_S	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "mode132_s");
	mode132_S	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "mode3_s");
	mode3_S		= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "lastmode_s");
	lastmode	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "ins");
	ins		= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "lfider");
	lfider	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "rfider");
	rfider	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "r_lfider");
	rlfider	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "r_rfider");
	rrfider	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "workfider");
	workfider	= conf->getSensorID(sname);


	sname 		= guixml->getProp(cnode, "closepump_s");
	closepump_s	= conf->getSensorID(sname);
	sname 		= guixml->getProp(cnode, "openpump_s");
	openpump_s	= conf->getSensorID(sname);


	out = new TriggerOUT<ControlWindow, ObjectId, bool>(this, &ControlWindow::setOut);
	out->add(start_s, false);
	out->add(stop_s, false);
	out->add(stop_ged_s, false);

	out->add(mode11_S, false);
	out->add(mode2_S, false);
	out->add(mode121_S, false);
	out->add(mode131_S, false);
	out->add(mode122_S, false);
	out->add(mode132_S, false);
	out->add(mode3_S, false);
	out->add(lastmode, false);
	out->add(lfider, false);
	out->add(rfider, false);
	out->add(rlfider, false);
	out->add(rrfider, false);
	out->add(numalarm, false);
	out->add(numwarn, false);
	out->add(typebtnJrn, false);
	out->add(workfider, false);


	out->add(closepump_s, false);
	out->add(openpump_s, false);

	setNumLock(false);

	// по умолчанию пока не выбран МПУ
	// управление отключено...
	set_control(false);
}

// -------------------------------------------------------------------------
ControlWindow::~ControlWindow()
{
	delete winSpec;
	//	delete winJournal;
	delete out;
}
// -------------------------------------------------------------------------
void ControlWindow::init( xmlNode* cnode, tpInfo* ti, const string name )
{
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();
	ti->text = guixml->getProp(cnode, name);

	{
		ostringstream cname;
		cname << name << "_bgcolor";
		ti->bgcolor = guixml->getProp(cnode, cname.str());
	}

	{
		ostringstream cname;
		cname << name << "_fgcolor";
		ti->fgcolor = guixml->getProp(cnode, cname.str());
	}

	{
		ostringstream cname;
		cname << name << "_font";
		ti->font = guixml->getProp(cnode, cname.str());

		if( ti->font.empty() )
			ti->font = guixml->getProp(cnode, "tp_default_font");
	}
}
// -------------------------------------------------------------------------
Gtk::Widget* ControlWindow::get_widget()
{
	return vbMain;
}
// -------------------------------------------------------------------------
void ControlWindow::show()
{
	ChildFrame::show();

	if( onControl )
		keyControl ? tp_set_state(tpDefault) : tp_set_state(tpOffRPM);
	else
		tp_set_state(tpOff);

	vbMain->show();

	// для защиты от случайного(удерживания) нажатия Enter
	// выставляем фокус на кнопку "Специальные функции"
	btnSpecial->grab_focus();	// btnExit->grab_focus();

	ModeDefault();
	vbmode->show();
}
// -------------------------------------------------------------------------
void ControlWindow::hide()
{
	ChildFrame::hide();
	vbMain->hide();
	winSpec->hide();
	Gtk::VBox* vben;
	GETWIDGET(vben, "vbEngineer");
	vben->hide();
	on_msg_finish_blink();
	connSetTimeout.disconnect();
	waitSetRPM = false;
	connStartTimeout.disconnect();
	waitStart = false;
	connStopTimeout.disconnect();
	waitStop = false;
	connStopGEDTimeout.disconnect();
	waitStopGED = false;

	btnStopGED->set_sensitive(onControl);
	btnStop->set_sensitive(onControl);
	btnStart->set_sensitive(onControl);

	connSetINTTimeout.disconnect();
	waitSetINT = false;

	prev_KeyControl = !prev_KeyControl;
	ModeDefault();
}
// -------------------------------------------------------------------------
bool ControlWindow::unlockExit()
{
	blockExitButton(false);
	return false;
}
// -------------------------------------------------------------------------
void ControlWindow::btnKeyboard_clicked()
{
	if( !keyControl )
		return;

	tp_set_state(tpDefault, false);

	float f_set_rpm = (float)cur_set_rpm / (float)kRPM;

	ostringstream s;
	s << f_set_rpm;

	string sval = dlgSetVal->run(s.str(), "Задание оборотов ГЭД", rpm_minval, rpm_maxval);
	long nval = str2RPM( sval );

	cout << "btnKeyboard_clicked= " << nval << endl;

	if( nval != cur_set_rpm )
	{
		blockExitButton(true);
		update_set_rpm(nval);
		rpm_entry_finish_input(Gtk::RESPONSE_OK);
	}
}
// -------------------------------------------------------------------------
long ControlWindow::str2RPM( const string& ss )
{
	string s(ss);

	//setlocale(LC_NUMERIC,"C");
	string::size_type pos = s.find(",");

	while( pos != string::npos )
	{
		s.replace(pos, 1, ".");
		pos = s.find(",");
	}

	// setlocale(LC_NUMERIC,"C");
	float f_nval = atof( s.c_str() );
	return lroundf( f_nval * kRPM );
}
// -------------------------------------------------------------------------
void ControlWindow::btnKeyboardGV_clicked()
{
	if( !keyControl )
		return;

	tp_set_state(tpDefault, false);
	long val_gv = lroundf( (float)cur_set_rpm / kGV ); // RPM --> GV
	long nval = dlgSetVal->run(val_gv, "Задание оборотов ГВ", lroundf( (float)rpm_set_minval / kGV), lroundf( (float)rpm_set_maxval / kGV) );

	if( nval != val_gv )
	{
		nval = lroundf( (float)nval * kGV ); // GV --> RPM
		update_set_rpm(nval);
		blockExitButton(true);
		rpm_entry_finish_input(Gtk::RESPONSE_OK);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::btnKeyboardINT_clicked()
{
	if( !keyControl )
		return;

	tp_set_state(tpDefault, false);

	long nval = dlgSetVal->run(r_setINT, "Задание интенсивности", INT_minval, INT_maxval);

	if( nval != r_setINT )
	{
		update_setINT(nval);
		blockExitButton(true);
		int_entry_finish_input(Gtk::RESPONSE_OK);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::rpm_entry_finish_input( Gtk::ResponseType res )
{
	if( res == Gtk::RESPONSE_OK )
	{
		// Здесь необходимо сделать несколько попыток!!!
		// for( int i=0; i<count; i ++ )
		// {

		// \warning Проверка на то, что не введены буквы не производится,
		// т.к. на клавиатуре нет "букв". (warning: хотя есть символы *,-,+,/ и '.')
		//  - Может быть ситуация когда все цифры стёрли и случайно нажали ввод,
		// получится ноль (остановка)!
		// Чтобы этого не произошло этот случай обрабатывается отдельно (получая сперва строку)
		string sval(entRPM->get_text());

		long val = str2RPM( sval );

		cerr << "set val: " << sval << " lval=" << val << endl;

		SetErrors err(erNone);

		bool checkVal = ( rpm_set_minval != 0 || rpm_set_maxval != 0 );

		try
		{
			if( !sval.empty() && ( !checkVal || checkVal && (val >= rpm_set_minval && val <= rpm_set_maxval) ) )
			{
				setRPM(val);
				// OK
				//connUnblockExit = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::unlockExit),200);
				tp_set_state(tpOK, true);
				rpm_entry_set(false);
				connSetTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::setTimeout), setTimeout_ms);
				waitSetRPM = true;
				tp->blink(300, (setTimeout_ms / 300) );
				set_rpm = val;
				prev_CurSetRPM++; // имитируем изменение, чтобы вызвался sensorInfo
				return;
			}
			else if( !sval.empty() )
				err = erBadValue;

			// если не ввели значение
			// то вернём старое
			if( sval.empty() )
				update_set_rpm(set_rpm);
		}
		catch( uniset::TimeOut )
		{
			dlog[Debug::WARN] << "(finish_input): set RPM Timeout()..." << endl;
			err = erTimeout;
		}
		catch( Exception& ex )
		{
			err = erTimeout;
			dlog[Debug::CRIT] << "(finish_input): " << ex << endl;
		}
		catch(CORBA::SystemException& ex)
		{
			err = erInternal;
			dlog[Debug::CRIT] << "(finish_input): (CORBA::SystemException): "
							  << ex.NP_minorString() << endl;
		}
		catch( omniORB::fatalException& fe )
		{
			err = erInternal;
			dlog[Debug::CRIT] << "(finish_input): omniORB::fatalException:" << endl;
			dlog[Debug::CRIT] << "  file: " << fe.file() << endl;
			dlog[Debug::CRIT] << "  line: " << fe.line() << endl;
			dlog[Debug::CRIT] << "  mesg: " << fe.errmsg() << endl;
		}
		catch(...)
		{
			err = erInternal;
		}

		switch(err)
		{
			case erNone:
				break;

			case erTimeout:
				tp_set_state(tpTimeout, true);
				break;

			case erBadValue:
				tp_set_state(tpBadValue, true);
				break;

			case erInternal:
				tp_set_state(tpInternal, true);
				break;

			default:
				tp_set_state(tpRepeat, true);
				break;
		}

		tp->blink(300, blinkCount);
	}
	else if( res == Gtk::RESPONSE_CANCEL )
	{
		dlog[Debug::INFO] << "(finish_input): [CANCEL]..." << endl;
		connUnblockExit = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::unlockExit), 200);
		update_set_rpm(set_rpm);
		tp_set_state(tpDefault);
		rpm_entry_set(false);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::int_entry_finish_input( Gtk::ResponseType res )
{
	if( res == Gtk::RESPONSE_OK )
	{
		string sval(entINT->get_text());
		long val = uni_atoi(sval);
		SetErrors err = erNone;

		bool checkVal = ( INT_minval != 0 || INT_maxval != 0 );

		try
		{
			if( !sval.empty() && ( !checkVal || checkVal && (val >= INT_minval && val <= INT_maxval) ) )
			{
				setINT(val);
				tp_set_state(tpOK, true);
				int_entry_set(false);
				//connSetINTTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::setINTTimeout), setTimeout_ms);
				//waitSetINT = true;
				tp->blink(300, (setTimeout_ms / 300) );
				w_setINT = val;
				prev_SetINT++; // имитируем изменение, чтобы вызвался sensorInfo
				return;
			}
			else if( !sval.empty() )
				err = erBadValue;

			// если не ввели значение
			// то вернём старое
			if( sval.empty() )
				update_setINT(r_setINT);
		}
		catch( uniset::TimeOut )
		{
			dlog[Debug::WARN] << "(finish_input): set INT Timeout()..." << endl;
			err = erTimeout;
		}
		catch( Exception& ex )
		{
			err = erTimeout;
			dlog[Debug::CRIT] << "(finish_input): " << ex << endl;
		}
		catch(CORBA::SystemException& ex)
		{
			err = erInternal;
			dlog[Debug::CRIT] << "(finish_input): (CORBA::SystemException): "
							  << ex.NP_minorString() << endl;
		}
		catch( omniORB::fatalException& fe )
		{
			err = erInternal;
			dlog[Debug::CRIT] << "(finish_input): omniORB::fatalException:" << endl;
			dlog[Debug::CRIT] << "  file: " << fe.file() << endl;
			dlog[Debug::CRIT] << "  line: " << fe.line() << endl;
			dlog[Debug::CRIT] << "  mesg: " << fe.errmsg() << endl;
		}
		catch(...)
		{
			err = erInternal;
		}

		switch(err)
		{
			case erNone:
				break;

			case erTimeout:
				tp_set_state(tpTimeout, true);
				break;

			case erBadValue:
				tp_set_state(tpBadValue, true);
				break;

			case erInternal:
				tp_set_state(tpInternal, true);
				break;

			default:
				tp_set_state(tpRepeat, true);
				break;
		}

		tp->blink(300, blinkCount);
	}
	else if( res == Gtk::RESPONSE_CANCEL )
	{
		dlog[Debug::INFO] << "(finish_input): [CANCEL]..." << endl;
		connUnblockExit = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::unlockExit), 200);
		update_setINT(r_setINT);
		tp_set_state(tpDefault);
		int_entry_set(false);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::btnSpecWin_clicked()
{
	dlog[Debug::INFO] << myguiname << ": btnSpecWin_clicked..." << endl;
	winSpec->win_activate();
}
bool ControlWindow::evben_clicked(GdkEvent* event)
{

	GdkEventButton event_btn = event->button;

	if((event_btn.type == GDK_2BUTTON_PRESS))
	{
		if (dlgPass->check("987"))
		{

			dlog[Debug::INFO] << myguiname << ": btnSpecWin_clicked..." << endl;
			jrnbox->hide();
			winSpec->win_activate();
		}
	}

	return false;
}

// -------------------------------------------------------------------------
void ControlWindow::init_first_time()
{
	// чтобы первый раз обязательно прошёл sensorInfo() (см. poll() UpdateXX)
	// намеренно делаем так, чтобы prev_xxx не совпадало с текущим в SM

	getState(shm, keyControl_s, prev_KeyControl, ditKeyControl);
	prev_KeyControl ^= true;

	getValue(shm, post_as, prev_Post, aitPost);
	prev_Post--;

	getValue(shm, mode_as, prev_Mode, aitMode);
	prev_Mode--;

	getValue(shm, pmode_as, prev_PMode, aitPMode);
	prev_PMode--;

	getValue(shm, set_rpm_as, prev_SetRPM, aitSetRPM);
	prev_SetRPM--;

	getValue(shm, cur_set_rpm_as, prev_CurSetRPM, aitCurSetRPM);
	prev_CurSetRPM--;

	getValue(shm, r_setINT, prev_SetINT, r_aitSetINT);
	prev_SetINT--;

	getValue(shm, numalarm, prev_numalarm, r_numalarm);
	prev_numalarm--;

	getValue(shm, numwarn, prev_numwarn, r_numwarn);
	prev_numwarn--;

	getValue(shm, intens, prev_intens, r_intens);
	prev_intens--;

	initTime = true;
}
// -------------------------------------------------------------------------
void ControlWindow::sensorInfo( uniset::SensorMessage* sm )
{
	if( sm->id == keyboard_s)
	{
		setDefaultMode();
		modeFrame->set_sensitive(sm->value);
		hboxcontrol->set_sensitive(sm->value);
	}

	if( sm->id == pmode_as)
	{
		if (sm->value == 1)
		{
			rpm_minval = -45; /* предельные обороты для режима ТГ от обоих бортов */
			rpm_maxval = 45;
		}
		else if( sm->value == 7)
		{
			rpm_minval = -20; /* предельные обороты для режима от батарей */
			rpm_maxval = 20;
		}
		else
		{
			rpm_minval = -35; /* предельные обороты для остальных режимов */
			rpm_maxval = 35;
		}
	}

	if( sm->id == cur_set_rpm_as )
	{
		/*
				cout << "(sensorInfo): set_rpm=" << set_rpm
				 << " cur_set_rpm=" << cur_set_rpm
				<< " keyControl=" << keyControl
				<< " cur_set_rpm_as=" << sm->value
				<< " waitSetRPM=" << waitSetRPM
					<< endl;
		*/
		cur_set_rpm = sm->value;

		if( waitSetRPM )
		{
			//		update_set_rpm(sm->value);
			if( set_rpm == cur_set_rpm )
			{
				connSetTimeout.disconnect();
				waitSetRPM = false;
				tp_set_state(tpDefault, false);

				if( onControl && keyControl )
					rpm_entry_set(true);
				else
				{
					update_set_rpm(sm->value);
					setRPM(sm->value);
				}

				blockExitButton(false);
				blockButtons(false);
			}
		}
		else
		{
			// поддерживаем заданные обороты от МПУ
			// в соответсвии с текущими заданными (то ЛСУ ПЧ)
			// чтобы при переходе на управление от МПУ
			// не было "скачка"
			update_set_rpm(sm->value);
			setRPM(sm->value);
		}
	}
	else if( sm->id == post_as )
	{
		TPStateMap::iterator it = tpPostMap.find(sm->value);
		setDefaultMode();

		if( it != tpPostMap.end() )
		{
			if( it->second.text.empty() )
				tpPost->hide();
			else
			{
				tpPost->modify_text(it->second.text, it->second.font);
				tpPost->modify_on_state_color(it->second.fgcolor, it->second.bgcolor);
				tpPost->modify_off_state_color(it->second.fgcolor, it->second.bgcolor);
				tpPost->show();
			}
		}

		switch( sm->value )
		{
			case idMPU:
				set_control(true);
				break;

			default:
			{
				set_control(false);
				dlog[Debug::WARN] << myname << "(sensorInfo): Unknown value PostID=" << sm->value << endl;
			}
			break;
		}
	}
	else if( sm->id == mode_as )
	{
		if( !onControl )
			return;

		if( waitStart )
		{
			if( sm->value == valReady2 || sm->value == valRun1 || sm->value == valRun2 )
			{
				connStartTimeout.disconnect();
				waitStart = false;
				btnStart->set_sensitive(true);

				if( !keyControl )
					tp_set_state(tpOffRPM, true);
				else
					tp_set_state(tpDefault, true);

				if( vbMain->is_visible() )
				{
					if( keyControl )
					{
						btnStart->set_state(Gtk::STATE_NORMAL);
						//btnKeyboard->grab_focus();
					}
				}

				out->set(start_s, false); // setOut(start_s,false);
			}
		}

		if( waitStop )
		{
			if( sm->value != valRun1 && sm->value != valRun2 )
			{
				connStopTimeout.disconnect();
				waitStop = false;
				btnStop->set_sensitive(true);
				btnStopGED->set_sensitive(true);

				if( !keyControl )
					tp_set_state(tpOffRPM, true);
				else
					tp_set_state(tpDefault, true);

				if( vbMain->is_visible() )
					btnStop->grab_focus();

				out->set(stop_s, false); // setOut(stop_s,false);
			}
		}
		else if( waitStopGED )
		{
			if( sm->value != valRun1 && sm->value != valRun2 )
			{
				connStopGEDTimeout.disconnect();
				waitStopGED = false;
				btnStop->set_sensitive(true);
				btnStopGED->set_sensitive(true);

				if( !keyControl )
					tp_set_state(tpOffRPM, true);
				else
					tp_set_state(tpDefault, true);

				if( vbMain->is_visible() )
					btnStopGED->grab_focus();

				out->set(stop_ged_s, false);
			}
		}
	}
	else if( sm->id == keyControl_s )
	{
		keyControl = sm->value;

		if( onControl )
		{
			btnKeyboardINT->set_sensitive(sm->value);
			vbKeyBox->set_sensitive(sm->value);
			btnKeyboardINT->set_sensitive(true);

			if( !waitStop && !waitStart && !waitStopGED )
				tp_set_state(tpDefault, false);

			update_set_rpm(cur_set_rpm);
			update_setINT(r_setINT);
		}

		if( !keyControl )
		{
			waitSetRPM = false;
			waitSetINT = false;

			connSetTimeout.disconnect();
			blockExitButton(false);
			update_set_rpm(cur_set_rpm);
			tp_set_state(tpOffRPM, false);
			// в качестве заданных выставляем текущие заданные
			setRPM(cur_set_rpm);
			update_setINT(r_setINT);
		}
	}
	else if( sm->id == setINT_as )
	{
		r_setINT = sm->value;

		if( waitSetINT )
		{
			if( r_setINT == w_setINT )
			{
				connSetINTTimeout.disconnect();
				waitSetINT = false;
				tp_set_state(tpDefault, false);

				if( onControl && keyControl )
					int_entry_set(true);
				else
				{
					update_setINT(sm->value);
					setINT(sm->value);
				}

				blockExitButton(false);
				blockButtons(false);
			}
		}
		else
		{
			// поддерживаем заданные обороты от МПУ
			// в соответсвии с текущими заданными (то ЛСУ ПЧ)
			// чтобы при переходе на управление от МПУ
			// не было "скачка"
			update_setINT(sm->value);
			setINT(sm->value);
		}
	}

	//	else if( sm->id == cur_rpm_as )
}
// -------------------------------------------------------------------------
void ControlWindow::update_set_rpm( long val )
{
	long gv_val = lroundf( (float)val / kGV );

	if( dlog.debugging(Debug::INFO) )
		dlog[Debug::INFO] << myname << "(update_set_rpm): set_rpm=" << val
						  << " set_rpm_gv=" << gv_val << endl;

	ostringstream sval;
	sval << ( (float)val / float(kRPM) );

	entRPM->set_text(sval.str());
	entRPM->set_position(sval.str().length());

	ostringstream gv_sval;
	gv_sval << gv_val;

	entGV->set_text(gv_sval.str());
	entGV->set_position(gv_sval.str().length());
}
// -------------------------------------------------------------------------
void ControlWindow::update_setINT( long val )
{
	ostringstream sval;
	sval << val;
	entINT->set_text(sval.str());
	entINT->set_position(sval.str().length());
}
// -------------------------------------------------------------------------
void ControlWindow::on_msg_finish_blink()
{
	tp_set_state(tpDefault, false);
	tp->off();

	// имитируем изменение, чтобы вызвался sensorInfo
	prev_SetRPM--;
	prev_SetINT--;
	blockExitButton(false);
}
// -------------------------------------------------------------------------
void ControlWindow::tp_set_state(tpInfo& ti, bool state)
{
	tp->modify_text(ti.text, ti.font);
	// off - всегда Default!!!
	tp->modify_off_state_color(tpDefault.fgcolor, tpDefault.bgcolor);
	tp->modify_on_state_color(ti.fgcolor, ti.bgcolor);
	state ? tp->on() :  tp->off();
}
// -------------------------------------------------------------------------
void ControlWindow::rpm_btn_on_click()
{
	if( !entRPM->is_sensitive() )
		rpm_entry_set(true);
}
// -------------------------------------------------------------------------
void ControlWindow::rpm_entry_set( bool state )
{
	if( state )
	{
		//btnKeyboard->set_sensitive(true);
		vbKeyBox->set_sensitive(true);

		if( vbMain->is_visible() )
			btnKeyboard->grab_focus();

		blockButtons(true);
	}
	else
	{
		vbKeyBox->set_sensitive(false);

		//		entRPM->set_sensitive(false);
		if( vbMain->is_visible() )
		{
			btnKeyboard->grab_focus();
			btnKeyboard->set_state(Gtk::STATE_PRELIGHT);
		}

		blockButtons(false);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::int_entry_set( bool state )
{
	if( state )
	{
		vbKeyBox->set_sensitive(true);

		if( vbMain->is_visible() )
			btnKeyboardINT->grab_focus();

		blockButtons(true);
	}
	else
	{
		vbKeyBox->set_sensitive(false);

		if( vbMain->is_visible() )
		{
			btnKeyboardINT->grab_focus();
			btnKeyboardINT->set_state(Gtk::STATE_PRELIGHT);
		}

		blockButtons(false);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::set_control( bool state )
{
	onControl = state;

	if( state )
	{
		btnStart->set_sensitive(true);
		btnStop->set_sensitive(true);
		btnStopGED->set_sensitive(true);
		vbModeBox->set_sensitive(true);
		vbKeyBox->set_sensitive(keyControl);
		btnKeyboardINT->set_sensitive(keyControl);

		tp->set_sensitive(true);

		if( keyControl )
		{
			tp_set_state(tpDefault, false);
			update_set_rpm(cur_set_rpm);
			// в качестве заданных выставляем текущие заданные
			setRPM(cur_set_rpm);
		}
		else
			tp_set_state(tpOffRPM, false);
	}
	else
	{
		waitSetINT = false;
		waitSetRPM = false;
		waitStart = false;
		waitStop = false;
		waitStopGED = false;
		blockButtons(false);
		blockExitButton(false);
		connUnblockExit.disconnect();
		connStartTimeout.disconnect();
		connStopTimeout.disconnect();
		connSetTimeout.disconnect();
		btnStart->set_sensitive(false);
		btnStop->set_sensitive(false);
		btnStopGED->set_sensitive(false);
		vbKeyBox->set_sensitive(false);
		vbModeBox->set_sensitive(false);
		btnKeyboardINT->set_sensitive(false);
		tp_set_state(tpOff, true);
		tp->set_sensitive(false);
		out->set(start_s, false);
		update_set_rpm(cur_set_rpm);
		// в качестве заданных выставляем текущие заданные
		setRPM(cur_set_rpm);

		update_setINT(r_setINT);
		setINT(r_setINT);
	}
}
bool ControlWindow::btnConfirm(string ask)
{
	Gtk::MessageDialog qdialog( (ask), true, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
	qdialog.set_default_response(Gtk::RESPONSE_NO);
	qdialog.set_position(Gtk::WIN_POS_CENTER);
	qdialog.set_keep_above(true);
	return (qdialog.run() == Gtk::RESPONSE_YES);
}

// -------------------------------------------------------------------------
void ControlWindow::btnStart_clicked()
{
	/*  Диалог подтверждения отключён по желанию заказчика*/
	//if( btnConfirm( string("Запустить ПЧ?") ) )
	//{
	connStopTimeout.disconnect();
	btnStart->set_sensitive(false);
	btnStop->set_sensitive(true);
	btnStopGED->set_sensitive(true);
	connStartTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::startTimeout), startTimeout_ms);
	waitStart = true;
	tp_set_state(tpStart, true);
	tp->blink(300, (startTimeout_ms / 300) );
	out->set(start_s, true);
	//}

	btnStart->grab_focus();
	btnStart->set_state(Gtk::STATE_PRELIGHT);
}
// -------------------------------------------------------------------------
void ControlWindow::btnStop_clicked()
{
	if( btnConfirm("Остановить ПЧ?") )
	{
		connStartTimeout.disconnect();
		btnStop->set_sensitive(false);
		btnStopGED->set_sensitive(true);
		btnStart->set_sensitive(true);
		connStopTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::stopTimeout), stopTimeout_ms);
		waitStop = true;
		tp_set_state(tpStop, true);
		tp->blink(300, (stopTimeout_ms / 300) );
		out->set(stop_s, true);
	}

	btnStop->grab_focus();
	btnStop->set_state(Gtk::STATE_PRELIGHT);
}
// -------------------------------------------------------------------------
void ControlWindow::btnStopGED_clicked()
{
	if( btnConfirm("Разобрать схему?") )
	{
		auto conf = uniset_conf();
		ControlWindow::setDefaultMode();
		connStartTimeout.disconnect();
		connStopTimeout.disconnect();
		btnStopGED->set_sensitive(false);
		btnStart->set_sensitive(true);
		btnStop->set_sensitive(true);
		connStopGEDTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ControlWindow::stopGEDTimeout), stopGEDTimeout_ms);
		waitStopGED = true;
		tp_set_state(tpStopGED, true);
		out->set(stop_ged_s, true);
		IOController_i::SensorInfo si;
		si.id 	= lastmode;
		si.node	= conf->getLocalNode();
		shm->setValue(shm->ID(), 0);
	}

	btnStopGED->grab_focus();
	btnStopGED->set_state(Gtk::STATE_PRELIGHT);

}
// -------------------------------------------------------------------------
bool ControlWindow::startTimeout()
{
	if( !waitStart )
		return false;

	if( onControl )
	{
		btnStart->set_sensitive(true);
		btnStop->set_sensitive(true);
		btnStopGED->set_sensitive(true);
	}

	waitStart = false;

	btnStart->set_state(Gtk::STATE_PRELIGHT);
	btnStart->grab_focus();
	blockButtons(false);
	blockExitButton(false);
	connUnblockExit.disconnect();

	//	setOut(start_s,false);
	out->set(start_s, false);
	return false;
}
// -------------------------------------------------------------------------
bool ControlWindow::stopTimeout()
{
	if( !waitStop )
		return false;

	if( onControl )
	{
		btnStop->set_sensitive(true);
		btnStart->set_sensitive(true);
		btnStopGED->set_sensitive(true);
	}

	waitStop = false;

	tp_set_state(tpStopFailed, true);
	tp->blink(300, blinkCount);

	btnStop->set_state(Gtk::STATE_PRELIGHT);
	btnStop->grab_focus();
	blockButtons(false);
	blockExitButton(false);
	connUnblockExit.disconnect();

	//	setOut(stop_s,false);
	out->set(stop_s, false);
	return false;
}
// -------------------------------------------------------------------------
bool ControlWindow::stopGEDTimeout()
{
	if( !waitStopGED )
		return false;

	if( onControl )
	{
		btnStop->set_sensitive(true);
		btnStart->set_sensitive(true);
		btnStopGED->set_sensitive(true);
	}

	waitStopGED = false;

	tp_set_state(tpStopGEDFailed, true);
	tp->blink(300, blinkCount);

	btnStopGED->set_state(Gtk::STATE_PRELIGHT);
	btnStopGED->grab_focus();
	blockButtons(false);
	blockExitButton(false);
	connUnblockExit.disconnect();

	out->set(stop_ged_s, false);
	return false;
}
// -------------------------------------------------------------------------
bool ControlWindow::setTimeout()
{
	if( !waitSetRPM )
		return false;

	waitSetRPM = false;

	if( keyControl )
		rpm_entry_set(true);

	/*
		cout << "(setTimeout): set_rpm=" << set_rpm
			 << " cur_set_rpm=" << cur_set_rpm
			<< " keyControl=" << keyControl
				<< endl;
	*/
	blockButtons(false);
	blockExitButton(false);
	connUnblockExit.disconnect();

	if( set_rpm == cur_set_rpm )
	{
		tp_set_state(tpDefault, false);
		return false;
	}

	tp_set_state(tpRepeat, true);
	tp->blink(300, blinkCount);
	// возвращаем текущие заданные..
	update_set_rpm(cur_set_rpm);
	setRPM(cur_set_rpm);
	return false;
}
// -------------------------------------------------------------------------
bool ControlWindow::setINTTimeout()
{
	if( !waitSetINT )
		return false;

	waitSetINT = false;

	if( keyControl )
		int_entry_set(true);

	blockButtons(false);
	blockExitButton(false);
	connUnblockExit.disconnect();

	if( r_setINT == w_setINT )
	{
		tp_set_state(tpDefault, false);
		return false;
	}

	tp_set_state(tpRepeat, true);
	tp->blink(300, blinkCount);
	// возвращаем текущие заданные..
	update_setINT(w_setINT);
	setINT(w_setINT);
	return false;
}
// -------------------------------------------------------------------------

void ControlWindow::setOut( uniset::ObjectId sid, bool state )
{
	auto conf = uniset_conf();

	if( !isActivated )
		return;

	IOController_i::SensorInfo si;
	si.id 	= sid;
	si.node	= conf->getLocalNode();

	for( int i = 0; i < 3; i++ )
	{
		try
		{
			shm->setValue(shm->ID(), state);

			if( dlog.debugging(Debug::INFO) )
			{
				dlog[Debug::INFO] << myname << "(setOut): " << conf->oind->getNameById(sid)
								  << "  state=" << state << endl;
			}

			return;
		}
		catch( Exception& ex )
		{
			dlog[Debug::CRIT] << myname << "(setOut): " << ex << endl;
		}
		catch( ... )
		{
			dlog[Debug::CRIT] << myname << "(setOut): catch ..." << endl;
		}

		msleep(100);
	}

	dlog[Debug::CRIT] << myname << "(setOut): failed save " << state <<
					  " for " << conf->oind->getNameById(sid) << endl;

}
// -------------------------------------------------------------------------
// дизайн (реализация подсвечивания кнопок)
// макросы см. GUIConfiguration.h
SET_FOCUS_FUNC_IMPL( ControlWindow, btnStart )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnStop )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnStopGED )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnKeyboard )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnKeyboardGV )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnKeyboardINT )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnSpecial )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode11 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode2 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode121 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode131 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode122 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode132 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode3 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnMode )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnSpeed )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnSControl )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnOpenPump )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnClosePump )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnTest9 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnTest18 )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnIns )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnRFider )
SET_FOCUS_FUNC_IMPL( ControlWindow, btnLFider )
// -------------------------------------------------------------------------
void ControlWindow::poll()
{
	auto conf = uniset_conf();
	updateAS(post_as, prev_Post, &sm, aitPost);
	updateAS(mode_as, prev_Mode, &sm, aitMode);
	updateAS(pmode_as, prev_PMode, &sm, aitPMode);
	updateAS(set_rpm_as, prev_SetRPM, &sm, aitSetRPM);
	updateAS(cur_set_rpm_as, prev_CurSetRPM, &sm, aitCurSetRPM);
	updateDS(keyControl_s, prev_KeyControl, &sm, ditKeyControl);
	updateAS(setINT_as, prev_SetINT, &sm, r_aitSetINT);
	updateAS(numalarm, prev_numalarm, &sm, r_numalarm);
	updateAS(numwarn, prev_numwarn, &sm, r_numwarn);
	updateAS(intens, prev_intens, &sm, r_intens);
	updateAS(lfider, prev_lfider, &sm, r_lfider);
	updateAS(rfider, prev_rfider, &sm, r_rfider);
	updateAS(rlfider, prev_rlfider, &sm, r_rlfider);
	updateAS(rrfider, prev_rrfider, &sm, r_rrfider);
	updateAS(workfider, prev_workfider, &sm, r_workfider);

	(prev_rlfider == 0) ? lbllfider->set_text("Запретить") : lbllfider->set_text("Разрешить");
	(prev_rrfider == 0) ? lblrfider->set_text("Запретить") : lblrfider->set_text("Разрешить");


	IOController_i::SensorInfo si;
	si.id 	= typebtnJrn;
	si.node	= conf->getLocalNode();

	if (prev_numalarm > 0)
	{
		shm->setValue(shm->ID(), 1);
	}
	else if (prev_numwarn > 0)
	{
		shm->setValue(shm->ID(), 2);
	}
	else
		shm->setValue(shm->ID(), 0);

	si.id 	= workfider;
	si.node	= conf->getLocalNode();

	if (prev_rlfider && prev_rrfider)
	{
		shm->setValue(shm->ID(), 1);
	}
	else if(prev_rlfider && !prev_rrfider)
	{
		shm->setValue(shm->ID(), 2);
	}
	else if(!prev_rlfider && prev_rrfider)
	{
		shm->setValue(shm->ID(), 3);
	}
	else
		shm->setValue(shm->ID(), 0);
}
// -------------------------------------------------------------------------
void ControlWindow::initIterators()
{
	shm->initIterator( aitPost );
	shm->initIterator( aitMode );
	shm->initIterator( aitPMode );
	shm->initIterator( aitCurRPM );
	shm->initIterator( aitSetRPM );
	shm->initIterator( aitCurSetRPM );
	shm->initIterator( ditKeyControl );
	shm->initIterator( r_aitSetINT );
	shm->initIterator( w_aitSetINT );
	shm->initIterator( r_numalarm);
	shm->initIterator( r_numwarn);
	shm->initIterator( r_intens);
	shm->initIterator( r_lfider);
	shm->initIterator( r_rfider);
	shm->initIterator( r_rlfider);
	shm->initIterator( r_rrfider);
	shm->initIterator( r_workfider);
	winSpec->initIterators();
	winJournal->initIterators();
	isActivated = true;
	setRPM(0);
}
// -------------------------------------------------------------------------
void ControlWindow::setRPM( long val )
{
	auto conf = uniset_conf();
	IOController_i::SensorInfo si;
	si.id 	= set_rpm_as;
	si.node	= conf->getLocalNode();

	for( int i = 0; i < 3; i++ )
	{
		try
		{
			shm->setValue(shm->ID(), val);
			set_rpm = val;
			update_set_rpm(val);
			break;
		}
		catch(...) {}

		msleep(50);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::setINT( long val )
{
	auto conf = uniset_conf();
	IOController_i::SensorInfo si;
	si.id 	= w_setINT_as;
	si.node	= conf->getLocalNode();

	for( int i = 0; i < 3; i++ )
	{
		try
		{
			shm->setValue(shm->ID(), val);
			w_setINT = val;
			update_setINT(val);
			break;
		}
		catch(...) {}

		msleep(50);
	}
}
// -------------------------------------------------------------------------
void ControlWindow::btnInfo()
{
	Gtk::MessageDialog qdialog( ("Для смены режима \nнеобходимо разобрать схему"), true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
	qdialog.set_position(Gtk::WIN_POS_CENTER);
	qdialog.set_keep_above(true);
	qdialog.run();
}

// -------------------------------------------------------------------------
void ControlWindow::btnMode11_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{
		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode11_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 1);
		}
	}
	else
	{
		btnInfo();
	}
}
// -------------------------------------------------------------------------
void ControlWindow::btnMode2_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{

		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode2_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 4);
		}
	}
	else
	{
		btnInfo();
	}

}
// -------------------------------------------------------------------------
void ControlWindow::btnMode121_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{

		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode121_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 3);
		}
	}
	else
	{
		btnInfo();
	}

}
// -------------------------------------------------------------------------
void ControlWindow::btnMode131_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{

		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode131_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 6);
		}
	}
	else
	{
		btnInfo();
	}

}
// -------------------------------------------------------------------------
void ControlWindow::btnMode122_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{
		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode122_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 2);
		}
	}
	else
	{
		btnInfo();
	}

}
// -------------------------------------------------------------------------
void ControlWindow::btnMode132_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{

		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode132_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 5);
		}
	}
	else
	{
		btnInfo();
	}

}
// -------------------------------------------------------------------------
void ControlWindow::btnMode3_clicked()
{
	auto conf = uniset_conf();

	if( prev_PMode == 0 )
	{

		if( btnConfirm("Вы уверены?") )
		{
			setDefaultMode();
			out->add(mode3_S, true);
			IOController_i::SensorInfo si;
			si.id 	= lastmode;
			si.node	= conf->getLocalNode();
			shm->setValue(shm->ID(), 7);
		}
	}
	else
	{
		btnInfo();
	}

}
// -------------------------------------------------------------------------
void ControlWindow::setDefaultMode()
{
	out->add(mode11_S, false);
	out->add(mode2_S, false);
	out->add(mode121_S, false);
	out->add(mode131_S, false);
	out->add(mode122_S, false);
	out->add(mode132_S, false);
	out->add(mode3_S, false);
}
// -------------------------------------------------------------------------
void ControlWindow::btnOpenPump_clicked()
{
	if( btnConfirm("Вы уверены?") )
	{
		out->add(closepump_s, false);
		out->add(openpump_s, true);
	}

}
// -------------------------------------------------------------------------
void ControlWindow::btnClosePump_clicked()
{

	if( btnConfirm("Вы уверены?") )
	{
		out->add(closepump_s, true);
		out->add(openpump_s, false);
	}

}
// -------------------------------------------------------------------------
void ControlWindow::ModeDefault()
{
	vbmode->hide();
	vbspeed->hide();
	vbscontrol->hide();

}

void ControlWindow::btnMode_clicked()
{
	ModeDefault();
	vbmode->show();
}
// -------------------------------------------------------------------------
void ControlWindow::btnSpeed_clicked()
{
	ModeDefault();
	vbspeed->show();
}
// -------------------------------------------------------------------------
void ControlWindow::btnSControl_clicked()
{
	if (dlgPass->check("987"))
	{
		ModeDefault();
		vbscontrol->show();
	}
}

// -------------------------------------------------------------------------
void ControlWindow::btnIns_clicked()
{
	auto conf = uniset_conf();
	ostringstream s;
	s << prev_intens;
	string sval = dlgSetVal->run(s.str(), "Задание интенсивности", 0, 500);
	long nval = str2RPM( sval ) / 10;
	IOController_i::SensorInfo si;
	si.id 	= ins;
	si.node	= conf->getLocalNode();
	shm->setValue(shm->ID(), nval);
}

void ControlWindow::btnLFider_clicked()
{
	auto conf = uniset_conf();
	IOController_i::SensorInfo si;
	si.id 	= lfider;
	si.node	= conf->getLocalNode();

	if (prev_rlfider == 0)
	{
		shm->setValue(shm->ID(), 1);
	}
	else
	{
		shm->setValue(shm->ID(), 0);
	}

}
void ControlWindow::btnRFider_clicked()
{
	auto conf = uniset_conf();
	IOController_i::SensorInfo si;
	si.id 	= rfider;
	si.node	= conf->getLocalNode();

	if (prev_rrfider == 0)
	{
		shm->setValue(shm->ID(), 1);
	}
	else
	{
		shm->setValue(shm->ID(), 0);
	}

}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------