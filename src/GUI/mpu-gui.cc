#include <libglademm.h>
#include <gtkmm.h>
#include <Exceptions.h>
#include <UniSetTypes.h>
#include <UniXML.h>
#include <extensions/Extensions.h>
#include <MPUConfiguration.h>
#include <GUIConfiguration.h>
#include <MBTCPMultiMaster.h>
#include <MPUSharedMemory.h>
#include <UniSetActivator.h>
#include "MainWindow.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace MPU;
// -------------------------------------------------------------------------
extern void setNumLock( bool state, bool no_emit = false );
// -------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
	Gtk::Main kit(argc, argv);

	setlocale(LC_NUMERIC, "C");

	try
	{
		auto conf = uniset::uniset_init(argc, argv, "configure.xml");
		auto act = UniSetActivator::Instance();

		// ------------ SharedMemory ----------------
		auto shm = MPUSharedMemory::init_mpusmemory(argc, argv);

		if( !shm )
			return 1;

		act->add(shm);

		ObjectId ID(DefaultObjectId);
		string name = conf->getArgParam("--name", "GUIManager1");
		ID = conf->getObjectID(name);

		if( ID == uniset::DefaultObjectId )
		{
			cerr << "(main): идентификатор '" << name
				 << "' не найден в конф. файле!"
				 << " в секции " << conf->getObjectsSection() << endl;
			return 0;
		}

		bool skip_mb = findArgParam("--skip-mb", argc, argv) != -1;

		if( !skip_mb )
		{
			auto mbm = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbtcp");

			if( !mbm )
				{
					cout << "Ошибка создания MBTCPMultiMaster" << endl;
				return 1;
				}

			act->add(mbm);
		}


		std::shared_ptr<IONotifyController> ioc = static_pointer_cast<IONotifyController>(shm);
		Glib::RefPtr<MainWindow> win = MainWindow::init_gui(ID, argc, argv, ioc);

		SystemMessage sm(SystemMessage::StartUp);
		act->broadcast( sm.transport_msg() );
		act->run(true);


		while( !shm->exist() )
		{
			cerr << "...waiting activation..." << endl;
			msleep(1000);
		}

		cerr << "..activation...[OK]" << endl;

		string theme = getArgParam( "--gtkthemedir", argc, argv, "");

		if ( !theme.empty() )
			gtk_rc_parse( (theme + "/gtkrc").c_str() );

		win->activate();
		Gtk::Main::run();
		return 0;
	}
	catch( uniset::SystemError& ex )
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex << endl;
	}
	catch( uniset::Exception& ex )
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex << endl;
	}
	catch(CORBA::NO_IMPLEMENT)
	{
		dlog[Debug::CRIT] << "(GUI:main): CORBA::NO_IMPLEMENT..." << endl;
	}
	catch(CORBA::OBJECT_NOT_EXIST)
	{
		dlog[Debug::CRIT] << "(GUI:main): CORBA::OBJECT_NOT_EXIST..." << endl;
	}
	catch(CORBA::COMM_FAILURE& ex)
	{
		dlog[Debug::CRIT] << "(GUI:main): CORBA::COMM_FAILURE..." << endl;
	}
	catch(CORBA::SystemException& ex)
	{
		dlog[Debug::CRIT] << "(GUI:main): (CORBA::SystemException): "
						  << ex.NP_minorString() << endl;
	}
	catch ( omniORB::fatalException& fe )
	{
		dlog[Debug::CRIT] << "(GUI::main): поймали omniORB::fatalException:" << endl;
		dlog[Debug::CRIT] << "  file: " << fe.file() << endl;
		dlog[Debug::CRIT] << "  line: " << fe.line() << endl;
		dlog[Debug::CRIT] << "  mesg: " << fe.errmsg() << endl;
	}
	catch( Glib::Error& ex )
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex.what() << endl;
	}
	catch( std::exception& ex )
	{
		dlog[Debug::CRIT] << "(GUI::main): std::Exception ..." << ex.what() << endl;
	}
	catch(...)
	{
		dlog[Debug::CRIT] << "(GUI::main): catch ..." << endl;
	}

	return 1;
}
