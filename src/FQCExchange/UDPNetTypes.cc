#include <cstdlib>
#include <algorithm>
#include <sstream>
#include <istream>
#include <iomanip>
#include <cmath>
#include <Exceptions.h>
#include "UDPNetTypes.h"
#include "MPUTypes.h"
#include "MPUConfiguration.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace MPU;
// -------------------------------------------------------------------------
static void udpSetValue( DataTable& dtbl, int tabIndex, const long val, const long prec = 0 )
{
	if( tabIndex < 0 || tabIndex >= dtbl.size() )
		return;

	if( dtbl[tabIndex].invert )
		dtbl[tabIndex].value = val ? 0 : 1 ;

	if( prec != 0 )
		dtbl[tabIndex].value = lroundf( val * prec );
	else
		dtbl[tabIndex].value = val;
}
// -------------------------------------------------------------------------
#define UDP_SET_VALUE(index_name) \
	udpSetValue( dtbl, umap[index_name], (long)msg.index_name )

#define UDP_SET_VALUE_PREC(index_name, prec) \
	udpSetValue( dtbl, umap[index_name], (long)msg.index_name, prec )

void MPU::udpSVUUpdate( DataTable& dtbl, const UDPIndexMap& umap, const UDPnet::UDPDataFrom& msg )
{
	UDP_SET_VALUE(rem_confirm);
	UDP_SET_VALUE(setRPM);
	UDP_SET_VALUE(stop);
	UDP_SET_VALUE(mode_1_1);
	UDP_SET_VALUE(mode_1_2_1);
	UDP_SET_VALUE(mode_1_2_2);
	UDP_SET_VALUE(mode_1_3_1);
	UDP_SET_VALUE(mode_1_3_2);
	UDP_SET_VALUE(mode_ab);
}
// -------------------------------------------------------------------------
#undef UDP_SET_VALUE
// -------------------------------------------------------------------------
static long udpGetValue( DataTable& dtbl, int tabIndex, const long oldValue )
{
	if( tabIndex == DefaultDataTableIndex )
		return oldValue;

	if( tabIndex < 0 || tabIndex >= dtbl.size() )
		return oldValue;

	DataTableItem* it( &(dtbl[tabIndex]) );

	//uniset_spin_lock lock(it->val_lock);
	uniset_rwmutex_rlock lock(it->val_lock);

	if( it->invert )
		return ( it->value ? 0 : 1 );

	return it->value;
}
// -------------------------------------------------------------------------
#define UDP_GET_VALUE(index_name) \
	s_msg.index_name = udpGetValue(dtbl,umap[index_name],s_msg.index_name)

#define UDP_GET_VALUE_PREC(index_name, prec) \
	s_msg.index_name = lroundf( (float)udpGetValue(dtbl,umap[index_name],s_msg.index_name) / (float)prec );

void MPU::udpUpdate( DataTable& dtbl, const UDPIndexMap& umap, UDPnet::UDPDataTo& s_msg )
{
	UDP_GET_VALUE(lsu_mode_du);
	UDP_GET_VALUE(lsu_mode_mu);
	UDP_GET_VALUE(lsu_ready1);
	UDP_GET_VALUE(lsu_ready2);
	UDP_GET_VALUE(rpm);

	UDP_GET_VALUE(allowP);
	UDP_GET_VALUE(P);

	UDP_GET_VALUE(channel1);
	UDP_GET_VALUE(channel2);

	UDP_GET_VALUE(trans1_warn_temp);
	UDP_GET_VALUE(trans1_alarm_temp);
	UDP_GET_VALUE(trans2_warn_temp);
	UDP_GET_VALUE(trans2_alarm_temp);
	UDP_GET_VALUE(ged_warn_temp);
	UDP_GET_VALUE(ged_alarm_temp);

	UDP_GET_VALUE(shu_overheating);
	UDP_GET_VALUE(sb_warn_temp);
	UDP_GET_VALUE(sb_alarm_temp);
	UDP_GET_VALUE(rt_warn_temp);
	UDP_GET_VALUE(rt_alarm_temp);
	UDP_GET_VALUE(bearing_warn_temp);
	UDP_GET_VALUE(bearing_alarm_temp);

	UDP_GET_VALUE(qp1_2);
	UDP_GET_VALUE(qp3_4);
	UDP_GET_VALUE(k1);
	UDP_GET_VALUE(k7);
	UDP_GET_VALUE(qtv1);
	UDP_GET_VALUE(qtv2);
	UDP_GET_VALUE(qc1);

	UDP_GET_VALUE(qf1);
	UDP_GET_VALUE(qf2);
	UDP_GET_VALUE(qs1);
	UDP_GET_VALUE(qs2);

	UDP_GET_VALUE(current_warn);
	UDP_GET_VALUE(current_alarm);
	UDP_GET_VALUE(sed_alarm);
	UDP_GET_VALUE(sed_warn);

	UDP_GET_VALUE(Itv1);
	UDP_GET_VALUE(Itv2);
	UDP_GET_VALUE(Iconst);

	UDP_GET_VALUE(Risol);
	UDP_GET_VALUE(oil_level1);
	UDP_GET_VALUE(oil_level2);
	UDP_GET_VALUE(oil_level3);
	UDP_GET_VALUE(oil_level4);

	UDP_GET_VALUE(water_low);

	UDP_GET_VALUE(c_setRPM);

	UDP_GET_VALUE(c_mode_1_1);
	UDP_GET_VALUE(c_mode_1_2_1);
	UDP_GET_VALUE(c_mode_1_2_2);
	UDP_GET_VALUE(c_mode_1_3_1);
	UDP_GET_VALUE(c_mode_1_3_2);
	UDP_GET_VALUE(c_mode_ab);
	UDP_GET_VALUE(c_mode_1_3_3);
	UDP_GET_VALUE(sb2_warn_temp);
	UDP_GET_VALUE(sb2_alarm_temp);
	UDP_GET_VALUE(rt2_warn_temp);
	UDP_GET_VALUE(rt2_alarm_temp);
	UDP_GET_VALUE(overspeeding_off);
}
// -------------------------------------------------------------------------
#undef UDP_GET_VALUE
// -------------------------------------------------------------------------
