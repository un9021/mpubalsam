#include <cmath>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <UniXML.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>
#include "MPUConfiguration.h"
#include "UDPServer.h"
#include "NetTypes.h"
// --------------------------------------------------------------------------------
using namespace uniset;
using namespace MPU;
using namespace std;
using namespace UDPnet;
// --------------------------------------------------------------------------------
UDPServer::~UDPServer()
{
	delete thr_send;
	delete udp_recv;
	delete udp_send1;
	delete udp_send2;
}
// --------------------------------------------------------------------------------
UDPServer::UDPServer( const std::string& name, xmlNode* cnode ):
	SServer(name, cnode),
	umap(MPU::udpMaxIndex),
	active(false),
	checkPause(500),
	lsuNetOK(false),
	udp_recv(0),
	udp_send1(0),
	udp_send2(0),
	thr_send(0),
	thr_send_priority(0),
	send_initOK(false),
	sendPause(300),
	sendTimeout(100)
{
	auto conf = uniset_conf();
	memset(&send_msg, 0, sizeof(send_msg));

	if( !cnode )
		cnode = conf->getNode("NetConverter");

	if( cnode == NULL )
	{
		cerr << "Not find conf-node for NetConverter" << endl;
		throw SystemError("Not find conf-node for NetConverter");
	}

	UniXML_iterator it(cnode);

	sendPause = conf->getArgPInt("--svu-send-pause", it.getProp("svu_send_pause"), 1000); // вообще по ТЗ 1Гц..
	sendTimeout = conf->getArgPInt("--svu-send-timeout", it.getProp("svu_send_timeout"), 100);
	checkPause = conf->getArgPInt("--svu-check-pause", it.getProp("svu_check_pause"), 500);
	/*
		int tout = conf->getArgPInt("--svu-timeout",it.getProp("svutimeout"),2000);
		ptTimeout.setTiming(tout);
		ip1Timeout.setTiming(tout);
		ip2Timeout.setTiming(tout);
		svu1Timeout.setTiming(tout);
		svu2Timeout.setTiming(tout);
	*/
	int t_stat = conf->getArgPInt("--svu-statistics-msec", it.getProp("svu_statistics_msec"), UniSetTimer::WaitUpTime);
	ptStatistics.setTiming(t_stat);

	// начальная инициализация
	for( size_t i = 0; i < umap.size(); i++ )
		umap[i] = DefaultDataTableIndex;

	initDataTable("direct", w_offset);
	// поскольку в initDataTable происходит сортировка
	// и tblIndex меняется
	// приходится "реинициализировать" индексы
	reinitUDPDataTable();

	// Проверяем корректность
	// Все ли "датчики" найдены
#if 1

	for( size_t i = 0; i < umap.size(); i++ )
	{
		if( umap[i] == DefaultDataTableIndex )
		{
			ostringstream err;
			err << myname << ": Not found sensor for UDPDataTable index '" << i << "'";
			throw SystemError(err.str());
		}
	}

#else
#warning (UDPServer): Временно проверка корректности данных ОТКЛЮЧЕНА

	if( dlog.debugging(Debug::INFO) )
	{
		for( size_t i = 0; i < umap.size(); i++ )
		{
			dlog[Debug::INFO] << myname << "(init): [" << i << "]=" << umap[i] << endl;

			if( umap[i] > 0 )
				dlog[Debug::INFO] << " [" << i << "].sid=" << dtbl[ umap[i] ].sid << endl;
		}
	}

#endif

	/*
		thr_ip1_priority  = conf->getArgPInt("--svu-thread-ip1-priority",it.getProp("svu_thread_ip1_priority"),0);
		thr_ip2_priority  = conf->getArgPInt("--svu-thread-ip2-priority",it.getProp("svu_thread_ip2_priority"),0);
		thr_send_priority  = conf->getArgPInt("--svu-thread-send-priority",it.getProp("svu_thread_send_priority"),0);
	*/
	string ip1 = conf->getArgParam("--svu-recv-ip1", it.getProp("recv_ip1"));
	string ip2 = conf->getArgParam("--svu-recv-ip2", it.getProp("recv_ip2"));
	int svu_port = conf->getArgPInt("--svu1-port", it.getProp("svu_port"), 2002);

	string ip1_send = conf->getArgParam("--svu-broadcast-ip1", it.getProp("broadcast_ip1"));
	string ip2_send = conf->getArgParam("--svu-broadcast-ip2", it.getProp("broadcast_ip2"));
	int port1_send = conf->getArgPInt("--svu-broadcast-port1", it.getProp("broadcast_port1"), 2000);
	int port2_send = conf->getArgPInt("--svu-broadcast-port2", it.getProp("broadcast_port2"), 2000);

	try
	{
		udp_recv = new UDPSystemExchange("SVU", cnode, ip1, ip2, svu_port);
		udp_recv->connectProcessing( sigc::mem_fun(this, &UDPServer::svuProcessing) );

		try
		{
			udp_send1 = new UDPSender("SEND-ETH1", ip1_send, port1_send);
		}
		catch( uniset::SystemError& err )
		{
			ostringstream s;
			s << myname << "(init): " << err << endl;
			dlog[Debug::CRIT] << s.str() << endl;
			udp_send1 = 0;
			//throw SystemError(s.str());
		}

		try
		{
			udp_send2 = new UDPSender("SEND-ETH2", ip2_send, port2_send);
		}
		catch( uniset::SystemError& err )
		{
			ostringstream s;
			s << myname << "(init): " << err << endl;
			dlog[Debug::CRIT] << s.str() << endl;
			udp_send2 = 0;
			//throw SystemError(s.str());
		}

		if( !udp_send1 && !udp_send2 )
		{
			ostringstream s;
			s << myname << "(init): Can`t open 'send connection' for " << ip1_send << ":" << port1_send
			  << " and " << ip2_send << ":" << port2_send << endl;
			dlog[Debug::CRIT] << s.str() << endl;
			throw SystemError(s.str());
		}
	}
	catch( ost::SockException& e )
	{
		ostringstream s;
		s << myname << "(init): " << e.getString() << ": " << e.getSystemErrorString() << endl;
		dlog[Debug::CRIT] << s.str() << endl;
		throw SystemError(s.str());
	}

	thr_send = new ThreadCreator<UDPServer>(this, &UDPServer::sendThread);

	setRespondID( conf->getSensorID(it.getProp("svu_respond_id")) );
	dlog[Debug::INFO] << myname << "(init): SVU respond " << it.getProp("svu_respond_id") << endl;

	setAskCountID( conf->getSensorID(it.getProp("svu_askcount_id")) );
	dlog[Debug::INFO] << myname << "(init): SVU askcount " << it.getProp("svu_askcount_id") << endl;

}
// --------------------------------------------------------------------------------
#define BIND_IF(NAME, IND, INVERT, DIRECT) \
	if( nm == NAME )\
	{\
		umap[IND] = i; \
		ti.invert = INVERT; \
		ti.direct = DIRECT; \
		ti.extdata = new int(IND); \
		return true; \
	}\

	bool UDPServer::addNewTabElement( size_t i, MPU::DataTableItem& ti, const UniXML_iterator& it )
	{
		// index всё равно изменится из-за сортировки в SServer::initDataTable
		const string nm(it.getProp("name"));

		// Можно было бы делать привязку сразу к регистрам..
		// но так универсальнее..
		//BIND_IF("name",UDPTabIndex,invert,direct);

		// FROM SVU
		BIND_IF("w_RemoteConfirm_S", MPU::rem_confirm, false, MPU::dFromSVU);
		BIND_IF("w_ksuGED_TargetRPM_AS", MPU::setRPM, false, MPU::dFromSVU);
		BIND_IF("w_svu_Stop_S", MPU::stop, false, MPU::dFromSVU);
		BIND_IF("w_svu_Mode_1_1_S", MPU::mode_1_1, false, MPU::dFromSVU);
		BIND_IF("w_svu_Mode_1_2_1_S", MPU::mode_1_2_1, false, MPU::dFromSVU);
		BIND_IF("w_svu_Mode_1_2_2_S", MPU::mode_1_2_2, false, MPU::dFromSVU);
		BIND_IF("w_svu_Mode_1_3_1_S", MPU::mode_1_3_1, false, MPU::dFromSVU);
		BIND_IF("w_svu_Mode_1_3_2_S", MPU::mode_1_3_2, false, MPU::dFromSVU);
		BIND_IF("w_svu_Mode_AB_S", MPU::mode_ab, false, MPU::dFromSVU);
		// TO SVU
		BIND_IF("ctlSVU_isOn_S", MPU::lsu_mode_du, false, MPU::dToSVU);
		BIND_IF("ctlPMU_isOn_S", MPU::lsu_mode_mu, false, MPU::dToSVU);
		BIND_IF("LSUNotReady1_S", MPU::lsu_ready1, true, MPU::dToSVU);
		BIND_IF("LSUNotReady2_S", MPU::lsu_ready2, true, MPU::dToSVU);
		BIND_IF("rpm_svu_AS", MPU::rpm, false, MPU::dToSVU);
		BIND_IF("K_PowerLimit_AS", MPU::allowP, false, MPU::dToSVU);
		BIND_IF("FQC1_Power_AS", MPU::P, false, MPU::dToSVU);  /* Рассчётное значение мощности ГЭД */

		// ------------------------------------------------------------------------------------------------
		/*
		 * т.к. с каким каналом работаем, выставляется на нашем уровне (см. setToSM)
		 * но при этом может быть интересно знать контроллеру ПЧ, то делаем датчики w_
		*/
		BIND_IF("w_svu_channel1_S", MPU::channel1, false, MPU::dToSVU);  /* управление по первому каналу */
		BIND_IF("w_svu_channel2_S", MPU::channel2, false, MPU::dToSVU);  /* управление по второму каналу  */
		// ------------------------------------------------------------------------------------------------

		BIND_IF("Trans1Temp_Warn_S", MPU::trans1_warn_temp, false, MPU::dToSVU); /* Повышенная температура трансформатора N1 */
		BIND_IF("Trans1Temp_Alarm_S", MPU::trans1_alarm_temp, false, MPU::dToSVU); /* Предельная температура трансформатора N1 */
		BIND_IF("Trans2Temp_Warn_S", MPU::trans2_warn_temp, false, MPU::dToSVU); /* Повышенная температура трансформатора N2 */
		BIND_IF("Trans2Temp_Alarm_S", MPU::trans2_alarm_temp, false, MPU::dToSVU); /* Предельная температура трансформатора N2 */
		BIND_IF("GEDTemp_Warn_S", MPU::ged_warn_temp, false, MPU::dToSVU);  /* Повышенная температура ГЭД */
		BIND_IF("GEDTemp_Alarm_S", MPU::ged_alarm_temp, false, MPU::dToSVU);  /* Предельная температура ГЭД */
		BIND_IF("lsu_shu_overheating_S", MPU::shu_overheating, false, MPU::dToSVU);  /* Перегрев ШУ */
		BIND_IF("lsu_SB_Temp_Warn_S", MPU::sb_warn_temp, false, MPU::dToSVU);  /* Повышенная температура силового блока */
		BIND_IF("lsu_SB_Temp_Alarm_S", MPU::sb_alarm_temp, false, MPU::dToSVU);  /* Предельная температура силового блока */
		BIND_IF("lsu_BTR_Temp_Warn_S", MPU::rt_warn_temp, false, MPU::dToSVU);  /* Повышенная температура тормозных резисторов */
		BIND_IF("lsu_BTR_Temp_Alarm_S", MPU::rt_alarm_temp, false, MPU::dToSVU);  /* Предельная температура тормозных резисторов */
		BIND_IF("lsu_Podship_Temp_Warn_S", MPU::bearing_warn_temp, false, MPU::dToSVU);  /* Повышенная температура подшипника */
		BIND_IF("lsu_Podship_Temp_Alarm_S", MPU::bearing_alarm_temp, false, MPU::dToSVU);  /* Предельная температура подшипника */
		BIND_IF("QP1_2_S", MPU::qp1_2, false, MPU::dToSVU);  /* состояние QP1_2 */
		BIND_IF("QP3_4_S", MPU::qp3_4, false, MPU::dToSVU);  /* состояние QP3_4 */
		BIND_IF("K1_S", MPU::k1, false, MPU::dToSVU);  /* состояние K1 */
		BIND_IF("K7_S", MPU::k7, false, MPU::dToSVU);  /* состояние K7 */
		BIND_IF("QTV1_S", MPU::qtv1, false, MPU::dToSVU);  /* состояние QTV1 */
		BIND_IF("QTV2_S", MPU::qtv2, false, MPU::dToSVU);  /* состояние QTV2 */
		BIND_IF("QC1_S", MPU::qc1, false, MPU::dToSVU);  /* состояние QC1 */
		BIND_IF("QF1_S", MPU::qf1, false, MPU::dToSVU);  /* состояние QF1 */
		BIND_IF("QF2_S", MPU::qf2, false, MPU::dToSVU);  /* состояние QF2 */
		BIND_IF("QS1_S", MPU::qs1, false, MPU::dToSVU);  /* состояние QS1 */
		BIND_IF("QS2_S", MPU::qs2, false, MPU::dToSVU);  /* состояние QS2 */
		BIND_IF("GEDCurrent_Warn_S", MPU::current_warn, false, MPU::dToSVU);  /* повышенная нагрузка по току */
		BIND_IF("GEDCurrent_Alarm_S", MPU::current_alarm, false, MPU::dToSVU); /* предельная нагрузка по току */
		BIND_IF("sed_alarm_S", MPU::sed_alarm, false, MPU::dToSVU);  /* Авария СЭД */
		BIND_IF("sed_warn_S", MPU::sed_warn , false, MPU::dToSVU);  /* Неисправность СЭД */
		BIND_IF("Itv1_AS", MPU::Itv1, false, MPU::dToSVU);  /* Питающий фидер к TV1, ток фазы */
		BIND_IF("Itv2_AS", MPU::Itv2, false, MPU::dToSVU);  /* Питающий фидер к TV2, ток фазы */
		BIND_IF("lsu_DC_I_AS", MPU::Iconst, false, MPU::dToSVU);  /* Питающий фидер постоянного тока, ток */
		BIND_IF("Risol_LowLevel_S", MPU::Risol, false, MPU::dToSVU);  /* снижение сопротивления изоляции ВСЭД */
		BIND_IF("oil_level1_S", MPU::oil_level1, false, MPU::dToSVU);  /* Уровень масла 1 */
		BIND_IF("oil_level2_S", MPU::oil_level2, false, MPU::dToSVU);  /* Уровень масла 2 */
		BIND_IF("oil_level3_S", MPU::oil_level3, false, MPU::dToSVU);  /* Уровень масла 3 */
		BIND_IF("oil_level4_S", MPU::oil_level4, false, MPU::dToSVU);  /* Уровень масла 4 */
		BIND_IF("water_low_S", MPU::water_low, false, MPU::dToSVU);  /* понижение расхода охлаждающей воды на ВСЭД */
		BIND_IF("Confirm_svu_rpm_AS", MPU::c_setRPM , false, MPU::dToSVU);  /* (подтверждение): Заданные обороты,  об/мин */

		BIND_IF("svu_Mode_1_1_S", MPU::c_mode_1_1, false, MPU::dToSVU);
		BIND_IF("svu_Mode_1_2_1_S", MPU::c_mode_1_2_1, false, MPU::dToSVU);
		BIND_IF("svu_Mode_1_2_2_S", MPU::c_mode_1_2_2, false, MPU::dToSVU);
		BIND_IF("svu_Mode_1_3_1_S", MPU::c_mode_1_3_1, false, MPU::dToSVU);
		BIND_IF("svu_Mode_1_3_2_S", MPU::c_mode_1_3_2, false, MPU::dToSVU);
		BIND_IF("svu_Mode_AB_S", MPU::c_mode_ab, false, MPU::dToSVU);
		BIND_IF("svu_Mode_1_3_3_S", MPU::c_mode_1_3_3, false, MPU::dToSVU);
		BIND_IF("lsu_SB2_Temp_Warn_S", MPU::sb2_warn_temp, false, MPU::dToSVU);  /* Повышенная температура силового блока N2 */
		BIND_IF("lsu_SB2_Temp_Alarm_S", MPU::sb2_alarm_temp, false, MPU::dToSVU);  /* Предельная температура силового блока N2  */
		BIND_IF("lsu_BTR2_Temp_Warn_S", MPU::rt2_warn_temp, false, MPU::dToSVU);  /* Повышенная температура тормозных резисторов  N2 */
		BIND_IF("lsu_BTR2_Temp_Alarm_S", MPU::rt2_alarm_temp, false, MPU::dToSVU);  /* Предельная температура тормозных резисторов  N2 */

		BIND_IF("Overspeeding_Off_S", MPU::overspeeding_off, false, MPU::dToSVU); /*!< Превышение допустимой частоты вращения с разрбором схемы */

		return true;
	}
	// --------------------------------------------------------------------------------
	void UDPServer::reinitUDPDataTable()
	{
		for( size_t i = 0; i < dtbl.size(); i++ )
		{
			DataTableItem* item = &(dtbl[i]);

			if( item->sid == DefaultObjectId )
				continue;

			if( item->extdata == 0 )
			{
				item->direct = dNone;
				continue;
			}

			int* udpIndex = (int*)(item->extdata);

			umap[ (UDPTabIndex)(*udpIndex) ] = i;

			delete ((int*)(item->extdata));
			item->extdata = 0;
		}
	}
	// --------------------------------------------------------------------------------
	void UDPServer::help_print( int argc, const char* const* argv )
	{
		cout << "--svu-timeout msec             - timeout for receive packets" << endl;
		cout << "--svu-reinit-timeout           - reinit timeout for reopen connection for receive" << endl;
		cout << "--svu-thread-ip1-priority N    - priority exchange thread. 0 - default." << endl;
		cout << "--svu-thread-ip2-priority N    - priority exchange thread. 0 - default." << endl;
		cout << "--svu-thread-send-priority N   - priority exchange thread. 0 - default." << endl;
		cout << "--svu-recv-ip1 ip              - ip for ethernet channel 1 (receiver)" << endl;
		cout << "--svu-recv-ip2 ip              - ip for ethernet channel 2 (recevier)" << endl;
		cout << "--svu1-port port               - port for SVU1 (receiver)" << endl;
		cout << "--svu2-port port               - port for SVU2 (receiver)" << endl;
		cout << "--svu-broadcast-ip1 ip         - ip for broadcast send channel 1 (sender)" << endl;
		cout << "--svu-broadcast-port1 port     - port for broadcast send channel 1 (sender)" << endl;
		cout << "--svu-broadcast-ip2 ip         - ip for broadcast send channel 2 (sender)" << endl;
		cout << "--svu-broadcast-port2 port     - port for broadcast send channel 2 (sender)" << endl;
		cout << "--svu-send-pause msec          - Send period. Default 300 msec." << endl;
		cout << "--svu-send-timeout msec        - Timeout for send one packet. Default 50 msec." << endl;
		cout << "--svu-check-pause msec         - Check timeout`s period. Default 1000 msec." << endl;
		cout << "--svu-statistics-msec msec     - Show exchange statistics period." << endl;
		cout << "** Debug log: " << endl;
		cout << "  INFO,WARN,CRIT - standart log level." << endl;
		cout << "  LEVEL5 - statistics" << endl;
		cout << "  LEVEL8 - show send packets" << endl;
		cout << "  LEVEL9 - show receive packets" << endl;
	}
	// -------------------------------------------------------------------------
	void UDPServer::sigterm( int signo )
	{
		active = false;
	}
	// --------------------------------------------------------------------------------
	void UDPServer::stop()
	{
		if( active )
		{
			active = false;

			if( udp_recv )
				udp_recv->stop();

			thr_send->stop();
		}
	}
	// --------------------------------------------------------------------------------
	void UDPServer::start()
	{
		if(!active )
		{
			active = true;

			if( udp_recv )
				udp_recv->start();

			thr_send->start();
		}
	}
	// --------------------------------------------------------------------------------
	bool UDPServer::isLinkOK( int num )
	{
		return ( udp_recv && udp_recv->isLinkOK(num) );
	}
	// --------------------------------------------------------------------------------
	void UDPServer::terminate( int signo )
	{

	}
	// --------------------------------------------------------------------------------
	void UDPServer::setTimeout( int msec )
	{

	}
	// --------------------------------------------------------------------------------
	void UDPServer::updateData( const std::shared_ptr<uniset::SMInterface>& shm )
	{
		for( MPU::DataTable::iterator it = dtbl.begin(); it != dtbl.end(); ++it )
		{
			if( it->direct == dToSVU )
				updateItem(shm, &(*it), dTo);
			else if( it->direct == dFromSVU )
				updateItem(shm, &(*it), dFrom);
		}
	}
	// --------------------------------------------------------------------------------
	void UDPServer::setToSM( const std::shared_ptr<uniset::SMInterface>& shm )
	{
		int i = 0;

		for( UDPIndexMap::iterator it = umap.begin(); it != umap.end(); ++it, i++ )
		{
			if( (*it) == DefaultDataTableIndex )
				continue;

			DataTableItem* e( &(dtbl[ (*it) ]) );

			// эти датчики мы выставляем сами.
			// т.к состояние этих датчиков выставляем мы и СВУ их "читает" (e->direct=dToSVU)
			// но для ПЧ они тоже как "чтение".. то искуственно их обновляем здесь как
			// "dFrom"
			if( i == MPU::channel1 )
			{
				e->value = ( udp_recv->getWorkChannel() == 1 ? 1 : 0 );
				updateItem(shm, e, dFrom);
				continue;
			}
			else if( i == MPU::channel2 )
			{
				e->value = ( udp_recv->getWorkChannel() == 2 ? 1 : 0 );
				updateItem(shm, e, dFrom);
				continue;
			}
			else if( i == MPU::setRPM )
			{
				//uniset_spin_lock lock(e->val_lock, 70);
				uniset_rwmutex_rlock lock(e->val_lock);
				long val = dtbl[ umap[i] ].value * K_RPM;

				if(e->sType == UniversalIO::AI)
					shm->localSetValue(e->ait, e->sid, val, shm->ID() );
				else if(e->sType == UniversalIO::AO)
					shm->localSetValue(e->ait, e->sid, val, shm->ID() );

				continue;
			}

			if( e->direct == dFromSVU )
				updateItem(shm, e, dFrom);
		}
	}
	// -------------------------------------------------------------------------
	void UDPServer::getFromSM( const std::shared_ptr<uniset::SMInterface>& shm )
	{
		int i = 0;

		for( UDPIndexMap::iterator it = umap.begin(); it != umap.end(); ++it, i++ )
		{
			if( (*it) == DefaultDataTableIndex )
				continue;

			DataTableItem* e( &(dtbl[ *it ]) );

			if( i == MPU::rpm || i == MPU::c_setRPM )
			{
				uniset_rwmutex_rlock lock(e->val_lock);
				e->value = lroundf( shm->localGetValue(e->ait, e->sid) / (float)K_RPM );
			}
			else if( e->direct == dToSVU )
				updateItem(shm, e, dTo);
		}

		// А теперь обновляем данные в сообщении для ответа
		// уже из нашей таблицы
		{
			std::lock_guard<std::mutex> l(send_mutex);
			udpUpdate(dtbl, umap, send_msg);

			// и выставим флаг, что данные обновлены
			std::lock_guard<std::mutex> l1(lsu_mutex);

			if( lsuNetOK )
				send_initOK = true;
		}
	}
	// -------------------------------------------------------------------------
	long UDPServer::getAskCount( int num ) const
	{
		if( udp_recv )
			return udp_recv->getAskCount(num);

		return 0;
	}
	// -------------------------------------------------------------------------
	long UDPServer::getErrCount( int num ) const
	{
		/*
			if( num == 1 && udp_luga )
				return udp_luga->getErrCount();
			if( num == 2 )
				return udp_andromeda->getErrCount();
		*/
#warning getErrCount() не реализована
		return 0;
	}
	// -------------------------------------------------------------------------
	void UDPServer::execute()
	{
		// сперва ждём паузу, чтобы успели старатнуть потоки обмена...
		msleep(checkPause);

		while( active )
		{
			if( udp_recv )
				udp_recv->step();

			if( ptStatistics.checkTime() )
			{
				ptStatistics.reset();

				if( dlog.debugging(Debug::LEVEL5) )
				{
					cout << " ******* STAT ****** " << endl;

					if( udp_recv )
						cout << udp_recv << endl;
				}
			}

			msleep(checkPause);
		}
	}
	// --------------------------------------------------------------------------------
	void UDPServer::setLSUNetOK( bool set )
	{
		{
			std::lock_guard<std::mutex> l(lsu_mutex);
			lsuNetOK = set;
		}

		if( trLsuOK.low(lsuNetOK) )
		{
			// выставим флаг, что данные недействительны
			std::lock_guard<std::mutex> l(send_mutex);
			send_initOK = false;
		}
	}
	// -------------------------------------------------------------------------
	void UDPServer::sendThread()
	{
		if( dlog.debugging(Debug::INFO) )
		{
			dlog[Debug::INFO] << myname << "(sendThread): activate...("
							  << ( udp_send1 ? udp_send1->getLinkName() : "send1 NOT Run" ) << " and "
							  << ( udp_send2 ? udp_send2->getLinkName() : "send2 NOT Run" ) << ")" << endl;
		}

		if( thr_send_priority )
		{
			dlog[Debug::INFO] << myname << "(sendThread): set priority " << thr_send_priority << endl;
			int res = nice(thr_send_priority);

			if( res < 0 )
			{
				if( dlog.debugging(Debug::WARN) )
				{
					dlog[Debug::WARN] << myname << "(sendThread): SET PRIORITY FAILED. err(" << errno << "): "
									  << strerror(errno)
									  << endl;
				}
			}
		}

		bool sendOK  = false;

		while( active )
		{
			//		cerr << myname << ": ************ LSU_OK= " << lsuNetOK << endl;

			// есть ли связь с ЛСУ
			{
				std::lock_guard<std::mutex> l(lsu_mutex);
				sendOK = lsuNetOK;
			}

			if( sendOK )
			{
				// Если есть, то действительны ли данные
				std::lock_guard<std::mutex> l(send_mutex);
				sendOK = send_initOK ? true : false;
			}

			if( !sendOK )
			{
				msleep(sendPause);
				continue;
			}


			{
				// lock send_msg
				std::lock_guard<std::mutex> l(send_mutex);

				if( dlog.debugging(Debug::LEVEL8) )
					dlog[Debug::LEVEL8] << myname << "(send): " << send_msg << endl;

				try
				{
					if( udp_send1 )
						udp_send1->send(send_msg, sendTimeout);
				}
				catch(...) {}

				try
				{
					if( udp_send2 )
						udp_send2->send(send_msg, sendTimeout);
				}
				catch(...) {}
			} // unlock send_msg

			msleep(sendPause);
		}
	}
	// -------------------------------------------------------------------------
	void UDPServer::svuProcessing( const UDPnet::UDPDataFrom& msg, bool force )
	{
		std::lock_guard<std::mutex> l1(data_mutex);
		udpSVUUpdate(dtbl, umap, msg);
	}
	// -------------------------------------------------------------------------
	bool UDPServer::isLinkOK() const
	{
		return udp_recv && udp_recv->isLinkOK();
	}
	// -------------------------------------------------------------------------
