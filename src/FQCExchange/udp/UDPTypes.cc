// -------------------------------------------------------------------------
#include <string>
#include <ostream>
#include <sstream>
#include <iomanip>
#include "UDPTypes.h"
// -------------------------------------------------------------------------
using namespace UDPnet;
using namespace std;
// -------------------------------------------------------------------------
std::ostream& UDPnet::operator<<(std::ostream& os, UDPnet::UDPDataFrom& m )
{
	return os << endl << setfill(' ')
				<< endl << setw(15) << " remConfirm=" << (int)m.rem_confirm
				<< endl << setw(15) << " setRPM=" << (int)m.setRPM
				<< endl << setw(15) << " stop=" << (int)m.stop
				<< endl << setw(15) << " mode1.1=" << (int)m.mode_1_1
				<< endl << setw(15) << " mode1.2.1=" << (int)m.mode_1_2_1
				<< endl << setw(15) << " mode1.2.2=" << (int)m.mode_1_2_2
				<< endl << setw(15) << " mode1.3.1=" << (int)m.mode_1_3_1
				<< endl << setw(15) << " mode1.3.2=" << (int)m.mode_1_3_2
				<< endl << setw(15) << " modeAB=" << (int)m.mode_ab
				<< endl;
}
// -------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, UDPnet::UDPDataFrom* m )
{
	return os << (*m);
}
// -------------------------------------------------------------------------
std::ostream& UDPnet::operator<<(std::ostream& os, UDPnet::UDPDataTo& m )
{
	ostringstream tmp;
    tmp.setf(ios::right, ios::adjustfield);
	tmp	<< setfill(' ')
		<< endl << setw(20) << " lsu_mode_du=" << (int)m.lsu_mode_du
		<< endl << setw(20) << " lsu_mode_mu=" << (int)m.lsu_mode_mu
		<< endl << setw(20) << " lsu_ready1=" << (int)m.lsu_ready1
		<< endl << setw(20) << " lsu_ready2=" << (int)m.lsu_ready2
		<< endl << setw(20) << " rpm=" << m.rpm
		<< endl << setw(20) << " allowP=" << m.allowP
		<< endl << setw(20) << " P=" << m.P
		<< endl << setw(20) << " channel1=" << (int)m.channel1
		<< endl << setw(20) << " channel2=" << (int)m.channel2
		<< endl << setw(20) << " trans1_warn_temp=" << (int)m.trans1_warn_temp
		<< endl << setw(20) << " trans1_alarm_temp=" << (int)m.trans1_alarm_temp
		<< endl << setw(20) << " trans2_warn_temp=" << (int)m.trans2_warn_temp
		<< endl << setw(20) << " trans2_alarm_temp=" << (int)m.trans2_alarm_temp
		<< endl << setw(20) << " ged_warn_temp=" << (int)m.ged_warn_temp
		<< endl << setw(20) << " ged_alarm_temp=" << (int)m.ged_alarm_temp
		<< endl << setw(20) << " shu_overheating=" << (int)m.shu_overheating
		<< endl << setw(20) << " sb_warn_temp=" << (int)m.sb_warn_temp
		<< endl << setw(20) << " sb_alarm_temp=" << (int)m.sb_alarm_temp
		<< endl << setw(20) << " rt_warn_temp=" << (int)m.rt_warn_temp
		<< endl << setw(20) << " rt_alarm_temp=" << (int)m.rt_alarm_temp
		<< endl << setw(20) << " bearing_warn_temp=" << (int)m.bearing_warn_temp
		<< endl << setw(20) << " bearing_alarm_temp=" << (int)m.bearing_alarm_temp
		<< endl << setw(20) << " qp1_2=" << (int)m.qp1_2
		<< endl << setw(20) << " qp3_4=" << (int)m.qp3_4
		<< endl << setw(20) << " k1=" << (int)m.k1
		<< endl << setw(20) << " k7=" << (int)m.k7
		<< endl << setw(20) << " qtv1=" << (int)m.qtv1
		<< endl << setw(20) << " qtv2=" << (int)m.qtv2
		<< endl << setw(20) << " qc1=" << (int)m.qc1
		<< endl << setw(20) << " qf1=" << (int)m.qf1
		<< endl << setw(20) << " qf2=" << (int)m.qf2
		<< endl << setw(20) << " qs1=" << (int)m.qs1
		<< endl << setw(20) << " qs2=" << (int)m.qs2
		<< endl << setw(20) << " current_warn=" << (int)m.current_warn
		<< endl << setw(20) << " current_alarm=" << (int)m.current_alarm
		<< endl << setw(20) << " sed_alarm=" << (int)m.sed_alarm
		<< endl << setw(20) << " sed_warn=" << (int)m.sed_warn
		<< endl << setw(20) << " Itv1=" << m.Itv1
		<< endl << setw(20) << " Itv2=" << m.Itv2
		<< endl << setw(20) << " Iconst=" << m.Iconst
		<< endl << setw(20) << " Risol=" << (int)m.Risol
		<< endl << setw(20) << " oil_level1=" << (int)m.oil_level1
		<< endl << setw(20) << " oil_level2=" << (int)m.oil_level2
		<< endl << setw(20) << " oil_level3=" << (int)m.oil_level3
		<< endl << setw(20) << " oil_level4=" << (int)m.oil_level4
		<< endl << setw(20) << " water_low=" << (int)m.water_low
		<< endl << setw(20) << " c_setRPM=" << (int)m.c_setRPM
		<< endl << setw(20) << " mode_1_1=" << (int)m.c_mode_1_1
		<< endl << setw(20) << " mode_1_2_1=" << (int)m.c_mode_1_2_1
		<< endl << setw(20) << " mode_1_2_2=" << (int)m.c_mode_1_2_2
		<< endl << setw(20) << " mode_1_3_1=" << (int)m.c_mode_1_3_1
		<< endl << setw(20) << " mode_1_3_2=" << (int)m.c_mode_1_3_2
		<< endl << setw(20) << " mode_ab=" << (int)m.c_mode_ab
		<< endl << setw(20) << " mode_1_3_3=" << (int)m.c_mode_1_3_3
		<< endl << setw(20) << " sb2_warn_temp=" << (int)m.sb2_warn_temp
		<< endl << setw(20) << " sb2_alarm_temp=" << (int)m.sb2_alarm_temp
		<< endl << setw(20) << " rt2_warn_temp=" << (int)m.rt2_warn_temp
		<< endl << setw(20) << " rt2_alarm_temp=" << (int)m.rt2_alarm_temp
		<< endl << setw(20) << " overspeeding_off=" << (int)m.overspeeding_off
		<< endl;
	
	return os << tmp.str();
}
// -------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, UDPnet::UDPDataTo* m )
{
	return os << (*m);
}
// -------------------------------------------------------------------------
std::string UDPnet::udpErr2Str( UDPnet::udpErrCode e )
{
	switch( e )
	{
		case erNoError:
			return "";

		case erInvalidFormat:
			return "неправильный формат";
		
		case erBadCheckSum:
			return "У пакета не сошлась контрольная сумма";
			
		case erBadReplyNodeAddress:
			return "Ответ на запрос адресован не мне или от станции,которую не спрашивали";
			
		case erTimeOut:
			return "Тайм-аут при приеме";
			
		case erUnExpectedPacketType:
			return "Неожидаемый тип пакета";
				
		case erPacketTooLong:
			return "пакет длинее буфера приема";
			
		case erHardwareError:
			return "ошибка оборудования";
	
		case erBadDataAddress:
			return "регистр не существует или запрещён к опросу";

		case erBadDataValue:
			return "значение не входит в разрешённый диапазон";

		case erAnknowledge:
			return "запрос принят в исполнению, но ещё не выполнен";

		case erSlaveBusy:
			return "контроллер занят длительной операцией (повторить запрос позже)";
		
		case erOperationFailed:
			return "сбой при выполнении операции (например: доступ запрещён)";
			
		case erMemoryParityError:
			return "ошибка паритета при чтении памяти";

		default:
			return "Неизвестный код ошибки";
	}
}
// -------------------------------------------------------------------------