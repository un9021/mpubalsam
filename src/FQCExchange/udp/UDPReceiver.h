#ifndef UDPReceiver_H_
#define UDPReceiver_H_
// -------------------------------------------------------------------------
#include <string>
#include <cc++/socket.h>
#include <Debug.h>
#include "UDPTypes.h"

// -------------------------------------------------------------------------
/*!	Приём UDP сообщений на заданном ip:port */
class UDPReceiver
{
	public:
		UDPReceiver( const std::string& name, const std::string& s_host, ost::tpport_t port );
		~UDPReceiver();
		
		void stop();
		
		/*! обработать очередное сообщение 
			\param msecTimeout 	- время ожидания прихода очередного сообщения в мсек.
		*/
		UDPnet::udpErrCode receive( UDPnet::UDPDataFrom& msg, uniset::timeout_t msecTimeout=TIMEOUT_INF );

		void initLog( DebugStream& dlog );

		inline int getRecvCount(){ return recv_cnt; }

		const std::string getLinkName();

		void reinit();
	
	protected:
		std::string myname;
		DebugStream dlog;

		unsigned int recv_cnt; /*!< количество принятых собщений */
		
		ost::UDPDuplex* udp;
		ost::IPV4Host host;
		ost::tpport_t port;
		std::string s_host;

	private:

};
// -------------------------------------------------------------------------
#endif // UDPReceiver_H_
// -------------------------------------------------------------------------
