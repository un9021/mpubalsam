#include <iostream>
#include <sstream>
#include <iomanip>
#include <UniXML.h>
#include <unistd.h>
#include <error.h>
#include <errno.h> 
#include "MPUConfiguration.h"
#include "UDPSystemExchange.h"
// --------------------------------------------------------------------------------
using namespace uniset;
using namespace MPU;
using namespace std;
using namespace UDPnet;
// --------------------------------------------------------------------------------
UDPSystemExchange::~UDPSystemExchange()
{
	delete thr_ip1;
	delete thr_ip2;
	delete udp_ip1;
	delete udp_ip2;
}
// --------------------------------------------------------------------------------
UDPSystemExchange::UDPSystemExchange( const std::string name, xmlNode* cnode, 
								 const std::string ip1, const std::string ip2, int port ):
	myname(name),
	ip1LinkOK(false),
	ip2LinkOK(false),
	ip1AckCount(0),
	ip2AckCount(0),
	active(false),
	proc_ip(0),
	udp_ip1(0),
	thr_ip1(0),
	thr_ip1_priority(0),
	udp_ip2(0),
	thr_ip2(0),
	thr_ip2_priority(0)
{
	auto conf = uniset_conf();
	memset(&empty_msg,0,sizeof(empty_msg));
	
	if( cnode == NULL )
	{
		cerr << myname << " cnode=NULL" << endl;
		throw SystemError(myname+" cnode=NULL"); 
	}
	
	UniXML_iterator it(cnode);

	int tout = conf->getArgPInt("--svu-timeout",it.getProp("svu_timeout"),3000);
	ip1Timeout.setTiming(tout);
	ip2Timeout.setTiming(tout);

	//waitTimeout = TIMEOUT_INF;
	waitTimeout = conf->getArgPInt("--svu-reinit-timeout",it.getProp("svu_reinit_timeout"),10000);

	thr_ip1_priority  = conf->getArgPInt("--svu-thread-ip1-priority",it.getProp("svu_thread_ip1_priority"),0);
	thr_ip2_priority  = conf->getArgPInt("--svu-thread-ip2-priority",it.getProp("svu_thread_ip2_priority"),0);

	try
	{	
		udp_ip1 = new UDPReceiver(myname,ip1,port);
	}
	catch( uniset::SystemError& err )
	{
		ostringstream s;
		s << myname << "(init): " << err << endl;
		dlog[Debug::CRIT] << s.str() << endl;
		udp_ip1 = 0;
		//throw SystemError(s.str());
	}
	
	try
	{	
		udp_ip2 = new UDPReceiver(myname,ip2,port);
	}
	catch( uniset::SystemError& err )
	{
		ostringstream s;
		s << myname << "(init): " << err << endl;
		dlog[Debug::CRIT] << s.str() << endl;
		udp_ip2 = 0;
		//throw SystemError(s.str());
	}
	
	if( !udp_ip1 && !udp_ip2 )
	{
		ostringstream s;
		s << myname << "(init): Can`t create socket for " << ip1 << ":" << port
			<< " and " << ip2 << ":" << port << endl;
		dlog[Debug::CRIT] << s.str() << endl;
		throw SystemError(s.str());
	}
	
	if( udp_ip1 )
		thr_ip1	= new ThreadCreator<UDPSystemExchange>(this, &UDPSystemExchange::ip1Thread);

	if( udp_ip2 )
		thr_ip2	= new ThreadCreator<UDPSystemExchange>(this, &UDPSystemExchange::ip2Thread);
}
// --------------------------------------------------------------------------------
void UDPSystemExchange::sigterm( int signo )
{
	active = false;
}
// --------------------------------------------------------------------------------
void UDPSystemExchange::stop()
{
	if( active )
	{
		active = false;
		if( thr_ip1 )
			thr_ip1->stop();
		if( thr_ip2 )
			thr_ip2->stop();
	}
}
// --------------------------------------------------------------------------------
void UDPSystemExchange::start()
{
	if(!active )
	{
		active = true;
		if( thr_ip1 )
			thr_ip1->start();
		if( thr_ip2 )
			thr_ip2->start();
	}
}
// --------------------------------------------------------------------------------
bool UDPSystemExchange::isLinkOK( int num )
{
	if( num == 1 )
		return ip1LinkOK;
	if( num == 2 )
		return ip2LinkOK;
	
	return false;
}
// --------------------------------------------------------------------------------
void UDPSystemExchange::terminate( int signo )
{

}
// --------------------------------------------------------------------------------
long UDPSystemExchange::getAskCount( int num )
{
	if( num == 1 )
		return ip1AckCount;
	if( num == 2 )
		return ip2AckCount;
	
	return 0;
}
// -------------------------------------------------------------------------
void UDPSystemExchange::step()
{
	{
		//uniset_mutex_lock l(ip_mutex, 1000);
		std::lock_guard<std::mutex> l(ip_mutex);
		ip1LinkOK = ip1Timeout.checkTime() ? false : true;
		ip2LinkOK = ip2Timeout.checkTime() ? false : true;
		
		if( proc_ip==1 && !ip1LinkOK )
			proc_ip = 0;
		else if( proc_ip==2 && !ip2LinkOK )
			proc_ip = 0;
			
		/*! \todo Надо выяснить переключаться ли принудительно на первый канал, как только по нему появилась связь */
		if( proc_ip == 0 )
		{
			if( ip1LinkOK )
				proc_ip = 1;
			else if( ip2LinkOK )
				proc_ip = 2;
		}
	}

	// Если связь с СВУ пропала обнуляем данные
	
	//убираею принудительное обнуление, т.к. на ходу обнуляяются обороты при пропаже свзяи
/*	if( trNetOK.low( isLinkOK() ) )
	{
		uniset_mutex_lock l(data_mutex, 110);
		slProcessing(empty_msg,true);
	}
*/
}
// --------------------------------------------------------------------------------
void UDPSystemExchange::ip1Thread()
{
	dlog[Debug::INFO] << myname << "(ip1Thread): activate...(" 
			<< udp_ip1->getLinkName() << ")"<< endl;

	if( thr_ip1_priority )
	{
		dlog[Debug::INFO] << myname << "(ip1Thread): set priority " << thr_ip1_priority << endl;
		int res = nice(thr_ip1_priority);
		if( res < 0 )
		{
			dlog[Debug::WARN] << myname << "(ip1Thread): SET PRIORITY FAILED. err(" << errno << "): " 
				<< strerror(errno)
				<< endl;
		}
	}

	// Сборсим таймеры
	{
		std::lock_guard<std::mutex>  l(ip_mutex);
		ip1Timeout.reset();
	}
	
	while( active )
	{
		try
		{
			UDPnet::udpErrCode er = udp_ip1->receive(ip1_rbuf,waitTimeout);
			if( er != UDPnet::erNoError )
			{
				/*! \todo Доделать ведение статистики ошибок */
				if( er == erTimeOut )
				{
//					if( dlog.debugging(Debug::LEVEL9) )
//						dlog[Debug::LEVEL9] << myname << ": IP1 reinit " << endl;
					//udp_ip1->reinit();
				}
				
				continue;
			}

			{
				std::lock_guard<std::mutex>  l(ip_mutex);
				std::lock_guard<std::mutex>  l1(data_mutex);
				memcpy(&ip1_msg,&ip1_rbuf,sizeof(ip1_msg));
				ip1Timeout.reset();
				ip1AckCount++;
			}
			// --------- ОБРАБОТКА -----------
			if( dlog.debugging(Debug::LEVEL9) )
				dlog[Debug::LEVEL9] << myname << "(ip1): recv: " << ip1_msg << endl;
			
			{
				std::lock_guard<std::mutex>  l(ip_mutex);
				
				// обрабатываем данные только, если нет обработки по другому каналу
				if( proc_ip != 1 )
					continue;
			}

			{
				std::lock_guard<std::mutex>  l1(data_mutex);
				slProcessing(ip1_msg,false);
			}
		}
		catch(...)
		{
			// на всякий, если будут сыпаться исключения..
			if( dlog.debugging(Debug::CRIT) )
				dlog[Debug::CRIT] << myname << "(ip1Thread): catch..." << endl;
			msleep(1000);
		}
	}
}
// -------------------------------------------------------------------------
void UDPSystemExchange::ip2Thread()
{
	dlog[Debug::INFO] << myname << "(ip2Thread): activate...(" 
			<< udp_ip2->getLinkName() << ")"<< endl;
	
	if( thr_ip2_priority )
	{
		dlog[Debug::INFO] << myname << "(ip2Thread): set priority " << thr_ip2_priority << endl;
		int res = nice(thr_ip2_priority);
		if( res < 0 )
		{
			dlog[Debug::WARN] << myname << "(ip2Thread): SET PRIORITY FAILED. err(" << errno << "): " 
				<< strerror(errno)
				<< endl;
		}
	}

	// Сборсим таймеры
	{
		std::lock_guard<std::mutex>  l(ip_mutex);
		ip2Timeout.reset();
	}
	
	while( active )
	{
		try
		{
			UDPnet::udpErrCode er = udp_ip2->receive(ip2_rbuf,waitTimeout);
			if( er != UDPnet::erNoError )
			{
				/*! \todo Доделать ведение статистики ошибок */
				if( er == erTimeOut )
				{
					//if( dlog.debugging(Debug::LEVEL9) )
					//	dlog[Debug::LEVEL9] << myname << ": IP2 reinit " << endl;
					//udp_ip2->reinit();
				}
	
				continue;
			}

			{
				std::lock_guard<std::mutex>  l(ip_mutex);
				std::lock_guard<std::mutex>  l1(data_mutex);
				memcpy(&ip2_msg,&ip2_rbuf,sizeof(ip2_msg));
				ip2Timeout.reset();
				ip2AckCount++;
			}
			// --------- ОБРАБОТКА -----------	
			if( dlog.debugging(Debug::LEVEL9) )
				dlog[Debug::LEVEL9] << myname << "(ip2): recv: " << ip2_msg << endl;
			
			{
				std::lock_guard<std::mutex>  l(ip_mutex);
					
				// обрабатываем данные только, если нет обработки по другому каналу
				if( proc_ip != 2 )
					continue;
			}

			{
				std::lock_guard<std::mutex>  l1(data_mutex);
				slProcessing(ip2_msg,false);
			}
		}
		catch(...)
		{
			// на всякий, если будут сыпаться исключения..
			if( dlog.debugging(Debug::CRIT) )
				dlog[Debug::CRIT] << myname << "(ip2Thread): catch..." << endl;
			msleep(1000);
		}
	}
}
// -------------------------------------------------------------------------
void UDPSystemExchange::connectProcessing( ProcessingSlot sl )
{
	slProcessing = sl;
}
// -------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, UDPSystemExchange* u )
{
	return os << (*u);
}

std::ostream& operator<<(std::ostream& os, UDPSystemExchange& u )
{
	return os << u.myname << ":"
				<< " proc_ip=" << u.proc_ip
				<< " ip1=" << u.ip1LinkOK
				<< " ip2=" << u.ip2LinkOK
				<< " ip1_askCount=" << u.ip1AckCount
				<< " ip2_askCount=" << u.ip2AckCount;
}
// -------------------------------------------------------------------------
void UDPSystemExchange::setReinitTime( uniset::timeout_t msec )
{
	waitTimeout = msec;
}
// -------------------------------------------------------------------------
