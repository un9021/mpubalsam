#include <iostream>
#include <sstream>
#include <Exceptions.h>
#include "UDPSender.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UDPnet;
using namespace uniset;
// -------------------------------------------------------------------------
UDPSender::UDPSender( const std::string& name, const string& s_host_, ost::tpport_t port_ ):
	myname(name),
	host(s_host_.c_str()),
	port(port_),
	s_host(s_host_)
{
	if( dlog.debugging(Debug::INFO) )
		dlog[Debug::INFO] << myname <<"(init): UDP set to " << s_host << ":" << port << endl;
	
	ost::Thread::setException(ost::Thread::throwException);
	try
	{
		udp = new ost::UDPBroadcast(host,port);
	}
	catch( std::exception& e )
	{
		ostringstream s;
		s << myname << "(init): (" << s_host << ":" << port << ")" << " err: " << e.what();
		dlog[Debug::CRIT] << s.str() << endl;
		throw SystemError(s.str());
	}
	catch( ... )
	{
		ostringstream s;
		s << myname << "(init): catch ...";
		dlog[Debug::CRIT] << s.str() << endl;
		throw SystemError(s.str());
	}
}
// -------------------------------------------------------------------------
UDPSender::~UDPSender()
{
	delete udp;
}
// -------------------------------------------------------------------------
UDPnet::udpErrCode UDPSender::send( UDPnet::UDPDataTo& msg, uniset::timeout_t timeout )
{	
	try
	{
		if( udp->isPending(ost::Socket::pendingOutput,timeout) )
		{
 			size_t ret = udp->send( (char*)&(msg),sizeof(msg) );
    		if( ret < sizeof(msg) )
			{
				if( dlog.debugging(Debug::CRIT) )
        			dlog[Debug::CRIT] << myname << "(send): FAILED ret=" << ret << " < sizeof=" << sizeof(msg) << endl;
				return erInternalErrorCode;
			}
			
			return UDPnet::erNoError;
		}
		
		return UDPnet::erTimeOut;
	}
	catch( ost::SockException& e )
	{
		if( dlog.debugging(Debug::CRIT) )
			dlog[Debug::CRIT] << myname << "(send): " << e.getString() << " (" << s_host << ":" << port << ")" << endl;
	}
	catch(...)
	{
		if( dlog.debugging(Debug::CRIT) )
			dlog[Debug::CRIT] << myname << "(send):  catch..." << endl;
	}
	
	return erInternalErrorCode;
}
// -------------------------------------------------------------------------
void UDPSender::initLog( DebugStream& dlog_ )
{						
	dlog = dlog_;
}
// -------------------------------------------------------------------------
const std::string UDPSender::getLinkName()
{
	ostringstream tmp;
	tmp << s_host << ":" << port;
	const string s(tmp.str());
	return s;
}
// -------------------------------------------------------------------------
