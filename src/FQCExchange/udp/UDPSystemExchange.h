#ifndef UDPSystemExchange_H_
#define UDPSystemExchange_H_
// --------------------------------------------------------------------------
#include <ostream>
#include <string>
#include <sigc++/sigc++.h>
#include <ThreadCreator.h>
#include <Trigger.h>
#include <Mutex.h>
#include <UniXML.h>
#include <PassiveTimer.h>
#include "UDPReceiver.h"
#include "../UDPNetTypes.h"
// --------------------------------------------------------------------------
/*!
	\page pgUDPSystemExchange Приём сообщений от СВУ
	\section sec_UDPSystemExchange Описание приёма сообщений от CВУ
	Класс UDPSystemExchange реализует приём сообщений по протоколу UDP по двум каналам на указанном порту.

	Для приёма запускается два потока.
	- Первый - для приёма сообщений по первому каналу
	- Второй - для приёма сообщений по второму каналу

	\section sec_UDPSystemProcessing Обработка сообщений	
		Сообщения постоянно принимаются по двум каналам. Но обработка сообщений ведётся только 
	из первого канала (ip1). Если связь по первому каналу пропадает, 
	идёт попытка обрабатывать сообщения по второму каналу.
		
	При этом если связь есть по какому-то каналу, то работа ведётся только с ним и переключение на другой
	канал происходит только, если пропадает связь по текущему рабочему каналу. Т.е. например работали с первым каналом, связь пропала, переключидись на второй. Связь на первом восстановилась, но мы продолжаем работать на втором, пока на нём не пропадёт.

	\todo Делать ли очередь на приём. Важно ли не терять пакеты...
*/
class UDPSystemExchange
{
	public:
		UDPSystemExchange( const std::string myname, xmlNode* cnode, 
							const std::string ip1, const std::string ip2, int port );
		~UDPSystemExchange();

		/*! один шаг проверок (связи и т.п.) */
		void step();
		
		/*! остановить обмен */
		void stop();
		
		/*! запустить обмен */
		void start();

		void setReinitTime( uniset::timeout_t msec );
		
		bool isLinkOK() { return (ip1LinkOK || ip2LinkOK); }
		
		bool isLinkOK( int num );
		
		/*! прервать работу */
		void terminate( int signo );

		long getAskCount() { return getAskCount(1); }
		long getAskCount( int num );

		/*! номер канала с которым ведётся работа */
		inline long getWorkChannel(){ return proc_ip; }

        typedef sigc::slot<void,const UDPnet::UDPDataFrom&,bool> ProcessingSlot;

		/*! подключение обработчика 'получения данных' */
        void connectProcessing( ProcessingSlot sl );

		friend std::ostream& operator<<(std::ostream& os, UDPSystemExchange* u );
		friend std::ostream& operator<<(std::ostream& os, UDPSystemExchange& u );

	protected:
		
		void sigterm( int signo );
		
		const std::string myname;
		bool ip1LinkOK;
		bool ip2LinkOK;
		unsigned int ip1AckCount;
		unsigned int ip2AckCount;
		
		bool active;
		uniset::PassiveTimer ip1Timeout;
		uniset::PassiveTimer ip2Timeout;
		UDPnet::UDPDataFrom empty_msg;
		uniset::Trigger trNetOK;
		unsigned int proc_ip;  /*!< номер канала с которым ведётся работа (обработка сообщений) */
		std::mutex ip_mutex;
		std::mutex data_mutex;
		ProcessingSlot slProcessing;

		UDPReceiver* udp_ip1;
		uniset::ThreadCreator<UDPSystemExchange>* thr_ip1;	/*!< приём сообщений ip1 */
		void ip1Thread();
		int thr_ip1_priority;
		UDPnet::UDPDataFrom ip1_msg, ip1_rbuf;
		
		UDPReceiver* udp_ip2;
		uniset::ThreadCreator<UDPSystemExchange>* thr_ip2;	/*!< приём сообщений ip2 */
		void ip2Thread();
		int thr_ip2_priority;
		UDPnet::UDPDataFrom ip2_msg, ip2_rbuf;

		timeout_t waitTimeout;
	
	private:
};
// --------------------------------------------------------------------------
#endif // UDPSystemExchange_H_
// --------------------------------------------------------------------------
