#include <cstdlib>
#include <bitset>
#include <errno.h>
#include <getopt.h>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <cc++/socket.h>
#include "UDPTypes.h"
// --------------------------------------------------------------------------
static struct option longopts[] = {
	{ "help", no_argument, 0, 'h' },
	{ "read", no_argument, 0, 'r' },
	{ "write", required_argument, 0, 'w' },
	{ "packet-type", required_argument, 0, 'a' },
	{ "from", required_argument, 0, 's' },
	{ "n1", required_argument, 0, 'n' },
	{ "pause", required_argument, 0, 'p' },
	{ "timeout", no_argument, 0, 'b' },
	{ "broadcast", required_argument, 0, 't' },
	{ "verbode", required_argument, 0, 'v' },
	{ NULL, 0, 0, 0 }
};
// --------------------------------------------------------------------------
using namespace std;
using namespace UDPnet;
// --------------------------------------------------------------------------
enum Command
{
	cmdNOP,
	cmdRead,
	cmdWrite
} cmd;
// --------------------------------------------------------------------------
void print_hex_dump( char* buf, size_t sz )
{
	cout << "----------------" << endl;
	for( int i=0; i<sz; i++ )
	{
		bitset<8> b( (unsigned char)buf[i] );
		cout << setw(2) << i << "   0x" << hex << std::left << setw(4) << (int)(buf[i]) 
				<< " (" << dec << setw(6) << (int)(buf[i]) << ")" 
				<< " [";
				for( int k=0; k<8; k++ )
					cout << b[k] << " ";
		cout <<  "]" << endl;
	}
	cout << "----------------" << endl;

}
// --------------------------------------------------------------------------
static bool split_addr( const string addr, string& host, ost::tpport_t& port )
{
	string::size_type pos = addr.rfind(':');
	if(  pos != string::npos )
	{
		host = addr.substr(0,pos);
		string s_port(addr.substr(pos+1,addr.size()-1));
		port = atoi(s_port.c_str());
		return true;
	}
	
	return false;
}
// --------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	int optindex = 0;
	int opt = 0;
	cmd = cmdNOP;
	int verb = 0;
	std::string addr = "";
	ost::tpport_t port=0;
	int usecpause = 500000;
	timeout_t tout = TIMEOUT_INF;
	bool broadcast = false;
	int packtype = 0;
	int from = 0;
	int setRPM = 100;

	while( (opt = getopt_long(argc, argv, "hr:w:vp:t:ba:s:n:",longopts,&optindex)) != -1 ) 
	{
		switch (opt) 
		{
			case 'h':	
				cout << "-h|--help                - this message" << endl;
				cout << "[-w|--write] host:port   - write udp message. Default type 'UDPnet::UDPDataFrom' (see -a)" << endl;
				cout << "[-a|--packet-type] n     - Write packet type: " << endl;
				cout << "                           0 - UDPnet::UDPDataFrom" << endl;
				cout << "                           1 - UDPnet::UDPDataTo" << endl;
				cout << "[-r|--read] host:port    - read udp message" << endl;
				cout << "[-v|--verbose]           - verbose mode." << endl;
				cout << "[-p|--pause] msec        - pause after write. Default: 500 msec." << endl;
				cout << "[-t|--timeout] msec      - timeout for receive. Default: 0 msec (waitup)." << endl;
				cout << "[-b|--broadcast]         - Set broadcast mode" << endl;
				cout << "[-s|--from]              - from system" << endl;
				cout << "                           0 - SVU1" << endl;
				cout << "                           1 - SVU2" << endl;
				cout << "[-n|--n1] val             - set the target speed. Default: 100" << endl;
				cout << endl;
			return 0;

			case 'r':	
				cmd = cmdRead;
				addr = string(optarg);
			break;

			case 'w':
				addr = string(optarg);
				cmd = cmdWrite;
			break;
			
			case 't':
				tout = atoi(optarg);
			break;	

			case 'p':
				usecpause = atoi(optarg)*1000;
			break;	

			case 's':
				from = atoi(optarg);
			break;	
			
			case 'a':
				packtype = atoi(optarg);
			break;	

			case 'n':
				setRPM = atoi(optarg);
			break;	
			
			case 'b':
				broadcast = true;
			break;	
			
			case 'v':	
				verb = 1;
			break;
			
			case '?':
			default:
				cerr << "? argumnet" << endl;
				return 0;
		}
	}
	
	if( cmd == cmdNOP )
	{
		cerr << "No command... Use -h for help" << endl;
		return -1;
	}

	if( tout < 0 )
		tout = TIMEOUT_INF;

	ost::Thread::setException(ost::Thread::throwException);
	
	try
	{
		string s_host;
		if( !split_addr(addr,s_host,port) )
		{
			cerr << "(main): Unknown 'host:port' for '" << addr << "'" << endl;
			return 1;
		}
		
		if( verb )
		{
			cout << " host=" << s_host 
				<< " port=" << port
				<< " timeout=";
			if( tout == TIMEOUT_INF )
				cout << "Waitup";
			else
				cout << tout;

			cout << " msecpause=" << usecpause/1000 
				<< endl;
		}

		ost::IPV4Host host(s_host.c_str());
//		udp.UDPTransmit::setBroadcast(broadcast);

		switch( cmd )
		{
			case cmdRead:
			{
				ost::UDPDuplex udp(host,port);
				
				// делаем универсальный read
				char buf[8194];
	
				//UDPnet::UDPDataTo msg;
				while(1)
				{
					try
					{
						if( !udp.isInputReady(tout) )
						{
							cout << "(read): Timeout.." << endl;
							continue;
						}
						
						size_t ret = udp.UDPReceive::receive(buf,sizeof(buf));
						if( ret == sizeof(UDPnet::UDPDataTo) )
						{
							UDPnet::UDPDataTo msg;
							memcpy(&msg,buf,sizeof(msg));
							cout << "(read): read 'UDPDataTo'  [OK].." 
								 << msg << endl;
							print_hex_dump( (char*)(&msg), sizeof(msg) );
						}
						else if( ret == sizeof(UDPnet::UDPDataFrom) )
						{
							UDPnet::UDPDataFrom msg;
							memcpy(&msg,buf,sizeof(msg));

							cout << "(read): read 'UDPDataFrom'  [OK].."
								 << msg << endl;

							print_hex_dump( (char*)(&msg), sizeof(msg) );
						}
						else
							cerr << "(read): BAD SIZE. ret=" << ret << endl;
					}
					catch( ost::SockException& e )
					{
						cerr << "(read): " << e.getString() << " (" << addr << ")" << endl;
					}
					catch( ... )
					{
						cerr << "(read): catch ..." << endl;
					}
				}
			}
			break;
	
			case cmdWrite:
			{	
				ost::UDPSocket* udp;
      			if( !broadcast )
            		udp = new ost::UDPSocket();
        		else
			        udp = new ost::UDPBroadcast(host,port); 

				UDPnet::UDPDataFrom msg_f;
				UDPnet::UDPDataTo msg_t;

				
				char buf[100];
				size_t sz = sizeof(buf);
				if( packtype == 0 )
				{
#if 0
					if( from )
					{
						msg_f.ctl_luga = 1;
						msg_f.ctl_andromeda = 0;
						if( verb && port != 2002 )
							cerr << "(read): WARNING! system='LUGA' but port='" << port << "'. Must be 2002" << endl;
					}
					else 
					{
						msg_f.ctl_luga = 0;
						msg_f.ctl_andromeda = 1;
						if( verb && port != 2001 )
							cerr << "(read): WARNING! system='ADROMEDA' but port='" << port << "'. Must be 2001" << endl;
					}
#endif
					msg_f.rem_confirm = false;
					msg_f.setRPM = setRPM;
//					msg_f.direction = 1;
					msg_f.mode_ab = 1;
					memcpy(buf,&msg_f,sizeof(msg_f));
					sz = sizeof(msg_f);
				}
				else if( packtype == 1 )
				{
				
					msg_t.lsu_mode_du = 1;
					msg_t.c_setRPM = setRPM;
//					msg_t.k2_failed = 1;
					msg_t.oil_level1 = 1;
					memcpy(buf,&msg_t,sizeof(msg_t));
					sz = sizeof(msg_t);
				}
				else
				{	
					cerr << "(send): Unknown packet type. Use -h for help." << endl;
					return 1;
				}
	
				if( sz > sizeof(buf) )
				{
					cerr << "(send): INTERNAL ERROR! BAD sizeof(buf)! (sz=" << sz << " buf=" << sizeof(buf) << endl;
					return 1;
				}

				udp->setPeer(host,port);

				while(1)
				{
					try
					{
						if( udp->isPending(ost::Socket::pendingOutput,tout) )
						{
							if( verb )
							{
								if( packtype ==  0 )
									cout << "(send): addr=" << addr << " " << msg_f << endl;
								else if( packtype ==  1 )
									cout << "(send): addr=" << addr << " " << msg_t << endl;
							}
 							
							size_t ret = udp->send(buf, sz);

							if( ret < sz )
        						cerr << "(send): FAILED ret=" << ret << " < sizeof=" << sz << endl;
						}
					}
					catch( ost::SockException& e )
					{
						cerr << "(send): " << e.getString() << " (" << addr << ")" << endl;
					}
					catch( ... )
					{
						cerr << "(send): catch ..." << endl;
					}

					usleep(usecpause);
				}
			}
			break;

			default:
				cerr << endl << "Unknown command: '" << cmd << "'. Use -h for help" << endl;
				return -1;
			break;
		}
	}
	catch( ost::SockException& e )
	{
		cerr << "(main): " << e.getString() << " (" << addr << ")" << endl;
		return 1;
	}
	catch( ... )
	{
		cerr << "(main): catch ..." << endl;
		return 1;
	}
	
	return 0;
}
// --------------------------------------------------------------------------
