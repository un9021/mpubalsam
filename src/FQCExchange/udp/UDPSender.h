#ifndef UDPSender_H_
#define UDPSender_H_
// -------------------------------------------------------------------------
#include <string>
#include <cc++/socket.h>
#include <Debug.h>
#include <Configuration.h>
#include <Mutex.h>
#include "UDPTypes.h"
// -------------------------------------------------------------------------
/*!	Реализация процесса посылки данных в СВУ */
class UDPSender
{
	public:
		UDPSender( const std::string& name, const std::string& s_host, ost::tpport_t port );
		~UDPSender();
		
		UDPnet::udpErrCode send( UDPnet::UDPDataTo& msg, uniset::timeout_t msec = 100 );
		
		void initLog( DebugStream& dlog );
		const std::string getLinkName();

	protected:
		
		std::string myname;
		DebugStream dlog;

		ost::UDPBroadcast* udp;
		ost::IPV4Host host;
		ost::tpport_t port;
		std::string s_host;
		
	private:
};
// -------------------------------------------------------------------------
#endif // UDPSender_H_
// -------------------------------------------------------------------------
