#include <iostream>
#include <sstream>
#include <Exceptions.h>
#include <PassiveTimer.h>
#include "UDPReceiver.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace UDPnet;
using namespace uniset;
// -------------------------------------------------------------------------
UDPReceiver::UDPReceiver( const std::string& name, const string& s_host_, ost::tpport_t port_ ):
	myname(name),
	recv_cnt(0),
	udp(0),
	host(s_host_.c_str()),
	port(port_),
	s_host(s_host_)
{
	if( dlog.debugging(Debug::INFO) )
		dlog[Debug::INFO] << myname <<"(init): UDP set to " << s_host << ":" << port << endl;
	
	ost::Thread::setException(ost::Thread::throwException);
	try
	{
		udp = new ost::UDPDuplex(host,port);
	}
	catch( std::exception& e )
	{
		ostringstream s;
		s << myname << "(init): (" << s_host << ":" << port << ")" << " err: " << e.what();
		dlog[Debug::CRIT] << s.str() << endl;
		throw SystemError(s.str());
	}
	catch( ... )
	{
		ostringstream s;
		s << myname << "(init): catch ...";
		dlog[Debug::CRIT] << s.str() << endl;
		throw SystemError(s.str());
	}
}
// -------------------------------------------------------------------------
UDPReceiver::~UDPReceiver()
{
	delete udp;
}
// -------------------------------------------------------------------------
const std::string UDPReceiver::getLinkName()
{
	ostringstream tmp;
	tmp << s_host << ":" << port;
	const string s(tmp.str());
	return s;
}
// -------------------------------------------------------------------------
void UDPReceiver::stop()
{
	if( udp )
		udp->disconnect();
}
// -------------------------------------------------------------------------
UDPnet::udpErrCode UDPReceiver::receive( UDPnet::UDPDataFrom& msg,  uniset::timeout_t timeout  )
{
	if( udp->isInputReady(timeout) )
	{
		size_t ret = udp->UDPReceive::receive(&msg,sizeof(msg));
		if( ret < sizeof(msg) )
		{
			if( dlog.debugging(Debug::CRIT) )
				dlog[Debug::CRIT] << myname << "(receive): ret=" << ret << " sizeof=" << sizeof(msg) << endl;
			return erInvalidFormat;
		}
		
		return erNoError;
	}	

	return erTimeOut;
}
// -------------------------------------------------------------------------
void UDPReceiver::initLog( DebugStream& dlog_ )
{						
	dlog = dlog_;
}
// -------------------------------------------------------------------------
void UDPReceiver::reinit()
{
	try
	{
		udp->disconnect();
		if( udp->connect(host,port) != 0 )
			cerr << "*********** REINIT FAILED ****** " << endl;
		return;
	}
 	catch( std::exception& e )
    {
        dlog[Debug::CRIT] << myname << "(reinit): " << e.what() << std::endl;
    }
	catch( ... )
	{
		dlog[Debug::CRIT] << myname << "(reinit): catch ..." << endl;
	}
}
// -------------------------------------------------------------------------
