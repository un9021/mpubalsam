#!/bin/sh

#ulimit -Sc 10000000
START=uniset2-start.sh

#	 --c-filter-field tsnode --c-filter-value 1 --heartbeat-node ts --lock-value-pause 10 \

${START} -f ./mpubalsam-exserver --confile configure.xml \
--smemory-id SharedMemory1 --rs-dev /dev/cbsideA0 --skip-can 1 --rs-use-485 0 --skip-mil --can-dev /dev/null \
--svu-recv-ip2 192.168.56.1 --svu1-port 2010 --svu2-port 2011 --svu-recv-ip1 localhost --svu-broadcast-ip2 192.168.56.255 --svu-broadcast-ip1 192.168.1.255 \
--svu-send-pause 3000 --svu-statistics-msec 5000 \
--dlog-add-levels info,warn,crit,level4,level5,level8
