#ifndef MyExchangeServer_H_
#define MyExchangeServer_H_
// --------------------------------------------------------------------------
#include <string>
#include <limits>
#include <ThreadCreator.h>
#include "ExchangeServer.h"
#include "UDPServer.h"
// --------------------------------------------------------------------------
/*! */
class MyExchangeServer:
	public ExchangeServer
{
	public:
		MyExchangeServer( const std::shared_ptr<uniset::SharedMemory>& shm);
		virtual ~MyExchangeServer();

		/*! глобальная функция для инициализации объекта */
		static MyExchangeServer* init_exchange( int argc, char* argv[], const std::shared_ptr<uniset::SharedMemory>& ic );

		/*! глобальная функция для вывода help-а */
		static void help_print( int argc, const char* const* argv );

	protected:

		virtual void svu_init( std::shared_ptr<uniset::SMInterface>& shm ) override;
		virtual void svu_start() override;
		virtual void svu_setToSM( std::shared_ptr<uniset::SMInterface>& shm ) override;
		virtual void svu_getFromSM( std::shared_ptr<uniset::SMInterface>& shm ) override;
		virtual void svu_setLSUNetOK( bool lsuDataOK ) override;
		virtual void svu_term( int signo ) override;
		virtual void svu_step( std::shared_ptr<uniset::SMInterface>& shm ) override;
		virtual bool svu_isLinkOK() override;
		virtual int svu_getAskCount() override;

		// обмен по UDP
		uniset::ThreadCreator<MyExchangeServer>* udpthr;
		std::shared_ptr<UDPServer> udp;
		void udpExchange();
};
// --------------------------------------------------------------------------
#endif // MyExchangeServer_H_
// --------------------------------------------------------------------------
