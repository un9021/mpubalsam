#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
//#include <NCRestorer.h>
#include <UInterface.h>
#include <UniXML.h>
#include "MPUConfiguration.h"
#include "MyExchangeServer.h"
#include "MPUTypes.h"
// --------------------------------------------------------------------------------
using namespace uniset;
using namespace MPU;
using namespace std;
// --------------------------------------------------------------------------------
void MyExchangeServer::help_print( int argc, const char* const* argv )
{
	ExchangeServer::help_print(argc, argv);

	cout << "************* UDP options ************" << endl;
	UDPServer::help_print(argc, argv);
}
// -----------------------------------------------------------------------------
MyExchangeServer::MyExchangeServer( const std::shared_ptr<SharedMemory>& ic ):
	ExchangeServer(ic),
	udpthr(0),
	udp(0)
{
	auto conf = uniset_conf();
	xmlNode* cnode = conf->getNode("ExchangeServer");

	if( cnode == NULL )
		throw SystemError("Not find conf-node for ExchangeServer");

	UniXML_iterator it(cnode);
	udp 		= make_shared<UDPServer>("UDPServer", cnode);
	udpthr	= new ThreadCreator<MyExchangeServer>(this, &MyExchangeServer::udpExchange);
}

// --------------------------------------------------------------------------------
MyExchangeServer::~MyExchangeServer()
{
	//delete udp;
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_init( std::shared_ptr<uniset::SMInterface>& shm )
{
	udp->init(shm);
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_start()
{
	bool udpOFF     = ( uniset::findArgParam("--skip-udp", uniset_conf()->getArgc(), uniset_conf()->getArgv()) != -1 ) ? true : false;

	if(!udpOFF)
		udpthr->start();
}
// --------------------------------------------------------------------------------
void MyExchangeServer::udpExchange()
{
	dlog[Debug::INFO] << myname << "(udpExchange): activate..." << endl;
	udp->start();
	udp->execute();
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_setToSM( std::shared_ptr<uniset::SMInterface>& shm )
{
	// обновляем данные от SUV
	udp->setToSM(shm);
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_getFromSM( std::shared_ptr<uniset::SMInterface>& shm )
{
	udp->getFromSM(shm);
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_setLSUNetOK( bool lsuDataOK )
{
	udp->setLSUNetOK(lsuDataOK);
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_step( std::shared_ptr<uniset::SMInterface>& shm )
{
	udp->step(shm);
}
// --------------------------------------------------------------------------------
bool MyExchangeServer::svu_isLinkOK()
{
	return udp->isLinkOK();
}
// --------------------------------------------------------------------------------
int MyExchangeServer::svu_getAskCount()
{
	return udp->getAskCount();
}
// --------------------------------------------------------------------------------
void MyExchangeServer::svu_term( int signo )
{
	dlog[Debug::SYSTEM] << "(MyExchangeServer:term): SIGNO=" << signo << endl << flush;

	try
	{
		udp->terminate(signo);
	}
	catch(...) {}
}
// --------------------------------------------------------------------------------
MyExchangeServer* MyExchangeServer::init_exchange( int argc, char* argv[], const std::shared_ptr<SharedMemory>& ic )
{
	return new MyExchangeServer(ic);
}
// --------------------------------------------------------------------------------
