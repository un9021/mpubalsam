#ifndef UDPServer_H_
#define UDPServer_H_
// --------------------------------------------------------------------------
#include <string>
#include <ThreadCreator.h>
#include <Trigger.h>
#include <Mutex.h>
#include <PassiveTimer.h>
#include "UDPNetTypes.h"
#include "SServer.h"
#include "udp/UDPSender.h"
#include "udp/UDPSystemExchange.h"
// --------------------------------------------------------------------------
/*!
	\page pgUDPExchange Обмен с системами верхнего уровня
	\section sec_UDPExchange Описание процесса обмена с СВУ
		Процесс UDPServer реализует обмен по протоколу UDP с системами Луга-Я.1(svu1) и Андромеда-Я.1(svu2).

	Для обмена запускается два потока:
	- для приёма сообщений от 15ИР3-8-1 и 15ИР3-8-2
	- Третий - для посылки broadcast сообщений системам по первому и второму каналам (одновременно)

	Согласно протоколу приём сообщений от СВУ ведётся по двум ethernet-каналам с двумя разными ip.
	При этом каждая из систем СВУ шлёт посылки на своём порту. А от нас посылка широкоещательная (broadcast).

	Описание организации приёма сообщений см. \ref pgUDPSystemExchange

	\section udp_Realisation Детали реализации
		Т.к. у каждого "наследника" от SServer есть своя копия таблицы данных (dtbl). То в данном классе, она используется для хранения данных от СВУ. Для соответствия идентификаторов данных (UDPTabIndex) и индексов в таблице данных, сделан специальный массив UDPServer::umap.

	\section udp_lsuOK Связь с ЛСУ
		Если связь с ЛСУ пропадает, то имитируется отсутствие связи и процесс
	ПЕРЕСТАЁТ ПОСЫЛАТЬ пакеты (см. UDPServer::sendThread() флаг 'sendOK').
	Для этого выставляется флаг UDPServer::lsuOK = false и сбрасывается флаг,
	что данные для посылки действительны (UDPServer::send_initOK = false).
	После того как связь появилась lsuOK = true, флаг, что данные действительны (send_iniOK=true) выставляется, только после первого обновления данных (см. UDPServer::setData).


	\todo Что делать если в одних пакетах приходит управление с луги, а в других с Андромеды?
          Вроде Андромеда - приоритетее. Но тогда надо организовать одновременную обработку пакетов
	(последних) от двух систем. И выбирать тот пакет, который приоритетнее..
*/
class UDPServer:
	public SServer
{
	public:
		UDPServer( const std::string& name, xmlNode* cnode = 0 );
		virtual ~UDPServer();

		static void help_print( int argc, const char* const* argv );

		/*! основная функция */
		virtual void execute();

		/*! остановить обмен */
		virtual void stop();

		/*! запустить обмен */
		virtual void start();

		virtual bool isLinkOK() const override;

		bool isLinkOK( int num );

		/*! прервать работу */
		virtual void terminate( int signo );

		/*! Установить время отсутсвия связи, после которго
			isLinkOK выдаст false
		*/
		virtual void setTimeout( int msec );

		virtual long getAskCount() const override
		{
			return getAskCount(1) + getAskCount(2);
		}
		long getAskCount( int num ) const;

		virtual long getErrCount() const override
		{
			return getErrCount(1) + getErrCount(2);
		}
		long getErrCount( int num ) const;

		virtual void updateData( const std::shared_ptr<uniset::SMInterface>& shm );
		virtual void setToSM( const std::shared_ptr<uniset::SMInterface>& shm );
		virtual void getFromSM( const std::shared_ptr<uniset::SMInterface>& shm );
		void initTabIndex( MPU::DataTable& tbl );

		void setLSUNetOK( bool set );

	protected:

		virtual bool addNewTabElement( size_t i, MPU::DataTableItem& ti, const uniset::UniXML::iterator& it );
		void reinitUDPDataTable();
		virtual void sigterm( int signo );

		MPU::UDPIndexMap umap;

		uniset::PassiveTimer ptStatistics;

		bool active;
		timeout_t checkPause;

		std::mutex lsu_mutex;
		bool lsuNetOK;
		uniset::Trigger trLsuOK;

		UDPSystemExchange* udp_recv;
		UDPSender* udp_send1;
		UDPSender* udp_send2;
		uniset::ThreadCreator<UDPServer>* thr_send;	/*!< посылка сообщений */
		void sendThread();	/*!< функция потока посылки сообщений в СВУ */
		int thr_send_priority;
		bool send_initOK;				/*!< флаг, что данные "действительны" */
		UDPnet::UDPDataTo send_msg;
		std::mutex send_mutex;
		timeout_t sendPause;
		timeout_t sendTimeout;

		std::mutex data_mutex;

		// обработка сообщений
		void svuProcessing( const UDPnet::UDPDataFrom& msg, bool force );

	private:
};
// --------------------------------------------------------------------------
#endif // UDPServer_H_
// --------------------------------------------------------------------------
