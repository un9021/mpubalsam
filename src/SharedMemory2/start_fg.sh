#!/bin/sh

#ulimit -Sc 10000000
START=uniset-start.sh

#	 --c-filter-field tsnode --c-filter-value 1 --heartbeat-node ts --lock-value-pause 10 \

export LD_LIBRARY_PATH="../../lib/.libs;../lib/.libs"

${START} -f ./smemory2-mpubalsam --smemory-id SharedMemory1 --sm-no-history 1 \
	--skip-mbs1 \
	--skip-mbs2 \
	--mbs-gui1-name MBSlave1 \
	--mbs-gui1-type TCP \
	--mbs-gui1-reg-from-id 1 \
	--mbs-gui1-inet-addr 192.168.177.100 \
	--mbs-gui1-inet-port 2048 \
	--mbs-gui1-respond-id MBSlave1_Respond_FS \
	--mbs-gui2-name MBSlave2 \
	--mbs-gui2-type TCP \
	--mbs-gui2-reg-from-id 1 \
	--mbs-gui2-inet-addr 192.168.178.100 \
	--mbs-gui2-inet-port 2048 \
	--mbs-gui2-respond-id MBSlave2_Respond_FS \
	--skip-rs 1 \
	--rs-dev /dev/null --rs-use-485 0 \
	--can-dev /dev/null \
	--can-type can200 \
	--can-baudrate 250 \
	--mbs-gui-name MBSlaveGUI \
	--mbs-gui-type TCP --mbs-gui-inet-addr localhost --mbs-gui-inet-port 2048 --mbs-gui-reg-from-id 1 --mbs-gui-my-addr 0x01 \
	--svu-recv-ip1 localhost --svu-broadcast-ip1 192.168.1.255 \
	--svu-send-pause 3000 --svu-statistics-msec 5000 \
	$*
