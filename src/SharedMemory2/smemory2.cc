// --------------------------------------------------------------------------
#include <string>
#include <sys/wait.h>
#include <Debug.h>
#include <UniSetActivator.h>
#include <ThreadCreator.h>
#include <modbus/ModbusTypes.h>
#include <MBSlave.h>
#include <Extensions.h>
#include <UHelpers.h>
#include <MPUSharedMemory.h>
#include <MPUConfiguration.h>
#include <UHelpers.h>
#include <ExchangeServer.h>
#include <MyExchangeServer.h>
#include <RMonitor.h>
#include <libUCan2/CanExceptions.h>
// --------------------------------------------------------------------------
using namespace uniset;
using namespace std;
using namespace MPU;
// --------------------------------------------------------------------------
static void help_print( int argc, const char* argv[] );
// --------------------------------------------------------------------------
int main( int argc, const char** argv )
{
	if( argc > 1 )
	{
		if( strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0 )
		{
			help_print(argc, argv);
			return 0;
		}
	}

	try
	{
		auto conf = uniset::uniset_init(argc, argv, "configure.xml");
		auto act = UniSetActivator::Instance();

		conf->initLogStream(dlog, "dlog");

		// ------------ SharedMemory ----------------
		auto shm = MPUSharedMemory::init_mpusmemory(argc, const_cast<char**>(argv));

		if( !shm )
			return 1;

		act->add(shm);

		// ------------- FQC Exchange --------------
		bool skip_exc 	= findArgParam("--skip-exc", argc, argv) != -1;
		ThreadCreator<MyExchangeServer>* es_thr = nullptr;

		if (!skip_exc)
		{
			auto es = MyExchangeServer::init_exchange(argc, const_cast<char**>(argv), shm);

			if( !es )
			{
				cout << "MyExchangeServer не создан" << endl;
				return 1;
			}

			es_thr = new ThreadCreator<MyExchangeServer>(es, &MyExchangeServer::execute);

			if( !es_thr )
			{
				cout << "ThreadCreator<MyExchangeServer> не создан" << endl;
				return 1;
			}
		}
		else
		{
			dlog[Debug::ANY] << "(smemory2): MyExchangeServer отключен " << endl;
		}

		// ------------- ModbusTCP Slave (GUI) --------------
		bool skip_mbs 	= findArgParam("--skip-mbs1", argc, argv) != -1;

		if( !skip_mbs )
		{
			auto mbs = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs-gui1");

			if( !mbs )
				return 1;

			act->add(mbs);
		}

		bool skip_mbs2 	= findArgParam("--skip-mbs2", argc, argv) != -1;

		if( !skip_mbs2 )
		{
			auto mbs2 = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs-gui2");

			if( !mbs2 )
				return 1;

			act->add(mbs2);
		}

		// ----------------------------------------
		// попытка решить вопрос с "зомби" процессами
		signal( SIGCHLD, MPU::on_sigchild );

		if (!skip_exc)
		{
			if( es_thr)
				es_thr->start();
		}

		// ----------------------------------------
		SystemMessage sm(SystemMessage::StartUp);
		act->broadcast( sm.transport_msg() );
		act->run(false);

		MPU::on_sigchild(SIGTERM);
		return 0;
	}
	catch(CanOpen::CanException& ex)
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch( ModbusRTU::mbException& ex )
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch(Exception& ex)
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch( CORBA::SystemException& ex )
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex.NP_minorString() << endl;
	}
	catch( ost::SockException& e )
	{
		dlog[Debug::CRIT] << "(smemory2): " << e.getString() << ": " << e.getSystemErrorString() << endl;
	}
	catch(...)
	{
		dlog[Debug::CRIT] << "(smemory2): catch(...)" << endl;
	}

	MPU::on_sigchild(SIGTERM);
	return 1;
}
// --------------------------------------------------------------------------
void help_print( int argc, const char* argv[] )
{
	cout << "--logfile fname	- выводить логи в файл fname. По умолчанию smemory.log" << endl;
	cout << "--datfile fname	- Файл с картой датчиков. По умолчанию configure.xml" << endl;
	cout << "--skip-can			- пропустить запуск CAN" << endl;
	cout << "--skip-rs			- пропустить запуск RS" << endl;
	cout << "--skip-udp			- пропустить запуск UDP" << endl;
	cout << "--skip-mbs1     		- пропустить запуск MBSlave с gui по 1 каналу" << endl;
	cout << "--skip-mbs2     		- пропустить запуск MBSlave с gui по 2 каналу" << endl;
	/*
		cout << "\n   ###### SM options ###### \n" << endl;
		MPUSharedMemory::help_print(argc, argv);

		cout << "\n   ###### Exchange options ###### \n" << endl;
		MyExchangeServer::help_print(argc, argv);*/
}
