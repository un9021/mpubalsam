#!/bin/sh

while [ true ]; do

	for p in `seq 1 100`; do
		uniset-mbrtutest -d /dev/cbsideB0 --write06 0x01 49460 $p
		sleep 0.5
	done

	for p in `seq 100 -1 1`; do
		uniset-mbrtutest -d /dev/cbsideB0 --write06 0x01 49460 $p
		sleep 0.5
	done

done

