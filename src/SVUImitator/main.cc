#include <sstream>
#include <ObjectsActivator.h>
#include "MPUConfiguration.h"
#include "SVUImitator.h"
// -----------------------------------------------------------------------------
using namespace uniset;
using namespace MPU;
using namespace std;
// -----------------------------------------------------------------------------
static bool init( const std::string& name, ObjectId& id, xmlNode*& cnode );
// -----------------------------------------------------------------------------
int main( int argc, const char** argv )
{
	try
	{
		string confile = uniset::getArgParam( "--confile", argc, argv, "configure.xml" );
		conf = new Configuration(argc, argv, confile);

		string logfilename = conf->getArgParam("--logfile", "svuimitator.log");
		string logname( conf->getLogDir() + logfilename );
		dlog.logFile( logname.c_str() );
		unideb.logFile( logname.c_str() );
		conf->initDebug(dlog, "dlog");

		ObjectsActivator act;

		ObjectId id;
		xmlNode* cnode;

		if( !init("SVUImitator1", id, cnode) )
			return 1;

		SVUImitator im1(id, cnode);

		act.addObject( static_cast<UniSetObject*>(&im1) );

		SystemMessage sm(SystemMessage::StartUp);
		act.broadcast( sm.transport_msg() );

		unideb(Debug::ANY) << "\n\n\n";
		unideb[Debug::ANY] << "(main): -------------- svuimitator START -------------------------\n\n";
		dlog(Debug::ANY) << "\n\n\n";
		dlog[Debug::ANY] << "(main): -------------- svuimitator START -------------------------\n\n";
		act.run(false);
	}
	catch(SystemError& err)
	{
		dlog[Debug::CRIT] << "(svuimitator): " << err << endl;
	}
	catch(Exception& ex)
	{
		dlog[Debug::CRIT] << "(svuimitator): " << ex << endl;
	}
	catch(...)
	{
		dlog[Debug::CRIT] << "(svuimitator): catch(...)" << endl;
	}

	return 0;
}
// -----------------------------------------------------------------------------
bool init( const std::string& name, ObjectId& id, xmlNode*& cnode )
{
	id 	= conf->getObjectID(name);
	cnode 	= conf->getNode(name);

	if( id == uniset::DefaultObjectId )
	{
		dlog[Debug::CRIT] << "(svuimitator): not found ID for " << name << endl;
		return false;
	}

	if( cnode == NULL )
	{
		dlog[Debug::CRIT] << "(svuimitator): not found section <" << name << "> in confile" << endl;
		return false;
	}

	return true;
}
// -----------------------------------------------------------------------------
