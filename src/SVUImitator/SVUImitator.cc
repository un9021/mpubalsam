#include <UniXML.h>
#include "SVUImitator.h"
// -----------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// -----------------------------------------------------------------------------
DebugStream SVUImitator::dlog;
static bool init_dlog_ok = false;
void SVUImitator::init_dlog( DebugStream& d )
{
	if( !init_dlog_ok )
	{
		SVUImitator::dlog = d;
		init_dlog_ok = true;
	}
}
// -----------------------------------------------------------------------------
SVUImitator::SVUImitator( uniset::ObjectId id, xmlNode* cnode ):
	SVUImitator_SK(id, cnode)
{
	if( cnode == NULL )
		throw Exception( myname + ": FAILED! not found confnode in confile!" );

	UniXML_iterator it(cnode);
}
// -----------------------------------------------------------------------------
SVUImitator::~SVUImitator()
{
}
// -----------------------------------------------------------------------------
void SVUImitator::step()
{
	out_RPM = in_SetRPM;
	out_c_setRPM = in_SetRPM;

	out_ready1 = true;

	if( out_ready2 )
	{
	}
}
// -----------------------------------------------------------------------------
void SVUImitator::sensorInfo( uniset::SensorMessage* sm )
{
	if( sm->id == stop )
	{
		if( sm->value )
			out_ready2 = false;
	}
}
// -----------------------------------------------------------------------------
