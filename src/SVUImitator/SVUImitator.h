#ifndef SVUImitator_H_
#define SVUImitator_H_
// -----------------------------------------------------------------------------
#include <list>
#include <UniSetTypes.h>
#include "SVUImitator_SK.h"
// -----------------------------------------------------------------------------
/*! Скелет примитивного имитатор-а для проверки работы на "Авроре"..
в итоге не понадобился. поэтому не дописан
*/
class SVUImitator:
	public SVUImitator_SK
{
	public:
		SVUImitator( uniset::ObjectId id, xmlNode* cnode );
		virtual ~SVUImitator();

		static DebugStream dlog;
		static void init_dlog( DebugStream& dlog );

	protected:
		virtual void sensorInfo( uniset::SensorMessage* sm );
		virtual void step();
		//		virtual void sigterm( int signo );

	private:
};
// -----------------------------------------------------------------------------
#endif // SVUImitator_H_
// -----------------------------------------------------------------------------
