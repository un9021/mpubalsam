#include <unistd.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <X11/keysym.h>

#define HAS_XKB 1
#ifdef HAS_XKB
#include <X11/XKBlib.h>
#endif

/*! При вызове программы с параметром off - отключает NumLock, с любыми другими параметрами - включает, дополнительно программа блокирует его изменение с клавиатуры */

static int AddModifier(XModifierKeymap **mapp, KeyCode keycode, int modifier)
{
    if (keycode) {
	*mapp = XInsertModifiermapEntry (*mapp, keycode, modifier);
	return (0);
    } else {
	return (-1);
    }
    /*NOTREACHED*/
}

static int RemoveModifier(XModifierKeymap **mapp, KeyCode keycode, int modifier)
{
    if (keycode) {
	*mapp = XDeleteModifiermapEntry (*mapp, keycode, modifier);
	return (0);
    } else {
	return (-1);
    }
    /*NOTREACHED*/
}
// -------------------------------------------------------------------------
static void mapping_busy_key(Display* dpy, int timeout)
{
    int i;
    unsigned char keymap[32];
    static unsigned int masktable[8] = {
	0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

    XQueryKeymap (dpy, (char *) keymap);

    fprintf (stderr,
	     "%s:  please release the following keys within %d seconds:\n",
	     "SetNumLock", timeout);
    for (i = 0; i < 256; i++) {
	if (keymap[i >> 3] & masktable[i & 7]) {
	    KeySym ks = XKeycodeToKeysym (dpy, (KeyCode) i, 0);
	    char *cp = XKeysymToString (ks);
	    fprintf (stderr, "    %s (keysym 0x%x, keycode %d)\n",
		     cp ? cp : "UNNAMED", (unsigned int)ks, i);
	}
    }
 	sleep(timeout);
    return;
}
// -------------------------------------------------------------------------
static int UpdateModifierMapping(Display* dpy, XModifierKeymap *map)
{
    int retries, timeout;

//  for (retries = 5, timeout = 2; retries > 0; retries--, timeout *= 2) {
	for (retries = 3, timeout = 1; retries > 0; retries--, timeout *= 2) {
	int result;

	result = XSetModifierMapping (dpy, map);
	switch (result) {
	  case MappingSuccess:	/* Success */
	    return (0);
	  case MappingBusy:		/* Busy */
	    mapping_busy_key(dpy,timeout);
	    continue;
	  case MappingFailed:
	    fprintf (stderr, "%s: bad set modifier mapping.\n",
		     "SetNumLock");
	    return (-1);
	  default:
	    fprintf (stderr, "%s:  bad return %d from XSetModifierMapping\n",
		     "SetNumLock", result);
	    return (-1);
	}
    }
    fprintf (stderr,
	     "%s:  unable to set modifier mapping, keyboard problem\n",
	     "SetNumLock");
    return (-1);
}
// -------------------------------------------------------------------------
#ifdef HAS_XKB
int get_numlock_state( Display* disp )
    {
    unsigned int states;
    if( XkbGetIndicatorState( disp, XkbUseCoreKbd, &states) != Success )
        {
        printf("Error while reading Indicator status\n");
//        XCloseDisplay( disp );
        return 0;
        }
    return states & 0x02; /* NumLock appears to be bit1 */
    }
#endif

void change_numlock( Display* disp )
    {
    XTestFakeKeyEvent( disp, XKeysymToKeycode( disp, XK_Num_Lock ), True, CurrentTime );
    XTestFakeKeyEvent( disp, XKeysymToKeycode( disp, XK_Num_Lock ), False, CurrentTime );
    }

#ifdef HAS_XKB
void set_on( Display* disp )
    {
    if( !get_numlock_state(disp))
        change_numlock(disp);
    }

void set_off( Display* disp )
    {
    if( get_numlock_state(disp))
        change_numlock(disp);
    }
#endif

int main(int args, char **argv)
{
	char on = 1;
	if( args > 0 )
	{
		if(!strcmp(argv[1],"off")) on = 0;
	}
	Display* disp = XOpenDisplay(0);
	XModifierKeymap *map = XGetModifierMapping(disp);
	if( AddModifier(&map,XKeysymToKeycode(disp, XK_Num_Lock),4) < 0 ) // 4 -  mod2
		fprintf(stderr,"(SetNumLock): ***** AddModifier failed...\n");
	else
	{
		UpdateModifierMapping(disp,map);
	}
	if(on==1) set_on(disp);
	else set_off(disp);
	if( RemoveModifier(&map,XKeysymToKeycode(disp,XK_Num_Lock),4) < 0 ) // 4 -  mod2
		fprintf(stderr,"(SetNumLock): ***** RemoveModifier failed...\n");
	else
		UpdateModifierMapping(disp,map);
	return 0;
}
